---
titleTemplate: hi-uniapp-starter
layout: doc
aside: left
---

# hi-uniapp-starter 项目基座

::: info 简介
一个 `vue3 + pinia + hi-ui` 的基础项目底座。
:::


# 开始

前往 `uni-app` 插件市场下载 `hi-uniapp-starter` 项目，[插件地址](https://ext.dcloud.net.cn/plugin?name=hi-uniapp-starter)。

::: tip 提示
由于 `uni-app` 对模板项目需要审核，所以一般插件市场上的版本不是最新的，可以前往仓库下载最新版本 [仓库地址](https://gitee.com/chenshuang-jinli/hi-uniapp-starter)。
:::


# 安装依赖

```bash
npm install
```


# 配置

## 配置 `main.js`

一般在 `main.js` 中只需配置一下扩展图标。

```js
// ...

// 配置 hi-ui 扩展图标
uni.$hi.config.icon.prefix = ["app-iconfont"];

// ...
```


## 扩展图标文件

扩展图标 `css` 文件放置在了 `@/styles/iconfont.css`，需将此文件替换为您当前项目用到的图标文件。


## 项目配置文件

修改 `@/settings/index.js` 中配置的项目默认配置。

> [!TIP] 提示
> 此项目并没有使用 `.env.development` 和 `.env.production` 文件，只是个人习惯而已。


## 配置 `pages.json`

在 `pages.json` 中除了常规的配置，还支持以下页面 `meta` 配置项：

| 名称        | 说明                                                                                                           | 类型      | 版本 |
| :---------- | :------------------------------------------------------------------------------------------------------------- | :-------- | :--- |
| `needLogin` | 当前页面是否需要登录才能访问                                                                                   | `Boolean` | -    |
| `isLogin`   | 是否是登录页（项目中处理跳转到登录页的逻辑时，如果没有特殊配置，会默认跳转到此字段为 `true` 的第一个页面路径） | `Boolean` | -    |
| isHome      | 是否是首页（项目中处理跳转到首页的逻辑时，如果没有特殊配置，会默认跳转到此字段为 `true` 的第一个页面路径）     | `Boolean` | -    |


## 配置开发服务器

如果您的项目是 `H5` 类型，建议在 `manifest.json` 中配置开发服务器。

```json
"devServer": {
    // 指定服务器应该监听哪个 IP 地址
    "host": "localhost",

    // 指定开发服务器端口
    "port": 9000,

    // disableHostCheck
    "https": false,

    // 禁用 Host 检查
    "disableHostCheck": true,

    // 为开发服务器配置自定义代理规则
    "proxy": {
        // 匹配规则
        "/api": {
            // 代理到的实际地址
            "target": "https://apifoxmock.com/m1/4872189-4528102-default",

            // changeOrigin是一个可选的配置项。当它被设置为true时，请求头中的origin属性会被改变为目标服务器的地址，这样可以避免跨域问题
            "changeOrigin": true,

            // 重写路径中的字符串
            "rewrite": {
                "/api": "/api"
            }
        }
    }
}
```

至此，此项目就配置完成了。


## 目录结构

```
| --- components                 组件目录
|     |__ a-component            一个组件模板
|     |__ page-root              页面根组件，所有页面的第一层组件
| --- constants                  常量目录
|     |__ demo.js                常量 `DEMO`
|     |__ index.js               常量索引
| --- functions                  工具函数目录
|     |__ demo.js                工具函数 `DEMO`
|     |__ index.js               工具函数索引
| --- mock                       模拟数据目录
|     |__ demo.js                模拟数据 `DEMO`
|     |__ index.js               模拟数据索引
| --- node_modules               `node_modules` 目录
| --- packages                   分包目录
|     |__ order                  订单模块包，用于订单模块，一个示例模块
|         |__ pages              订单模块页面目录
| --- pages                      主包页面目录
|     |__ auth                   鉴权相关页面，例如：登录、注册、认证等页面
|         |__ login.vue          登录页面
|     |__ home                   主页相关页面
|         |__ index.vue          主页
|     |__ mine                   我的相关页面
|         |__ index.vue          我的
|     |__ start                  启动相关页面，放置一些启动相关的页面，例如：启动页、欢迎页、场景分发等页面
|         |__ index.vue          启动页
|     |__ webview                `webview` 相关页面，放置一些 `webview` 页面
|         |__ index.vue          公共 `webview` 页面
|     |__ a-page.vue             一个页面模板
| --- router                     路由管理目录
|     |__ config.js              `hi-router` 路由配置文件
|     |__ index.js               路由相关工具函数
| --- services                   接口管理目录
|     |__ common.js              公共接口函数
|     |__ index.js               接口函数索引
|     |__ instances.js           `hi-http` 请求示例文件
|     |__ upload.js              上传接口函数
|     |__ user.js                用户相关接口
| --- settings                   项目配置管理目录
|     |__ index.js               项目配置文件
| --- static                     静态资源目录
|     |__ images                 图片资源目录
|         |__ bgis               背景图资源目录
|         |__ icons              图片图标资源目录
|         |__ temps              临时图片资源目录
|     |__ tabBar                 `tabBar` 相关资源目录
|         |__ icons              `tabBar` 图标资源目录
|     |__ favicon.ico            项目网站图标文件
|     |__ logo.jpg               项目 `logo` 图片
| --- stores                     状态数据管理目录
|     |__ demo.js                状态数据 `DEMO`
|     |__ index.js               状态数据索引
|     |__ persist-common-config  `pinia` 持久化插件 `"pinia-plugin-persistedstate"` 公共配置
|     |__ platform.js            平台相关状态数据
|     |__ user.js                用户相关状态数据
| --- styles                     样式管理目录
|     |__ assist.scss            辅助样式类
|     |__ iconfont.css           项目 `iconfont` 字体图标文件
|     |__ index.scss             样式索引
|     |__ variables.scss         项目样式变量
| --- uni_modules                `uni_modules` 目录
| --- unpackage                  编译目录
| --- .gitignore                 `.gitignore` 文件
| --- App.vue                    应用配置，用来配置 `App` 全局样式以及监听应用生命周期
| --- changelog.md               更新日志
| --- index.html                 `index.html` 模板
| --- package.json               `package.json` 文件
| --- pages.json                 配置页面路由、导航条、选项卡等页面类信息
| --- prettier.config.js         `prettier` 格式化配置
| --- readme.md                  项目介绍
| --- uni.promisify.adaptor.js   创建项目时自带的 `Promise` 拦截器配置文件
| --- uni.scss                   内置的常用样式变量
```

## 加群交流反馈

### QQ群

<br/>

<Qrcodes :list="list" />

<script setup>
    import { ref } from 'vue';
    import Qrcodes from '../components/Qrcodes.vue';

    // 列表
    const list = ref([
        { text: "1️⃣群", src: "/hi-uniapp-starter/qrcode/qq-group-01.png" }
    ]);
</script>