---
# 页面的布局 doc | home | page
layout: home

# Hero 部分位于主页顶部
hero:
  name: "嘿，道可斯！"
  text: "Hi系列开源项目<br/>官方参考文档"
  tagline: 开源不易，欢迎Star！
  image:
    src: /logo-large.png
    alt: Hi Docs！
  actions:
    - theme: brand
      text: hi-ui 文档
      link: /hi-ui/demo

    - theme: brand
      text: hi-ui pro 文档
      link: /hi-ui-pro/index

    - theme: brand
      text: hi-http 文档
      link: /hi-http/index

    - theme: brand
      text: hi-router 文档
      link: /hi-router/index

    - theme: brand
      text: hi-uniapp-starter 文档
      link: /hi-uniapp-starter/index

# 可以在 Hero 部分之后列出任意数量的 Feature
features:
  - title: hi-ui 基础组件库
    icon:
      src: /hi-ui/logo.png
    details: 开箱使用，丰富的组件配置，易于修改默认组件样式
    link: /hi-ui/demo

  - title: hi-ui pro 扩展组件库
    icon:
      src: /hi-ui-pro/logo.png
    details: 基于 hi-ui 封装的常见模块组件，快速实现页面的组装
    link: /hi-ui-pro/index

  - title: hi-http 请求/上传工具
    icon:
      src: /hi-http/logo.png
    details: 支持 request 接口请求，upload 文件上传
    link: /hi-http/index

  - title: hi-router 路由拦截/监听工具
    icon:
      src: /hi-router/logo.png
    details: 支持路由跳转前的拦截，路由跳转后的监听
    link: /hi-router/index

  - title: hi-uniapp-starter 一个项目基座
    icon:
      src: /hi-router/logo.png
    details: 一个 vue3 + pinia + hi-ui 的基础项目底座
    link: /hi-uniapp-starter/index
---

