---
titleTemplate: hi-router
layout: doc
aside: left
---

# hi-router 路由拦截/监听工具

::: info 简介
一个简单实用的路由拦截/监听工具。
:::

::: tip 提示
由于 `uni.switchTab` 的特殊性，该工具暂时不支持原生 `tabbar` 跳转的拦截。
:::


# 开始

## 安装

前往 `uni-app` 插件市场引用 `hi-router` 插件，[插件地址](https://ext.dcloud.net.cn/plugin?name=hi-router)。


## 配置

在 `main.js` 中引入并配置。

```js
// 引入 hi-router
import hiRouter from "@/uni_modules/hi-router";

// 使用并配置 hi-router
export function createApp() {
    const app = createSSRApp(App);

    // 使用并配置 hi-router
    app.use(hiRouter, {
        // 配置项，具体配置项请查看下方配置项说明
        // ...
    });

    return {
        app
    };
}
```


## 使用示例

```vue
<script setup>
    import { ref } from 'vue';

    // 跳转页面
    /// 对应 uni.navigateTo() 方法
    uni.$hiRouter.navigateTo("/pages/order/list");
    uni.$hiRouter.to("/pages/order/list");

    /// 对应 uni.redirectTo() 方法
    uni.$hiRouter.redirectTo("/pages/order/list");
    uni.$hiRouter.redirect("/pages/order/list");

    /// 对应 uni.reLaunch() 方法
    uni.$hiRouter.reLaunch("/pages/order/list");
    uni.$hiRouter.launch("/pages/order/list");

    /// 对应 uni.switchTab() 方法
    uni.$hiRouter.switchTab("/pages/order/list");
    uni.$hiRouter.tab("/pages/order/list");

    /// 对应 uni.navigateBack() 方法
    uni.$hiRouter.navigateBack();
    uni.$hiRouter.back();
</script>
```

## 方法列表

| 名称           | 说明                                                 | 参数                                                                                                                    | 返回值 | 版本 |
| :------------- | :--------------------------------------------------- | :---------------------------------------------------------------------------------------------------------------------- | :----- | :--- |
| `navigateTo`   | 保留当前页面，跳转到应用内的某个页面                 | `options`: `String` 时表示页面路径，`Object` 时 [参考](https://uniapp.dcloud.net.cn/api/router.html#navigateto)         | -      | -    |
| `to`           | `navigateTo` 的简写形式                              | 同上                                                                                                                    | -      | -    |
| `redirectTo`   | 关闭当前页面，跳转到应用内的某个页面                 | `options`: `String` 时表示页面路径，`Object` 时 [参考](https://uniapp.dcloud.net.cn/api/router.html#redirectto)         | -      | -    |
| `redirect`     | `redirectTo` 的简写形式                              | 同上                                                                                                                    | -      | -    |
| `reLaunch`     | 关闭所有页面，打开到应用内的某个页面                 | `options`: `String` 时表示页面路径，`Object` 时 [参考](https://uniapp.dcloud.net.cn/api/router.html#relaunch)           | -      | -    |
| `launch`       | `reLaunch` 的简写形式                                | 同上                                                                                                                    | -      | -    |
| `switchTab`    | 跳转到 `tabBar` 页面，并关闭其他所有非 `tabBar` 页面 | `options`: `String` 时表示页面路径，`Object` 时 [参考](https://uniapp.dcloud.net.cn/api/router.html#switchtab)          | -      | -    |
| `tab`          | `switchTab` 的简写形式                               | 同上                                                                                                                    | -      | -    |
| `navigateBack` | 关闭当前页面，返回上一页面或多级页面                 | `options`: `Number` 时表示要返回的页面数，`Object` 时 [参考](https://uniapp.dcloud.net.cn/api/router.html#navigateback) | -      | -    |
| `back`         | `navigateBack` 的简写形式                            | 同上                                                                                                                    | -      | -    |
| `route`        | 跳转页面，请查看下方的详细说明                       | `options`: `String` 时表示页面路径，`Object` 时对应上方方法的 `Object` 参数；`type`: `String`，跳转方式。               | -      | -    |
| `go`           | `route` 的简写形式                                   | 同上                                                                                                                    | -      | -    |
| `refresh`      | 刷新当前页面，请查看下方的详细说明                   | -                                                                                                                       | -      | -    |
| `setConfig`    | 设置配置                                             | `config`: 配置项，一个 `Object`                                                                                         | -      | -    |

**`route()` 和 `go()` 方法的详细说明**

参数：

* `options`:
    - `String` 时：表示页面路径；
    - `Number` 时：表示返回的页面数；
    - `Object` 时：对应上方 `navigateTo`、`redirectTo`、`reLaunch`、`switchTab`、`navigateBack` 方法的参数为 `Object` 时的说明。两外，该 `Object` 扩展了一个 `method` 属性用于指定跳转方式，`method` 对应的值同第二个参数 `type`。
* `type`：`String`，跳转方式，可选值 `navigateTo`、`redirectTo`、`reLaunch`、`switchTab`、`navigateBack`。


这两个方法是一个综合跳转逻辑，具体的逻辑为：

1. 如果目标页面是一个绝对 `URL`
    - 如果是在 `H5` 环境中：直接用 `window.open()` 打开页面；
    - 如果是在小程序环境中：直接返回，不会打开页面；
    - 如果是在 `APP` 环境中：跳转到外部浏览器打开页面；
2. 如果没有指定跳转方式，即：第二个参数为空，或第一个参数是个字符串，又或第一个参数没有指定 `method`
    - 如果目标页面是 `tabBar` 页面则使用 `switchTab` 方式跳转；
    - 如果目标页面是非 `tabBar` 页面默认使用 `navigateTo` 方式跳转；
    - 如果第一个参数是数字则使用 `navigateBack` 方式返回对应的页数；


## 配置项

| 名称     | 说明                                                                                                                                                           | 类型       | 默认值 | 版本 |
| :------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------- | :--------- | :----- | :--- |
| `before` | 路由跳转前的拦截函数，如果返回 `false`，则不会继续执行跳转，第一个参数为目标页面的路由数据，第二个参数为跳转时的参数对象数据，第三个参数为当前页面的路由数据。 | `Function` | `null` | -    |
| `after`  | 后置路由函数，每次进入页面都会触发，参数为当前页面的路由数据，不要在此函数中进行刷新页面的操作，否则会陷入死循环。                                             | `Function` | `null` | -    |


## 加群交流反馈

### QQ群

<br/>

<Qrcodes :list="list" />

<script setup>
    import { ref } from 'vue';
    import Qrcodes from '../components/Qrcodes.vue';

    // 列表
    const list = ref([
        { text: "1️⃣群", src: "/hi-router/qrcode/qq-group-01.png" }
    ]);
</script>