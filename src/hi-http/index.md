---
titleTemplate: hi-http
layout: doc
aside: left
---

# hi-http 请求/上传工具

::: info 简介
一个基于 `uni.request()` 和 `uni.uploadFile()` 进行二次封装的请求/上传工具。
<br/>
只是增加了一些额外配置，方便在项目中使用。
:::

::: tip 提示
如果接口返回 `header` 中有最新 `token` 的值，该工具会自动更新。
:::


# 开始

## 安装

前往 `uni-app` 插件市场引用 `hi-http` 插件，[插件地址](https://ext.dcloud.net.cn/plugin?name=hi-http)。


## 使用示例

```vue
<script setup>
    import { ref } from 'vue';

    // 引用 hi-http
    import hiHttp from "@/uni_modules/hi-http";

    // 创建一个 hi-http 实例
    const hh = new hiHttp({
        // 可以在这里配置一些配置项
        baseURL: "https://jinanchenshuang.com", // 接口基准路径
    });

    // GET 请求
    async function getUserInfo() {
        // 开始请求
        const res = await hh.get({
            // 请求的接口，会和 baseURL 合并成完整的接口地址
            url: "/api/get_userinfo",

            // 请求参数
            data: {
                id: 1001
            },

            // 处理指定的响应码
            codeFunctions: {
                // token 失效
                401: (response, config) {
                    // 处理 token 失效
                },

                // 其他响应码
                // ...
            }

            // 其他配置
            // ...
        });

        // 请求完成
        // 提示：不管请求成功或失败，res 始终是一个包含状态码、状态信息、数据（可能有）字段的对象，例如：

        // {
        //     code: 0, // 状态码
        //     errMsg: "ok", // 状态信息
        //     data: {}, // 数据
        // }

        // 或

        // {
        //     code: -1, // 状态码
        //     errMsg: "fail", // 状态信息
        //     data: null, // 数据
        // }

        // 或
        // 可能没有 data 字段

        // {
        //     code: -2, // 状态码
        //     errMsg: "fail", // 状态信息
        // }
    }

    // 或
    function getUserInfo() {
        hh.get({
            // 请求的接口，会和 baseURL 合并成完整的接口地址
            url: "/api/get_userinfo",

            // 请求参数
            data: {
                id: 1001
            },

            // 其他配置
            // ...
        })

        // 成功
        .then(res => {
            // 走到此回调函数表明请求成功并且数据的状态码也正常
        })

        // 失败
        .catch(err => {
            // 走到这里有可能时请求成功但是数据的状态码不正常或者请求未成功
        })

        // 完成
        .finally(() => {
            // 走到此处表明请求已经完成，不管成功后失败
        });
    }

    // 或
    // 提示：使用此中方式，即：传递 success、error、fail、complete 四个回调函数中的任何一个时，返回的不是 Promise，而是返回该次请求的 taskId，可以通过 taskId 来取消该次请求。
    function getUserInfo() {
        hh.get({
            // 请求的接口，会和 baseURL 合并成完整的接口地址
            url: "/api/get_userinfo",

            // 请求参数
            data: {
                id: 1001
            },

            // 成功
            success: (res) => {
                // 走到此回调函数表明请求成功并且数据的状态码也正常
            },

            // 失败
            // 提示: uni 本身没有此回调函数，该回调函数是为了更好的处理返回数据而扩展增加的
            error: (err) => {
                // 走到此处表明请求成功但是数据的状态码不正常
            },

            // 失败
            fail: (err) => {
                // 走到此处表明请求未成功，包括: 网络不通 | 请求被强行中断 | 请求超时 | 找不到域名 | ssl握手失败等
            },

            // 完成
            complete: () => {
                // 走到此处表明请求已经完成，不管成功后失败
            }
        });
    }


    // POST 请求
    async function queryData() {
        const res = await hh.post({
            // 请求的接口，会和 baseURL 合并成完整的接口地址
            url: "/api/query_data",

            // 请求参数
            data: {
                id: 1001
            },

            // 其他配置
            // ...
        });
    }


    // UPLOAD 上传请求
    async function uploadImage() {
        const res = await hh.upload({
            // 请求的接口，会和 baseURL 合并成完整的接口地址
            url: "/api/query_data",

            // 文件资源的路径
            filePath: ""

            // 其他配置
            // ...
        });
    }
</script>
```

## 方法列表

支持以下方法：

* `setConfig(config)`: 设置实例配置；
* `get(config)`: GET 请求；
* `post(config)`: POST 请求；
* `put(config)`: PUT 请求；
* `del(config)`: DELETE 请求：
* `connect(config)`: CONNECT 请求；
* `head(config)`: HEAD 请求；
* `options(config)`: OPTIONS 请求；
* `trace(config)`: TRACE 请求；
* `upload(config)`: 上传请求；
* `aborts()`: 取消所有请求；

> [!TIP] 提示
> 除 `aborts()` 方法外，所有方法都可以接受一个配置对象参数，具体的配置对象配置项请参考下方的说明。

> [!TIP] 提示
> 每个平台支持的可使用的方法不同，具体参考 [官方说明](https://uniapp.dcloud.net.cn/api/request/request.html)

## 配置项

* [uni.request() 自带配置](https://uniapp.dcloud.net.cn/api/request/request.html)
* [uni.uploadFile() 自带配置](https://uniapp.dcloud.net.cn/api/request/network-file.html)

| 名称                      | 说明                                                                                                                  | 类型       | 默认值          | 版本 |
| :------------------------ | :-------------------------------------------------------------------------------------------------------------------- | :--------- | :-------------- | :--- |
| `url`                     | 接口地址，可以是绝对路径或者相对路径，如果是相对路径，最终会和 `baseURL` 合并成一个完整的 `URL`                       | `String`   | `""`            | -    |
| `data`                    | 请求参数                                                                                                              | `Object`   | `{}`            | -    |
| `header`                  | 请求的 `header`, `header` 中不能设置 `Referer`                                                                        | `Object`   | `{}`            | -    |
| `method`                  | 请求方式，可选值参考上方的方法列表                                                                                    | `String`   | `GET`           | -    |
| `timeout`                 | 超时时间，单位为毫秒                                                                                                  | `Number`   | `30000`         | -    |
| `dataType`                | 告诉服务器，我要发送的数据类型，如果设为 `json`, 会对返回的数据进行一次 `JSON.parse`, 非 `json` 不会进行 `JSON.parse` | `String`   | `json`          | -    |
| `responseType`            | 告诉服务器，我要接收的数据类型，`text / arraybuffer`                                                                  | `String`   | `text`          | -    |
| `sslVerify`               | 是否校验 `ssl` 证书                                                                                                   | `Boolean`  | `true`          | -    |
| `withCredentials`         | 跨域请求时是否携带凭证`（cookies）`                                                                                   | `Boolean`  | `false`         | -    |
| `firstIpv4`               | `DNS` 解析时是否优先使用 `ipv4`                                                                                       | `Boolean`  | `false`         | -    |
| `enableHttp2`             | 是否开启 `http2`                                                                                                      | `Boolean`  | `false`         | -    |
| `enableQuic`              | 是否开启 `quic`                                                                                                       | `Boolean`  | `false`         | -    |
| `enableCache`             | 是否开启 `cache`                                                                                                      | `Boolean`  | `false`         | -    |
| `enableHttpDNS`           | 是否开启 `HttpDNS` 服务                                                                                               | `Boolean`  | `false`         | -    |
| `httpDNSServiceId`        | `HttpDNS` 服务 `ID`                                                                                                   | `String`   | `""`            | -    |
| `enableChunked`           | 是否开启 `transfer-encoding chunked`                                                                                  | `Boolean`  | `false`         | -    |
| `forceCellularNetwork`    | 是否在 `wifi` 下使用移动网络发送请求                                                                                  | `Boolean`  | `false`         | -    |
| `enableCookie`            | 开启后可在 `headers` 中编辑 `cookie`                                                                                  | `Boolean`  | `false`         | -    |
| `cloudCache`              | 是否开启云加速                                                                                                        | `Boolean`  | `false`         | -    |
| `defer`                   | 控制当前请求是否延时至首屏内容渲染后发送                                                                              | `Boolean`  | `false`         | -    |
| `files`                   | 需要上传的文件列表。使用 `files` `时，filePath` 和 `name` 不生效                                                      | `Array`    | `null`          | -    |
| `fileType`                | 文件类型，`image / video / audio`                                                                                     | `String`   | `image`         | -    |
| `file`                    | 要上传的文件对象                                                                                                      | `Object`   | `null`          | -    |
| `filePath`                | 要上传文件资源的路径                                                                                                  | `String`   | `null`          | -    |
| `name`                    | 文件对应的 `key` , 开发者在服务器端通过这个 `key` 可以获取到文件二进制内容                                            | `String`   | `file`          | -    |
| `formData`                | `HTTP` 请求中其他额外的 `form data`                                                                                   | `Object`   | `{}`            | -    |
| `success`                 | 请求成功并且数据状态正常的回调函数，参数为：`response.data` 和本次请求的最终配置                                      | `Function` | `null`          | -    |
| `error`                   | 请求成功但是数据状态异常的回调函数，参数为：错误信息对象和本次请求的最终配置                                          | `Function` | `null`          | -    |
| `fail`                    | 请求失败的回调函数，参数为：错误信息对象和本次请求的最终配置                                                          | `Function` | `null`          | -    |
| `complete`                | 请求完成的回调函数，参数为 `response` 和本次请求的最终配置                                                            | `Function` | `null`          | -    |
| `baseURL`                 | 接口基准前缀                                                                                                          | `String`   | `""`            | -    |
| `requestTokenKey`         | 请求时携带的鉴权凭证名称                                                                                              | `String`   | `Authorization` | -    |
| `responseTokenKey`        | 响应 `Header` 中携带的鉴权凭证名称，每次响应该工具都会尝试更新鉴权凭证的值                                            | `String`   | `Authorization` | -    |
| `storageTokenKey`         | 本地存储的鉴权凭证名称                                                                                                | `String`   | `Authorization` | -    |
| `requestRefreshTokenKey`  | 请求时携带的用于更新鉴权凭证的字段的名称                                                                              | `String`   | `refresh_token` | -    |
| `responseRefreshTokenKey` | 响应 `Header` 中携带的用于更新鉴权凭证的字段的名称                                                                    | `String`   | `refresh_token` | -    |
| `dataStatusKey`           | 接口返回数据中表示状态码的字段名称                                                                                    | `String`   | `code`          | -    |
| `dataStatusTextKey`       | 接口返回数据中表示状态文本的字段名称                                                                                  | `String`   | `errMsg`        | -    |
| `dataSuccessStatusCode`   | 接口返回数据中表示数据状态正常的状态码的值                                                                            | `Number`   | `0`             | -    |
| `requestFailStatusCode`   | 当请求失败时的状态码值                                                                                                | `Number`   | `-1`            | -    |
| `before`                  | 请求前的拦截器，参数为本次请求的最终配置，返回 `true` 则继续请求，返回 `false` 则中断请求                             | `Function` | `null`          | -    |
| `after`                   | 响应处理器，不会中断响应，参数为 `response` 和本次请求的最终配置                                                      | `Function` | `null`          | -    |
| `codeFunctions`           | `HTTP` 状态码和数据状态码处理函数集，调参数为 `response` 和本次请求的最终配置                                         | `Object`   | `{}`            | -    |
| `loadingTips`             | 是否显示 `loading`                                                                                                    | `Boolean`  | `true`          | -    |
| `loadingTime`             | 请求超过此值设置的毫秒数后才会显示 `loading`，设置为 `0` 会在请求后立即显示                                           | `Number`   | `800`           | -    |
| `loadingText`             | `loading` 提示内容                                                                                                    | `String`   | `""`            | -    |
| `loadingMask`             | 是否显示 `loading` 的透明蒙层，防止触摸穿透                                                                           | `Boolean`  | `true`          | -    |
| `loadingShowFunction`     | 自定义显示 `loading` 的函数，参数为本次请求的最终配置                                                                 | `Function` | `null`          | -    |
| `loadingHideFunction`     | 自定义隐藏 `loading` 的函数，参数为本次请求的最终配置                                                                 | `Function` | `null`          | -    |
| `successTips`             | 是否显示成功提示                                                                                                      | `Boolean`  | `false`         | -    |
| `successText`             | 成功提示内容                                                                                                          | `String`   | `""`            | -    |
| `successIcon`             | 成功提示图标，[参考](https://uniapp.dcloud.net.cn/api/ui/prompt.html#showtoast)                                       | `String`   | `none`          | -    |
| `successImage`            | 自定义成功提示图标的本地路径，[参考](https://uniapp.dcloud.net.cn/api/ui/prompt.html#showtoast)                       | `String`   | `""`            | -    |
| `successMask`             | 是否显示成功提示的透明蒙层，防止触摸穿透                                                                              | `Boolean`  | `true`          | -    |
| `successDuration`         | 成功提示的持续时间，单位毫秒                                                                                          | `Number`   | `2500`          | -    |
| `successPositon`          | 成功提示的位置，[参考](https://uniapp.dcloud.net.cn/api/ui/prompt.html#showtoast)                                     | `String`   | `""`            | -    |
| `successEndCallback`      | 成功提示结束后的回调函数，参数为接口返回的数据对象和本次请求的最终配置                                                | `Function` | `null`          | -    |
| `successShowFunction`     | 自定义显示成功提示的函数，参数为接口返回的数据对象和本次请求的最终配置                                                | `Function` | `null`          | -    |
| `errorTips`               | 是否显示错误提示                                                                                                      | `Boolean`  | `true`          | -    |
| `errorText`               | 错误提示内容                                                                                                          | `String`   | `""`            | -    |
| `errorIcon`               | 错误提示图标，[参考](https://uniapp.dcloud.net.cn/api/ui/prompt.html#showtoast)                                       | `String`   | `none`          | -    |
| `errorImage`              | 自定义错误提示图标的本地路径，[参考](https://uniapp.dcloud.net.cn/api/ui/prompt.html#showtoast)                       | `String`   | `""`            | -    |
| `errorMask`               | 是否显示错误提示的透明蒙层，防止触摸穿透                                                                              | `Boolean`  | `true`          | -    |
| `errorDuration`           | 错误提示的持续时间，单位毫秒                                                                                          | `Number`   | `2500`          | -    |
| `errorPositon`            | 错误提示的位置，[参考](https://uniapp.dcloud.net.cn/api/ui/prompt.html#showtoast)                                     | `String`   | `""`            | -    |
| `errorEndCallback`        | 错误提示结束后的回调函数，参数为错误信息对象和本次请求的最终配置                                                      | `Function` | `null`          | -    |
| `errorShowFunction`       | 自定义显示错误提示的函数，参数为错误信息对象和本次请求的最终配置                                                      | `Function` | `null`          | -    |
| `failTips`                | 是否显示失败提示                                                                                                      | `Boolean`  | `true`          | -    |
| `failText`                | 失败提示内容                                                                                                          | `String`   | `""`            | -    |
| `failIcon`                | 失败提示图标，[参考](https://uniapp.dcloud.net.cn/api/ui/prompt.html#showtoast)                                       | `String`   | `none`          | -    |
| `failImage`               | 自定义失败提示图标的本地路径，[参考](https://uniapp.dcloud.net.cn/api/ui/prompt.html#showtoast)                       | `String`   | `""`            | -    |
| `failMask`                | 是否显示失败提示的透明蒙层，防止触摸穿透                                                                              | `Boolean`  | `true`          | -    |
| `failDuration`            | 失败提示的持续时间，单位毫秒                                                                                          | `Number`   | `2500`          | -    |
| `failPositon`             | 失败提示的位置，[参考](https://uniapp.dcloud.net.cn/api/ui/prompt.html#showtoast)                                     | `String`   | `""`            | -    |
| `failEndCallback`         | 失败提示结束后的回调函数，参数为错误信息对象和本次请求的最终配置                                                      | `Function` | `null`          | -    |
| `failShowFunction`        | 自定义显示失败提示的函数，参数为错误信息对象和本次请求的最终配置                                                      | `Function` | `null`          | -    |


## 加群交流反馈

### QQ群

<br/>

<Qrcodes :list="list" />

<script setup>
    import { ref } from 'vue';
    import Qrcodes from '../components/Qrcodes.vue';

    // 列表
    const list = ref([
        { text: "1️⃣群", src: "/hi-http/qrcode/qq-group-01.png" }
    ]);
</script>