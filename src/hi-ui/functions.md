---
titleTemplate: hi-ui - 工具函数
layout: doc
---

# 工具函数

`hi-ui` 在开发过程中封装了一些工具函数，工具函数分类如下：

* `common` - 通用工具函数；
* `array` - `Array` 类型数据处理工具函数;
* `date` - `Date` 类型数据处理工具函数;
* `function` - `Function` 类型数据处理工具函数;
* `image` - `Image` 图片处理工具函数集;
* `number` - `Number` 类型数据处理工具函数;
* `object` - `Object` 类型数据处理工具函数;
* `richtext` - 富文本内容处理工具函数;
* `string` - `String` 类型数据处理工具函数;
* `uniapp` - 对 `uni-app` 一些 `api` 的封装，为了方便使用;
* `url` - `URL` 处理工具函数;
* `validate` - `Validate` 验证工具函数;

具体的函数说明，请查看 [工具函数](/hi-ui/functions/common)