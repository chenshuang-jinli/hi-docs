---
titleTemplate: hi-ui - 样式指南
layout: doc
---

# 样式指南

`hi-ui` 的样式设计原则为简介够用，方便修改控制，具体请查看下方的具体说明。


## 组件样式变量

`hi-ui` 中的组件大量使用了样式变量，变量名称通常以 `--hi-{组件名称}-${样式属性名}` 的形式命名，如：

 * `--hi-icon-font-size`
 * `--hi-button-background`

这些变量会直接应用到样式属性值中，并同时设置了一个默认值，如：

```css
/* 图标组件 */
.hi-icon {
    font-size: var(--hi-icon-font-size, inherit);
}

/* 按钮组件 */
.hi-button {
    background: var(--hi-button-background, var(--hi-background-default));
}
```

这样设计是为了更好的在外部控制组件的样式，这是因为设计这些变量是为了占位使用，并没有给这些变量设置值，这样如果没有在外部重新定义这些变量，组件就会使用默认值，如果在外部重新定义这些变量，组件就会使用外部定义的值，例如：

```vue
<template>
    <view class="icons">
        <hi-icon name="wechat"></hi-icon>
        <hi-icon name="douyin"></hi-icon>
    </view>
<template>

<style scoped lang="scss">
    .icons {
        /* 此处就可以通过变量控制组件的样式 */
        --hi-icon-font-size: 20px;
    }
</style>
```

## 主题变量

`hi-ui` 中提供了一组主题变量，具体如下：

```scss
--hi-theme-primary: #409eff; // 主要
--hi-theme-success: #67c23a; // 成功
--hi-theme-warning: #e6a23c; // 警告
--hi-theme-error: #f56c6c; // 错误
--hi-theme-info: #909399; // 信息
```

`hi-ui` 中的一些组件提供了 `theme` 属性来配置主题，例如：

```vue
<template>
    <hi-button text="按钮主题" theme="primary"></hi-button>
    <hi-button text="按钮主题" theme="success"></hi-button>
    <hi-button text="按钮主题" theme="warning"></hi-button>
    <hi-button text="按钮主题" theme="error"></hi-button>
    <hi-button text="按钮主题" theme="info"></hi-button>
</template>
```

除了这些内置主题，用户还可以扩展自己的主题，具体如下：

```scss
page {
    // 通过 --hi-theme-{主题名称} 的方式扩展主题
    --hi-theme-haha: #ff0000;
    --hi-theme-hehe: #ff9900;
}
```

使用扩展的主题：

```vue
<template>
    <hi-button text="扩展的主题" theme="haha"></hi-button>
    <hi-button text="扩展的主题" theme="hehe"></hi-button>
</template>
```

## 全局样式变量

`hi-ui` 中提供尽可能的只定义了一些组件用的到的样式变量，具体如下：

```scss
page {
    // 主题
    --hi-theme-main: #1678ff;
    --hi-theme-primary: #409eff; // 主要
    --hi-theme-success: #67c23a; // 成功
    --hi-theme-warning: #e6a23c; // 警告
    --hi-theme-error: #f56c6c; // 错误
    --hi-theme-info: #909399; // 信息

    // 背景
    --hi-background-default: #e8e8e8;
    --hi-background-element: #ffffff;
    --hi-background-overlay: rgba(0, 0, 0, 0.5); // 遮罩

    /* 文本颜色 */
    --hi-color-main: #333333; // 主文本色
    --hi-color-main-reverse: #ffffff; // 主文本色 - 反转色
    --hi-color-middle: #666666; // 中间文本色
    --hi-color-light: #999999; // 浅色文本
    --hi-color-placeholder: #c4c4c4; // 占位文本

    /* 透明度 */
    --hi-opacity-hover: 0.8; // 点击状态时元素的透明度，使用透明度实现元素点击态效果
    --hi-opacity-disabled: 0.5; // 禁用状态时元素的透明度，使用透明度实现元素禁用态效果

    // 圆角
    --hi-radius-big: 20rpx; // 大圆角
    --hi-radius-default: 10rpx; // 默认圆角
    --hi-radius-small: 5rpx; // 小圆角

    // 边框
    --hi-border-color: #f1f1f1; // 默认边框色
    --hi-border-color-deep: #adadad; // 默认边框色

    // 层级
    --hi-index-default: 698; // 默认层级，比如状态栏、导航栏、底部导航栏等
    --hi-index-upper: 998; // 高层级，比如遮罩、弹窗等，因为 uni-app 弹窗组件的层级为 999，所以这里设置小 1 可以让 hi-ui 的遮罩等元素不遮挡 uni-app 的弹出元素
    --hi-index-lower: 398; // 底层级
}
```


## 辅助样式

同样，`hi-ui` 只提供了一些组件用得到的辅助样式，具体如下：

```scss
// hover-class 默认类
.hi-hover {
    opacity: var(--hi-opacity-hover, 0.8);
}

// disabled 默认类
.hi-disabled {
    opacity: var(--hi-opacity-disabled, 0.5);
    pointer-events: none;
}

// placeholder 默认类
.hi-placeholder {
    color: var(--hi-color-placeholder);
}

/* 超出行数，自动显示行尾省略号，最多10行 */
@for $i from 1 through 10 {
    .hi-line-#{$i} {
        /* #ifdef APP-NVUE */
        // nvue下，可以直接使用lines属性，这是weex特有样式
        lines: $i;
        text-overflow: ellipsis;
        overflow: hidden;
        flex: 1;
        /* #endif */

        /* #ifndef APP-NVUE */
        // vue下，单行和多行显示省略号需要单独处理
        @if $i == "1" {
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        } @else {
            display: -webkit-box !important;
            overflow: hidden;
            text-overflow: ellipsis;
            word-break: break-all;
            -webkit-line-clamp: $i;
            -webkit-box-orient: vertical !important;
        }
        /* #endif */
    }
}

// 旋转动画类
.hi-animate-spin {
    animation-name: hi-ani-spin;
    animation-iteration-count: infinite;
    animation-duration: 1500ms;
    animation-timing-function: linear;
}

// 旋转动画
@keyframes hi-animate-spin {
    0% {
        transform: rotate(0deg);
    }

    100% {
        transform: rotate(360deg);
    }
}

```


## 统一标签

`hi-ui` 统一了 `uni-app` 标签的表现形式，具体为：

```scss
所有标签 {
    // 统一所有标签的内外边距的表现形式
    margin: 0;
    padding: 0;

    // 统一所有标签的盒子模型表现形式
    box-sizing: border-box;
}

image {
    // 统一 image 标签的文字尺寸
    font-size: 0;

    // 页面结构复杂，css样式太多的情况，使用 image 可能导致样式生效较慢，出现 “闪一下” 的情况，此时设置 image{will-change: transform}，可优化此问题。
    will-change: transform;

    // 将 image 标签统一设置为块级元素，因为 inline-block 会有默认间距的问题
    display: block;
}
```