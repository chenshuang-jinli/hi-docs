---
titleTemplate: hi-ui - Tag - 标签
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/tag
pageClass: demo-page
---

# Badge 徽标数

该组件一般用于般用于标记和选择。
<br/>
我们提供了丰富的配置项来尽量满足不同的使用场景。


## 基础使用

通过 `text` 属性设置标签文本。

```vue
<hi-tag text="标签" />
```


## 边框

通过 border 属性设置是否显示边框。

```vue
<hi-tag text="标签" border />
```

* 边框默认宽度：`1px`
* 边框默认颜色：`currentColor`


## 主题

通过 `theme` 属性设置组件主题。

```vue
<hi-tag text="主要" theme="primary" />
<hi-tag text="成功" theme="success" />
<hi-tag text="警告" theme="warning" />
<hi-tag text="错误" theme="error" />
<hi-tag text="信息" theme="info" />
```

> [!TIP] 提示
> 设置主题后，文本颜色会自动设置为 `#ffffff`，如果需要修改，可以通过 `color` 属性设置。


## 镂空

通过 `plain` 属性设置为镂空。
<br/>
设置为镂空后，背景会自动设置为 `transparent`，如果需要修改，可以通过 `bg` 属性设置。
<br/>
同时镂空时会默认显示边框。

```vue
<hi-tag text="默认镂空" plain />
<hi-tag text="主题镂空" plain theme="primary" />
```


## 浅化

通过 `tint` 属性设置浅化背景。
<br/>
浅化背景即是：为组件的背景设置透明度，默认为 `0.2` 透明度。

```vue
<hi-tag text="默认浅化" tint />
<hi-tag text="默认浅化" tint theme="primary" />
```


## 标签图标

通过 `icon` 属性设置为标签图标。

```vue
<hi-tag text="标签图标" icon="__gonggao" />
```


## 关闭按钮

通过 close 属性设置关闭按钮。

```vue
<hi-tag text="显示关闭按钮的标签" close />
```

默认关闭按钮在文字的右侧。可通过 `closeAbsolute` 属性设置为 `true`，将关闭按钮设置为 `absolute` 模式。

```vue
<hi-tag text="显示关闭按钮的标签" close closeAbsolute />
```


## Props

| 属性名          | 说明                 | 类型               | 默认值     | 可选值                    | 版本 |
| :-------------- | :------------------- | :----------------- | :--------- | :------------------------ | :--- |
| `hover`         | 指定按下去的样式类   | `String`           | `hi-hover` | -                         | -    |
| `text`          | 标签文本             | `String`           | -          | -                         | -    |
| `icon`          | 标签图标             | `String`           | -          | -                         | -    |
| `close`         | 是否显示关闭按钮     | `Boolean`          | `false`    | `true`                    | -    |
| `closeIcon`     | 关闭按钮图标         | `String`           | -          | -                         | -    |
| `closeAbsolute` | 关闭按钮是否绝对定位 | `Boolean`          | `false`    | `true`                    | -    |
| `theme`         | 组件的主题           | `String`           | -          | [参考](/hi-ui/style#主题) | -    |
| `plain`         | 是否镂空             | `Boolean`          | `false`    | `true`                    | -    |
| `tint`          | 是否浅化背景         | `Boolean`          | `false`    | `true`                    | -    |
| `tintOpacity`   | 背景透明度           | `[Number, String]` | `0.2`      | -                         | -    |
| `bg`            | 背景                 | `String`           | -          | -                         | -    |
| `color`         | 文本颜色             | `String`           | -          | -                         | -    |
| `fontSize`      | 文本大小             | `String`           | -          | -                         | -    |
| `radius`        | 圆角大小             | `String`           | -          | -                         | -    |
| `width`         | 宽度                 | `String`           | -          | -                         | -    |
| `height`        | 高度                 | `String`           | -          | -                         | -    |
| `border`        | 是否显示边框         | `Boolean`          | `false`    | `true`                    | -    |
| `borderWidth`   | 边框宽度             | `String`           | -          | -                         | -    |
| `borderColor`   | 边框颜色             | `String`           | -          | -                         | -    |
| `borderStyle`   | 边框类型             | `String`           | -          | -                         | -    |


## Event

| 事件名  | 说明             | 回调参数 | 版本 |
| :------ | :--------------- | :------- | :--- |
| `click` | 组件点击事件     | -        | -    |
| `close` | 关闭按钮点击事件 | -        | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                           | 默认值                                         | 版本 |
| :------------------------------- | :--------------------------------------------- | :--- |
| `--hi-tag-display`               | `inline-flex`                                  | -    |
| `--hi-tag-align-items`           | `center`                                       | -    |
| `--hi-tag-padding`               | `0 10px`                                       | -    |
| `--hi-tag-gap`                   | `5px`                                          | -    |
| `--hi-tag-position`              | `relative`                                     | -    |
| `--hi-tag-font-size`             | `0.8em`                                        | -    |
| `--hi-tag-border-radius`         | `3px`                                          | -    |
| `--hi-tag-background`            | `var(--hi-background-default)`                 | -    |
| `--hi-tag-color`                 | `inherit`                                      | -    |
| `--hi-tag-width`                 | `auto`                                         | -    |
| `--hi-tag-height`                | `2em`                                          | -    |
| `--hi-tag-icon-color`            | `inherit`                                      | -    |
| `--hi-tag-icon-font-size`        | `inherit`                                      | -    |
| `--hi-tag-close-position`        | `relative / absolute`                          | -    |
| `--hi-tag-close-top`             | `auto / 0`                                     | -    |
| `--hi-tag-close-right`           | `auto / 0`                                     | -    |
| `--hi-tag-close-bottom`          | `auto`                                         | -    |
| `--hi-tag-close-left`            | `auto`                                         | -    |
| `--hi-tag-close-width`           | `2em`                                          | -    |
| `--hi-tag-close-height`          | `2em`                                          | -    |
| `--hi-tag-close-background`      | `transparent / var(--hi-theme-error, #ff0000)` | -    |
| `--hi-tag-close-display`         | `flex`                                         | -    |
| `--hi-tag-close-flex-direction`  | `column`                                       | -    |
| `--hi-tag-close-align-items`     | `center`                                       | -    |
| `--hi-tag-close-justify-content` | `center`                                       | -    |
| `--hi-tag-close-border-radius`   | `50%`                                          | -    |
| `--hi-tag-close-z-index`         | `8`                                            | -    |
| `--hi-tag-close-color`           | `inherit / #ffffff`                            | -    |
| `--hi-tag-close-font-size`       | `0.6em`                                        | -    |
| `--hi-tag-close-transform`       | `none / translate(50%, -50%)`                  | -    |
| `--hi-tag-close-border-width`    | `1px / 0`                                      | -    |
| `--hi-tag-close-border-style`    | `solid`                                        | -    |
| `--hi-tag-close-border-color`    | `currentColor`                                 | -    |
| `--hi-tag-background-opacity`    | `0.2`                                          | -    |
