---
titleTemplate: hi-ui - Countdown - 倒计时
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/countdown
pageClass: demo-page
---

# Countdown 倒计时

此组件通常用于显示一个倒计时文本，可以设置倒计时级别，支持设置主题、镂空、浅化等。


## 基础使用

通过 `date` 属性设置倒计时结束日期。

```vue
<template>
    <hi-countdown :date="endDate"></hi-countdown>
</template>

<script setup>
    import { ref } from 'vue';

    // 倒计时结束日期
    const endDate = ref(new Date("2035/12/12 12:12:12"));
</script>
```


## 倒计时格式

通过 `format` 属性设置倒计时格式，最大支持“周”，一个完整的示例：`{WW}周{DD}天{HH}时{MM}分{SS}秒{MSS}毫秒`，格式说明：
* `{WW}`: 周，补零；
* `{W}`: 周，不补零
* `{DD}`: 天，补零；
* `{D}`: 天，不补零
* `{HH}`: 小时，补零；
* `{H}`: 小时，不补零
* `{MM}`: 分钟，补零；
* `{M}`: 分钟，不补零
* `{SS}`: 秒，补零；
* `{S}`: 秒，不补零
* `{MSS}`: 毫秒，补零。
* `{MS}`: 毫秒，不补零。


```vue
<template>
    <hi-countdown :date="endDate" format="{WW}周{DD}天{HH}时{MM}分{SS}秒{MSS}毫秒"></hi-countdown>
</template>

<script setup>
    import { ref } from 'vue';

    // 倒计时结束日期
    const endDate = ref(new Date("2035/12/12 12:12:12"));
</script>
```


## 更新周期

通过 `interval` 属性设置倒计时更新周期，单位为毫秒，默认为 `1000`。

```vue
<template>
    <hi-countdown :date="endDate" format="{WW}周{DD}天{HH}时{MM}分{SS}秒{MSS}毫秒" :interval="100"></hi-countdown>
</template>

<script setup>
    import { ref } from 'vue';

    // 倒计时结束日期
    const endDate = ref(new Date("2035/12/12 12:12:12"));
</script>
```


## 结束提示

通过 `tips` 属性自定义倒计时结束文本，默认为空。

```vue
<template>
    <hi-countdown :date="endDate" tips="活动已结束"></hi-countdown>
</template>

<script setup>
    import { ref } from 'vue';

    // 倒计时结束日期
    const endDate = ref(new Date());
</script>
```


## 组件边框

通过 `border` 属性开启组件边框。

```vue
<hi-countdown :date="endDate" border></hi-countdown>
```


## 组件主题

通过 `theme` 属性设置组件主题。

```vue
<hi-countdown :date="endDate" theme="primary"></hi-countdown>
<hi-countdown :date="endDate" theme="success"></hi-countdown>
<hi-countdown :date="endDate" theme="warning"></hi-countdown>
<hi-countdown :date="endDate" theme="error"></hi-countdown>
<hi-countdown :date="endDate" theme="info"></hi-countdown>
```


## 组件镂空

通过 `plain` 属性开启组件镂空。

```vue
<hi-countdown :date="endDate" plain></hi-countdown>
<hi-countdown :date="endDate" plain theme="primary"></hi-countdown>
```


## 组件浅化背景

通过 `tint` 属性开启组件浅化背景。

```vue
<hi-countdown :date="endDate" tint></hi-countdown>
<hi-countdown :date="endDate" tint theme="primary"></hi-countdown>
```


## 数字边框

通过 `numBorder` 属性开启数字边框。

```vue
<hi-countdown :date="endDate" numBorder></hi-countdown>
```


## 数字主题

通过 `numTheme` 属性设置数字主题。

```vue
<hi-countdown :date="endDate" numTheme="primary"></hi-countdown>
<hi-countdown :date="endDate" numTheme="success"></hi-countdown>
<hi-countdown :date="endDate" numTheme="warning"></hi-countdown>
<hi-countdown :date="endDate" numTheme="error"></hi-countdown>
<hi-countdown :date="endDate" numTheme="info"></hi-countdown>
```


## 数字镂空

通过 `numPlain` 属性开启数字镂空。

```vue
<hi-countdown :date="endDate" numPlain></hi-countdown>
<hi-countdown :date="endDate" numPlain numTheme="primary"></hi-countdown>
```


## 数字浅化背景

通过 `numTint` 属性开启数字浅化背景。

```vue
<hi-countdown :date="endDate" numTint></hi-countdown>
<hi-countdown :date="endDate" numTint numTheme="primary"></hi-countdown>
```


## Props

| 属性名           | 说明                 | 类型                     | 默认值                     | 可选值       | 版本 |
| :--------------- | :------------------- | :----------------------- | :------------------------- | :----------- | :--- |
| `date`           | 倒计时结束日期       | `[Date, Number, String]` | `undefined`                | -            | -    |
| `format`         | 格式化字符串         | `String`                 | `{DD}天{HH}时{MM}分{SS}秒` | 详见上方说明 | -    |
| `interval`       | 更新间隔             | `[String, Number]`       | `1000`                     | -            | -    |
| `tips`           | 倒计时结束后的文本   | `String`                 | -                          | -            | -    |
| `color`          | 组件文本颜色         | `String`                 | -                          | -            | -    |
| `fontSize`       | 组件文本大小         | `String`                 | -                          | -            | -    |
| `fontWeight`     | 组件文本粗细         | `String`                 | -                          | -            | -    |
| `gap`            | 内容项的间隔         | `String`                 | -                          | -            | -    |
| `bg`             | 组件背景             | `String`                 | -                          | -            | -    |
| `radius`         | 组件圆角大小         | `String`                 | -                          | -            | -    |
| `padding`        | 组件内边距           | `String`                 | -                          | -            | -    |
| `border`         | 是否显示组件边框     | `Boolean`                | `false`                    | `true`       | -    |
| `theme`          | 组件主题             | `String`                 | -                          | -            | -    |
| `plain`          | 是否开启组件镂空     | `Boolean`                | `false`                    | `true`       | -    |
| `tint`           | 是否开启组件浅化背景 | `Boolean`                | `false`                    | `true`       | -    |
| `tintOpacity`    | 浅化背景透明度       | `[String, Number]`       | `0.2`                      | -            | -    |
| `numFontSize`    | 数字文本大小         | `String`                 | -                          | -            | -    |
| `numColor`       | 数字文本颜色         | `String`                 | -                          | -            | -    |
| `numFontWeight`  | 数字文本粗细         | `String`                 | -                          | -            | -    |
| `numWidth`       | 数字宽度             | `String`                 | -                          | -            | -    |
| `numHeight`      | 数字高度             | `String`                 | -                          | -            | -    |
| `numRadius`      | 数字圆角大小         | `String`                 | -                          | -            | -    |
| `numBorder`      | 是否显示数字边框     | `Boolean`                | `false`                    | `true`       | -    |
| `numBorderWidth` | 数字边框粗细         | `String`                 | -                          | -            | -    |
| `numBorderColor` | 数字边框颜色         | `String`                 | -                          | -            | -    |
| `numBorderStyle` | 数字边框类型         | `String`                 | -                          | -            | -    |
| `numPadding`     | 数字内边距           | `String`                 | -                          | -            | -    |
| `numTheme`       | 数字主题             | `String`                 | -                          | -            | -    |
| `numPlain`       | 是否开启数字镂空     | `Boolean`                | `false`                    | `true`       | -    |
| `numTint`        | 是否开启数字浅化背景 | `Boolean`                | `false`                    | `true`       | -    |
| `numTintOpacity` | 数字浅化背景透明度   | `[String, Number]`       | `0.2`                      | -            | -    |


## Event

| 事件名   | 说明           | 回调参数                              | 版本 |
| :------- | :------------- | :------------------------------------ | :--- |
| `end`    | 倒计时结束事件 | -                                     | -    |
| `change` | 倒计时更新事件 | `countdown`: 倒计时数据，详见下方说明 | -    |

**倒计时数据**

| 属性名         | 说明       | 类型     |
| :------------- | :--------- | :------- |
| `weeks`        | 剩余周数   | `Number` |
| `days`         | 剩余天数   | `Number` |
| `hours`        | 剩余小时数 | `Number` |
| `minutes`      | 剩余分钟数 | `Number` |
| `seconds`      | 剩余秒数   | `Number` |
| `milliseconds` | 剩余毫秒数 | `Number` |


## Slots

| 名称                    | 说明           | 参数                    | 版本 |
| :---------------------- | :------------- | :---------------------- | :--- |
| `weeks`                 | 周数字插槽     | number: 剩余周数        | -    |
| `days`                  | 天数字插槽     | number: 剩余天数        | -    |
| `hours`                 | 小时数字插槽   | number: 剩余小时数      | -    |
| `minutes`               | 分钟数字插槽   | number: 剩余分钟数      | -    |
| `seconds`               | 秒数字插槽     | number: 剩余秒数        | -    |
| `milliseconds`          | 毫秒数字插槽   | number: 剩余毫秒数      | -    |
| `weeksSeparator`        | 周分隔符插槽   | separator: 周分隔符值   | -    |
| `daysSeparator`         | 天分隔符插槽   | separator: 天分隔符值   | -    |
| `hoursSeparator`        | 小时分隔符插槽 | separator: 小时分隔符值 | -    |
| `minutesSeparator`      | 分钟分隔符插槽 | separator: 分钟分隔符值 | -    |
| `secondsSeparator`      | 秒分隔符插槽   | separator: 秒分隔符值   | -    |
| `millisecondsSeparator` | 毫秒分隔符插槽 | separator: 毫秒分隔符值 | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                                     | 默认值                                           | 版本 |
| :----------------------------------------- | :----------------------------------------------- | :--- |
| `--hi-countdown-background`                | `none / var(--hi-countdown-theme, none)`         | -    |
| `--hi-countdown-border-radius`             | `3px`                                            | -    |
| `--hi-countdown-color`                     | `inherit / var(--hi-countdown-theme, inherit)`   | -    |
| `--hi-countdown-font-size`                 | `inherit`                                        | -    |
| `--hi-countdown-font-weight`               | `inherit`                                        | -    |
| `--hi-countdown-display`                   | `inline-flex`                                    | -    |
| `--hi-countdown-padding`                   | `0 / 5px 10px`                                   | -    |
| `--hi-countdown-border-width`              | `0 / 1`                                          | -    |
| `--hi-countdown-border-style`              | `solid`                                          | -    |
| `--hi-countdown-border-color`              | `var(--hi-countdown-theme, currentColor)`        | -    |
| `--hi-countdown-position`                  | `relative`                                       | -    |
| `--hi-countdown-content-display`           | `flex`                                           | -    |
| `--hi-countdown-gap`                       | `3px`                                            | -    |
| `--hi-countdown-background-opacity`        | `0.2`                                            | -    |
| `--hi-countdown-content-align-items`       | `center`                                         | -    |
| `--hi-countdown-content-position`          | `relative`                                       | -    |
| `--hi-countdown-content-z-index`           | `2`                                              | -    |
| `--hi-countdown-number-font-size`          | `inherit`                                        | -    |
| `--hi-countdown-number-color`              | `inherit`                                        | -    |
| `--hi-countdown-number-font-weight`        | `inherit`                                        | -    |
| `--hi-countdown-number-width`              | `auto`                                           | -    |
| `--hi-countdown-number-height`             | `auto`                                           | -    |
| `--hi-countdown-number-border-radius`      | `3px`                                            | -    |
| `--hi-countdown-number-border-width`       | `0 / 1`                                          | -    |
| `--hi-countdown-number-border-style`       | `solid`                                          | -    |
| `--hi-countdown-number-border-color`       | `var(--hi-countdown-number-theme, currentColor)` | -    |
| `--hi-countdown-number-display`            | `flex`                                           | -    |
| `--hi-countdown-number-align-items`        | `center`                                         | -    |
| `--hi-countdown-number-justify-content`    | `center`                                         | -    |
| `--hi-countdown-number-text-align`         | `center`                                         | -    |
| `--hi-countdown-number-background`         | `none / var(--hi-countdown-number-theme, none)`  | -    |
| `--hi-countdown-number-padding`            | `0 / 3px 6px`                                    | -    |
| `--hi-countdown-number-position`           | `relative`                                       | -    |
| `--hi-countdown-number-background-opacity` | `0.2`                                            | -    |