---
titleTemplate: hi-ui - NavigationBar - 导航栏
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/navigation-bar
pageClass: demo-page
---

# NavigationBar 导航栏

此组件通常用于自定义导航栏。


## 基础使用

* 此组件默认高度为 `44px`。

```vue
<hi-navigation-bar></hi-navigation-bar>
```

## 背景

可以通过 `bg` 属性设置导航栏背景，也可以通过 `autoBg` 属性来设置是否开启自动读取在 `pages.json` 中配置的 `navigationBarBackgroundColor` 值用作导航栏背景， `autoBg` 默认为 `true`。

```vue
<hi-navigation-bar autoBg></hi-navigation-bar>
<hi-navigation-bar bg="#ff0000"></hi-navigation-bar>
```


## 设置标题

**标题文本**

可以通过 `title` 属性设置导航栏标题，也可以通过 autoTitle 属性来设置是否自动读取在 `pages.json` 中配置的 `navigationBarTitleText` 值用作导航栏标题， `autoTitle` 默认为 `true`。


**标题颜色**

标题的颜色默认读取 `pages.json` 中配置的 `navigationBarTextStyle` 值用作导航栏标题颜色。也可以通过 `color` 或 `titleColor` 属性自定义设置标题颜色。


```vue
<hi-navigation-bar autoTitle></hi-navigation-bar>
<hi-navigation-bar title="导航栏标题" titleColor="#ff0000"></hi-navigation-bar>
```


**标题对齐方式**

通过 `align` 属性设置标题对齐方式，可选值为：`left`、`center`、`right`。

```vue
<hi-navigation-bar title="标题居左" align="left"></hi-navigation-bar>
<hi-navigation-bar title="标题居中" align="center"></hi-navigation-bar>
<hi-navigation-bar title="标题居右" align="right"></hi-navigation-bar>
```


## 返回按钮

* 通过 `showBack` 属性设置是否开启显示返回按钮。
* 通过 `backText` 属性设置返回文字。

> [!TIP] 提示
> `showBack` 的描述为是否开启显示返回按钮，设置为 `true` 时并不一定会显示返回按钮，这是因为组件还可以设置 `autoShowBack` 并且默认为 `true`，`autoShowBack` 为 `true` 时，表示开启让组件自己判断是否显示返回按钮，判断逻辑为，当 `showBack` 为 `true`，并且当前页面不是 `tabbar` 页面，并且当前页面不是页面栈中的唯一一个页面时才会显示返回按钮。如果需要手动控制返回按钮的显示隐藏，请将 autoShwowBack 设置为 `false` 并通过 `showBack` 手动控制返回按钮的显示状态。


```vue
<hi-navigation-bar title="手动控制返回按钮" :autoShowBack="false" :showBack="true" backText="返回"></hi-navigation-bar>
```


## 自动返回

通过 `autoBack` 设置点击返回按钮后是否自动返回上一页。默认为 `true`。如果在返回上一页之前需要处理其他逻辑，请将 `autoBack` 设置为 `false`，等处理完逻辑后在手动调用 `api` 返回上一页。

```vue
<template>
    <hi-navigation-bar title="标题" :autoBack="false" :autoShowBack="false" :showBack="true" backText="返回" @back="handleBack"></hi-navigation-bar>
</template>

<script setup>
    // 返回事件
    function handleBack() {
        // ... 处理逻辑

        // 返回上一页
        uni.$hiRouter.back();

        // 或
        uni.navigateBack({
            delta: 1
        });
    }
</script>
```


## 菜单

为了应对开发中常见的需求，此组件还提供了菜单栏的功能，通过 `leftMenu` 设置左侧菜单数据，通过 `rightMenu` 设置右侧菜单数据，菜单项数据格式如下示例：

```vue
<template>
    <hi-navigation-bar :leftMenu="menus" :rightMenu="menus"></hi-navigation-bar>
</template>

<script setup>
    import { ref } from 'vue';

    // 菜单数据
    // 菜单数据格式如下，所有属性都为可选，为空则不显示或不设置
    const menus = ref([
        {
            icon: "weixin", // 菜单图标
            text: "微信", // 菜单文本
            disabled: false, // 菜单是否禁用
            class: "navigation-bar-menu--weixin" // 菜单类名
            style: "color: red" // 菜单样式
            hover: "h-hover", // 菜单点击态类名
            iconStyle: "color: red" // 菜单图标样式
            textStyle: "color: red" // 菜单文本样式
        }
    ]);
</script>
```


## Props

| 属性名            | 说明                             | 类型            | 默认值     | 可选值          | 版本 |
| :---------------- | :------------------------------- | :-------------- | :--------- | :-------------- | :--- |
| `hover`           | 指定返回按钮、菜单按下去的样式类 | `String`        | `hi-hover` | -               | -    |
| `title`           | 导航栏标题                       | `String`        | -          | -               | -    |
| `titleColor`      | 导航栏标题颜色                   | `String`        | -`         | -               | -    |
| `titleFontSize`   | 导航栏标题字体大小               | `String`        | -          | -               | -    |
| `titleFontWeight` | 导航栏标题字体粗细               | `String`        | -          | -               | -    |
| `autoTitle`       | 是否自动设置标题                 | `Boolean`       | `true`     | `false`         | -    |
| `align`           | 导航栏标题对齐方式               | `String`        | `center`   | `left`、`right` | -    |
| `showBack`        | 是否显示返回按钮                 | `Boolean`       | `true`     | `false`         | -    |
| `autoShowBack`    | 是否自动显示返回按钮             | `Boolean`       | `true`     | `false`         | -    |
| `autoBack`        | 点击后是否自动返回上一页         | `Boolean`       | `true`     | `false`         | -    |
| `backText`        | 返回按钮文本                     | `String`        | -          | -               | -    |
| `backIcon`        | 返回按钮图标                     | `String`        | `__zuo`    | -               | -    |
| `color`           | 组件文字颜色                     | `String`        | -          | -               | -    |
| `fontSize`        | 组件文字大小                     | `String`        | -          | -               | -    |
| `height`          | 组件高度                         | `String`        | -          | -               | -    |
| `bg`              | 组件背景                         | `String`        | -          | -               | -    |
| `autoBg`          | 是否自动设置背景                 | `Boolean`       | `true`     | `false`         | -    |
| `backFontSize`    | 返回按钮字体大小                 | `String`        | -          | -               | -    |
| `backColor`       | 返回按钮字体颜色                 | `String`        | -          | -               | -    |
| `backFontWeight`  | 返回按钮字体粗细                 | `String`        | -          | -               | -    |
| `backIconColor`   | 返回按钮图标颜色                 | `String`        | -          | -               | -    |
| `backIconSize`    | 返回按钮图标大小                 | `String`        | -          | -               | -    |
| `leftMenu`        | 左侧菜单数据                     | `Array<Object>` | `null`     | -               | -    |
| `rightMenu`       | 右侧菜单数据                     | `Array<Object>` | `null`     | -               | -    |
| `menuColor`       | 菜单字体颜色                     | `String`        | -          | -               | -    |
| `menuFontSize`    | 菜单字体大小                     | `String`        | -          | -               | -    |
| `menuIconColor`   | 菜单图标颜色                     | `String`        | -          | -               | -    |
| `menuIconSize`    | 菜单图标大小                     | `String`        | -          | -               | -    |


## Event

| 事件名           | 说明             | 回调参数                                                                    | 版本 |
| :--------------- | :--------------- | :-------------------------------------------------------------------------- | :--- |
| `back`           | 返回按钮点击事件 | -                                                                           | -    |
| `menuClick`      | 菜单点击事件     | `menu`: 按钮数据。`current`: 按钮下标。`position`: 按钮位置，`left / right` | -    |
| `leftMenuClick`  | 左侧菜单点击事件 | `menu`: 按钮数据。`current`: 按钮下标。                                     | -    |
| `rightMenuClick` | 右侧菜单点击事件 | `menu`: 按钮数据。`current`: 按钮下标。                                     | -    |


## Slots

| 名称    | 说明                         | 参数 | 版本 |
| :------ | :--------------------------- | :--- | :--- |
| `back`  | 返回按钮插槽                 | -    | -    |
| `left`  | 左侧内容插槽，不包含返回按钮 | -    | -    |
| `title` | 标题插槽                     | -    | -    |
| `right` | 右侧内容插槽                 | -    | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                                          | 默认值                  | 版本 |
| :---------------------------------------------- | :---------------------- | :--- |
| `--hi-navigation-bar-height`                    | `44px`                  | -    |
| `--hi-navigation-bar-display`                   | `flex`                  | -    |
| `--hi-navigation-bar-align-items`               | `center`                | -    |
| `--hi-navigation-bar-padding`                   | `0 8px`                 | -    |
| `--hi-navigation-bar-gap`                       | `8px`                   | -    |
| `--hi-navigation-bar-background`                | `transparent`           | -    |
| `--hi-navigation-bar-color`                     | `inherit`               | -    |
| `--hi-navigation-bar-font-size`                 | `inherit`               | -    |
| `--hi-navigation-bar-z-index`                   | `var(--hi-index-upper)` | -    |
| `--hi-navigation-bar-left-flex-shrink`          | `0`                     | -    |
| `--hi-navigation-bar-left-flex`                 | `1 / none`              | -    |
| `--hi-navigation-bar-left-display`              | `flex`                  | -    |
| `--hi-navigation-bar-left-align-items`          | `center`                | -    |
| `--hi-navigation-bar-left-justify-content`      | `flex-start`            | -    |
| `--hi-navigation-bar-left-gap`                  | `0`                     | -    |
| `--hi-navigation-bar-back-font-size`            | `14px`                  | -    |
| `--hi-navigation-bar-back-color`                | `inherit`               | -    |
| `--hi-navigation-bar-back-font-weight`          | `500`                   | -    |
| `--hi-navigation-bar-back-display`              | `flex`                  | -    |
| `--hi-navigation-bar-back-align-items`          | `center`                | -    |
| `--hi-navigation-bar-back-gap`                  | `0`                     | -    |
| `--hi-navigation-bar-back-icon-color`           | `inherit`               | -    |
| `--hi-navigation-bar-back-icon-font-size`       | `22px`                  | -    |
| `--hi-navigation-bar-center-flex-shrink`        | `0`                     | -    |
| `--hi-navigation-bar-center-flex`               | `2 / 1`                 | -    |
| `--hi-navigation-bar-center-display`            | `block`                 | -    |
| `--hi-navigation-bar-title-color`               | `inherit`               | -    |
| `--hi-navigation-bar-title-font-size`           | `17px`                  | -    |
| `--hi-navigation-bar-title-font-weight`         | `700`                   | -    |
| `--hi-navigation-bar-title-text-align`          | `center / left / right` | -    |
| `--hi-navigation-bar-right-flex-shrink`         | `0`                     | -    |
| `--hi-navigation-bar-right-flex`                | `1 / none`              | -    |
| `--hi-navigation-bar-right-display`             | `flex`                  | -    |
| `--hi-navigation-bar-right-align-items`         | `center`                | -    |
| `--hi-navigation-bar-right-justify-content`     | `flex-end`              | -    |
| `--hi-navigation-bar-menu-button-width`         | `0`                     | -    |
| `--hi-navigation-bar-menus-color`               | `inherit`               | -    |
| `--hi-navigation-bar-menus-display`             | `flex`                  | -    |
| `--hi-navigation-bar-menus-align-items`         | `center`                | -    |
| `--hi-navigation-bar-menus-font-size`           | `12px`                  | -    |
| `--hi-navigation-bar-menus-text-align`          | `center`                | -    |
| `--hi-navigation-bar-menus-gap`                 | `10px`                  | -    |
| `--hi-navigation-bar-menu-display`              | `flex`                  | -    |
| `--hi-navigation-bar-menu-flex-direction`       | `column`                | -    |
| `--hi-navigation-bar-menu-flex-align-items`     | `center`                | -    |
| `--hi-navigation-bar-menu-flex-justify-content` | `center`                | -    |
| `--hi-navigation-bar-menu-gap`                  | `2px`                   | -    |
| `--hi-navigation-bar-menu-line-height`          | `1`                     | -    |
| `--hi-navigation-bar-menu-icon-color`           | `inherit`               | -    |
| `--hi-navigation-bar-menu-icon-font-size`       | `18px`                  | -    |