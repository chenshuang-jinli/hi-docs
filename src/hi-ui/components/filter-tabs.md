---
titleTemplate: hi-ui - FilterTabs - 过滤器选项卡
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/filter-tabs
pageClass: demo-page
---

# FilterTabs 过滤器选项卡

此组件通常用于数据筛选场景，通常会有多个选项卡，由于数据筛选场景各不相同，有的是可以多选，有的单选，有的还需要排序，所以此组件只提供样式，具体的逻辑还需用户自行处理。


## 基础使用

* 通过 `list` 属性设置选项卡数据。
* 通过 `value` 属性设置激活项值，`value` 可以是字符串、数字、数组，一般字符串、数字用于单选，数组用于多选。
* 通过 `keyName` 属性设置数据中显示文本的字段名。
* 通过 `valueName` 属性设置绑定值字段的字段名。

```vue
<template>
    <hi-filter-tabs :list="list" keyName="label" valueName="value" :value="values" @itemClick="handleItemClick"></hi-filter-tabs>
</template>

<script setup>
    import { ref } from 'vue';

    // 选项卡数据
    // 所有属性都为可选，为空则不显示或不设置
    /// icon: 图标。
    /// activeIcon: 选中时的图标
    /// label: 显示文本
    /// disabled: 是否禁用。
    /// hover: 选项点击态类名
    /// classes: 选项类名
    /// activeClasses: 选项激活态类名
    /// styles: 选项样式
    /// activeStyles: 选项激活态样式
    const list = ref([
        { value: "1", label: "最新", disabled: false, icon: "", activeIcon: "", hover: "hi-hover", classes: "", activeClasses: "", styles: "", activeStyles: "" },
        { value: "2", label: "价格", disabled: false, icon: "", activeIcon: "", hover: "hi-hover", classes: "", activeClasses: "", styles: "", activeStyles: "" },
        { value: "3", label: "销量", disabled: false, icon: "", activeIcon: "", hover: "hi-hover", classes: "", activeClasses: "", styles: "", activeStyles: "" },
        { value: "4", label: "好评", disabled: false, icon: "", activeIcon: "", hover: "hi-hover", classes: "", activeClasses: "", styles: "", activeStyles: "" },
    ]);

    // 激活项下标
    const values = ref(["1", "2"]);

    // 选项点击事件
    function handleItemClick(item, index) {
        if(values.value.includes(item.value)) {
            values.value.splice(values.value.indexOf(item.value), 1);
        } else {
            values.value.push(item.value);
        }
    }
</script>
```


## 右侧固定项

此组件可以通过 `right` 属性设置右侧固定项，数据结构如下：。

```vue
<template>
    <hi-filter-tabs :right="rightMenus" @rightClick="handleRightClick"></hi-filter-tabs>
</template>

<script setup>
    import { ref } from 'vue';

    // 右侧固定菜单数据
    // 所有属性都为可选，为空则不显示或不设置
    /// icon: 图标。
    /// label: 显示文本
    /// disabled: 是否禁用。
    /// hover: 选项点击态类名
    /// classes: 选项类名
    /// styles: 选项样式
    const rightMenus = ref([
        { label: "筛选", disabled: false, icon: "filter",  hover: "hi-hover", classes: "",  styles: "" },
    ]);

    // 右侧菜单点击事件
    function onRightClick(item, index) {
        console.log(item, index);
    }
</script>
```


## 等分布局

通过 `equal` 属性设置是否等分选项卡进行布局。
<br/>
等分布局通常来说就是选项的宽度相同，选项的内容居中展示。

```vue
<hi-filter-tabs :list="list" :value="values" equal></hi-filter-tabs>
```


## 两端对齐

通过 `justify` 属性设置是否两端对齐选项卡进行布局。
<br/>
两端对齐布局通常来说就是选项靠两侧进行对齐，选项之间的间隔相同。

```vue
<hi-filter-tabs :list="list" :value="values" justify></hi-filter-tabs>
```


## Props

| 属性名             | 说明                   | 类型                      | 默认值      | 可选值 | 版本 |
| :----------------- | :--------------------- | :------------------------ | :---------- | :----- | :--- |
| `hover`            | 指定选项按下去的样式类 | `String`                  | `hi-hover`  | -      | -    |
| `list`             | 选项卡数据             | `Array<Object>`           | `[]`        | -      | -    |
| `value`            | 选中项的值             | `[String, Number, Array]` | `undefined` | -      | -    |
| `keyName`          | 显示文字的字段名       | `String`                  | `label`     | -      | -    |
| `valueName`        | 绑定值值字段名         | `String`                  | `value`     | -      | -    |
| `right`            | 右侧固定菜单数据       | `Array<Object>`           | `[]`        | -      | -    |
| `equal`            | 是否等分布局           | `Boolean`                 | `false`     | `true` | -    |
| `justify`          | 是否两端对齐布局       | `Boolean`                 | `false`     | `true` | -    |
| `color`            | 组件文字颜色           | `String`                  | -           | -      | -    |
| `activeColor`      | 激活项文字颜色         | `String`                  | -           | -      | -    |
| `fontSize`         | 组件文字大小           | `String`                  | -           | -      | -    |
| `activeFontSize`   | 激活项文字大小         | `String`                  | -           | -      | -    |
| `fontWeight`       | 组件文字粗细           | `String`                  | -           | -      | -    |
| `activeFontWeight` | 激活项文字粗细         | `String`                  | -           | -      | -    |
| `bg`               | 组件背景               | `String`                  | -           | -      | -    |
| `radius`           | 组件圆角               | `String`                  | -           | -      | -    |
| `gap`              | 选项间距               | `String`                  | -           | -      | -    |
| `itemPadding`      | 选项内边距             | `String`                  | -           | -      | -    |
| `itemBg`           | 选项背景               | `String`                  | -           | -      | -    |
| `activeItemBg`     | 激活项背景             | `String`                  | -           | -      | -    |
| `itemStyle`        | 选项样式               | `[String, Object, Array]` | -           | -      | -    |
| `activeItemStyle`  | 激活项样式             | `[String, Object, Array]` | -           | -      | -    |
| `iconColor`        | 图标颜色               | `String`                  | -           | -      | -    |
| `activeIconColor`  | 激活项图标颜色         | `String`                  | -           | -      | -    |
| `iconSize`         | 图标大小               | `String`                  | -           | -      | -    |
| `activeIconSize`   | 激活项图标大小         | `String`                  | -           | -      | -    |


## Event

| 事件名       | 说明                 | 回调参数                                  | 版本 |
| :----------- | :------------------- | :---------------------------------------- | :--- |
| `itemClick`  | 选项点击事件         | `item`: 选项数据。`index`: 选项索引。     | -    |
| `rightClick` | 右键固定菜单点击事件 | `item`: 菜单项数据。`index`: 菜单项索引。 | -    |


## Slots

| 名称        | 说明         | 参数 | 版本                                  |
| :---------- | :----------- | :--- | :------------------------------------ |
| `item`      | 选项插槽     | -    | `item`: 选项数据。`index`: 选项索引。 |
| `rightItem` | 右侧选项插槽 | -    | `item`: 选项数据。`index`: 选项索引。 |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                                        | 默认值                       | 版本 |
| :-------------------------------------------- | :--------------------------- | :--- |
| `--hi-filter-tabs-background`                 | `none`                       | -    |
| `--hi-filter-tabs-border-radius`              | `0`                          | -    |
| `--hi-filter-tabs-display`                    | `flex`                       | -    |
| `--hi-filter-tabs-flex-direction`             | `row`                        | -    |
| `--hi-filter-tabs-align-items`                | `stretch`                    | -    |
| `--hi-filter-tabs-gap`                        | `0`                          | -    |
| `--hi-filter-tabs-width`                      | `100%`                       | -    |
| `--hi-filter-tabs-height`                     | `3em`                        | -    |
| `--hi-filter-tabs-text-align`                 | `center`                     | -    |
| `--hi-filter-tabs-color`                      | `inherit`                    | -    |
| `--hi-filter-tabs-active-color`               | `var(--hi-theme-primary)`    | -    |
| `--hi-filter-tabs-font-size`                  | `inherit`                    | -    |
| `--hi-filter-tabs-active-font-size`           | -                            | -    |
| `--hi-filter-tabs-font-weight`                | `inherit`                    | -    |
| `--hi-filter-tabs-active-font-weight`         | -                            | -    |
| `--hi-filter-tabs-left-flex`                  | `1`                          | -    |
| `--hi-filter-tabs-left-overflow`              | `hidden`                     | -    |
| `--hi-filter-tabs-left-position`              | `relative`                   | -    |
| `--hi-filter-tabs-scroll-white-space`         | `nowrap`                     | -    |
| `--hi-filter-tabs-scroll-height`              | `100%`                       | -    |
| `--hi-filter-tabs-scroll-content-height`      | `100%`                       | -    |
| `--hi-filter-tabs-scroll-content-display`     | `flex`                       | -    |
| `--hi-filter-tabs-scroll-content-flex-wrap`   | `nowrap`                     | -    |
| `--hi-filter-tabs-scroll-content-white-space` | `nowrap`                     | -    |
| `--hi-filter-tabs-scroll-content-width`       | `100%`                       | -    |
| `--hi-filter-tabs-items-display`              | `flex`                       | -    |
| `--hi-filter-tabs-items-gap`                  | `15px`                       | -    |
| `--hi-filter-tabs-items-width`                | `100%`                       | -    |
| `--hi-filter-tabs-items-height`               | `100%`                       | -    |
| `--hi-filter-tabs-items-flex-wrap`            | `nowrap`                     | -    |
| `--hi-filter-tabs-items-white-space`          | `nowrap`                     | -    |
| `--hi-filter-tabs-items-flex-direction`       | `row`                        | -    |
| `--hi-filter-tabs-items-justify-content`      | `flex-start / space-between` | -    |
| `--hi-filter-tabs-item-gap`                   | `2px`                        | -    |
| `--hi-filter-tabs-item-position`              | `relative`                   | -    |
| `--hi-filter-tabs-item-display`               | `flex`                       | -    |
| `--hi-filter-tabs-item-flex-direction`        | `row`                        | -    |
| `--hi-filter-tabs-item-align-items`           | `center`                     | -    |
| `--hi-filter-tabs-item-justify-content`       | `center`                     | -    |
| `--hi-filter-tabs-item-transition`            | `100ms`                      | -    |
| `--hi-filter-tabs-item-flex-shrink`           | `0`                          | -    |
| `--hi-filter-tabs-item-background`            | `none`                       | -    |
| `--hi-filter-tabs-item-active-background`     | -                            | -    |
| `--hi-filter-tabs-item-padding`               | `0 5px`                      | -    |
| `--hi-filter-tabs-item-flex`                  | `none / 1`                   | -    |
| `--hi-filter-tabs-item-width`                 | `auto`                       | -    |
| `--hi-filter-tabs-item-icon-color`            | `inherit`                    | -    |
| `--hi-filter-tabs-item-active-icon-color`     | -                            | -    |
| `--hi-filter-tabs-item-icon-font-size`        | `1.25em`                     | -    |
| `--hi-filter-tabs-item-active-icon-font-size` | -                            | -    |
| `--hi-filter-tabs-right-flex-shrink`          | `0`                          | -    |
| `--hi-filter-tabs-right-display`              | `flex`                       | -    |
| `--hi-filter-tabs-right-align-items`          | `center`                     | -    |
| `--hi-filter-tabs-right-font-size`            | `inherit`                    | -    |
| `--hi-filter-tabs-right-font-weight`          | `inherit`                    | -    |
| `--hi-filter-tabs-right-color`                | `inherit`                    | -    |
| `--hi-filter-tabs-right-gap`                  | `10px`                       | -    |