---
titleTemplate: hi-ui - Empty - 空状态
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/empty
pageClass: demo-page
---

# Empty 空状态

此组件通常用于数据为空的场景。


## 图标

默认图标为 `__kongshuju`，通过 `icon` 属性可以修改图标名称。


```vue
<hi-empty icon="empty"></hi-empty>
```


## 提示文本

默认提示文本为 `"Ops! 暂无数据~"`，通过 `tips` 属性可以修改提示文本。

```vue
<hi-empty tips="暂无数据~"></hi-empty>
```


## 背景

该组件默认透明背景，通过 `bg` 属性可以修改背景。

```vue
<hi-empty bg="#ffffff"></hi-empty>
```


## 按钮

为了方便使用，该组件还定义了一个按钮，用于常见的返回首页等场景。通过 `showBtn` 属性可以控制是否显示按钮。该组件定义了一些属性用来定义按钮样式，具体查看下方的属性说明。

```vue
<hi-empty showBtn></hi-empty>
```


## Props

| 属性名         | 说明                   | 类型      | 默认值           | 可选值                                     | 版本 |
| :------------- | :--------------------- | :-------- | :--------------- | :----------------------------------------- | :--- |
| `icon`         | 图标名称               | `String`  | `__kongshuju`    | -                                          | -    |
| `tips`         | 提示文本               | `String`  | `Ops! 暂无数据~` | -                                          | -    |
| `mode`         | 图片图标的裁剪模式     | `String`  | -                | -                                          | -    |
| `height`       | 组件高度               | `String`  | -                | -                                          | -    |
| `bg`           | 组件背景               | `String`  | -                | -                                          | -    |
| `radius`       | 组件圆角大小           | `String`  | -                | -                                          | -    |
| `color`        | 组件文字颜色           | `String`  | -                | -                                          | -    |
| `fontSize`     | 组件文字大小           | `String`  | -                | -                                          | -    |
| `iconColor`    | 图标颜色               | `String`  | -                | -                                          | -    |
| `iconFontSize` | 图标文字大小           | `String`  | -                | -                                          | -    |
| `showBtn`      | 是否显示按钮           | `Boolean` | `false`          | `true`                                     | -    |
| `btnText`      | 按钮文本               | `String`  | `继续逛逛`       | -                                          | -    |
| `btnTheme`     | 按钮主题               | `String`  | `primary`        | -                                          | -    |
| `btnTop`       | 按钮距离上方元素的距离 | `String`  | -                | -                                          | -    |
| `btnWidth`     | 按钮宽度               | `String`  | -                | -                                          | -    |
| `btnHeight`    | 按钮高度               | `String`  | -                | -                                          | -    |
| `btnBg`        | 按钮背景               | `String`  | -                | -                                          | -    |
| `btnColor`     | 按钮文字颜色           | `String`  | -                | -                                          | -    |
| `btnFontSize`  | 按钮文字大小           | `String`  | -                | -                                          | -    |
| `btnRadius`    | 按钮圆角大小           | `String`  | -                | -                                          | -    |
| `btnOpts`      | 按钮配置               | `Object`  | `{}`             | [参考 hi-button](/hi-ui/components/button) | -    |



## Event

| 事件名  | 说明               | 回调参数 | 版本 |
| :------ | :----------------- | :------- | :--- |
| `click` | 组件内按钮点击事件 | -        | -    |


## Slots

| 名称     | 说明     | 参数 | 版本 |
| :------- | :------- | :--- | :--- |
| `button` | 按钮插槽 | -    | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                       | 默认值                  | 版本 |
| :--------------------------- | :---------------------- | :--- |
| `--hi-empty-display`         | `flex`                  | -    |
| `--hi-empty-flex-direction`  | `column`                | -    |
| `--hi-empty-align-items`     | `center`                | -    |
| `--hi-empty-justify-content` | `center`                | -    |
| `--hi-empty-height`          | `500rpx`                | -    |
| `--hi-empty-color`           | `var(--hi-color-light)` | -    |
| `--hi-empty-font-size`       | `24rpx`                 | -    |
| `--hi-empty-gap`             | `10px`                  | -    |
| `--hi-empty-background`      | `transparent`           | -    |
| `--hi-empty-border-radius`   | `10px`                  | -    |
| `--hi-empty-icon-color`      | `inherit`               | -    |
| `--hi-empty-icon-font-size`  | `100rpx`                | -    |
| `--hi-empty-button-top`      | `30rpx`                 | -    |