---
titleTemplate: hi-ui - Gap - 间隔
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/gap
pageClass: demo-page
---

# Gap 间隔

此组件只是为了快速的构建页面布局。


## 基础使用

此组件默认无背景。默认宽高 `12px`。

```vue
<hi-gap></hi-gap>
```


## 设置间隔大小

通过 `size` 属性设置间隔大小（宽/高）。
<br/>
为了处理一些特殊的布局，`size` 支持设置成负数。

> [!TIP]
> 如果在小程序中组件存在虚拟节点，由于虚拟节点的存在，负值可能不会生效。

```vue
<hi-gap size="30px"></hi-gap>
<hi-gap size="-30px"></hi-gap>
```


## 行内模式

组件默认为块级元素，通过 `inline` 属性设置为行内模式。

```vue
<hi-gap size="30px" inline></hi-gap>
```


## Props

| 属性名   | 说明             | 类型      | 默认值  | 可选值 | 版本 |
| :------- | :--------------- | :-------- | :------ | :----- | :--- |
| `size`   | 间隔大小         | `String`  | -       | -      | -    |
| `inline` | 是否开启行内模式 | `Boolean` | `false` | `true` | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名             | 默认值                | 版本 |
| :----------------- | :-------------------- | :--- |
| `--hi-gap-size`    | `0 / 12px`            | -    |
| `--hi-gap-display` | `block / inline-flex` | -    |