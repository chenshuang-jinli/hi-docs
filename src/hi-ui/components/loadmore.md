---
titleTemplate: hi-ui - LoadMore - 加载更多
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/loadmore
pageClass: demo-page
---

# LoadMore 加载更多

此组件一般用于标识加载数据的状态，共有三种状态：

* `loadmore`: 加载更多
* `loading`: 加载中
* `nomore`: 没有更多数据


## 加载状态

通过 `status` 属性设置加载状态。

* `loadmore`: 加载更多
* `loading`: 加载中
* `nomore`: 没有更多数据

```vue
<hi-loadmore status="loadmore" />
<hi-loadmore status="loading" />
<hi-loadmore status="nomore" />
```


## 状态文字

* 通过 `more` 设置加载更多提示文本。
* 通过 `loading` 设置加载中提示文本。
* 通过 `nomore` 设置没有更多数据提示文本。

```vue
<hi-loadmore status="loadmore" more="加载更多" />
<hi-loadmore status="loading" loading="加载中..." />
<hi-loadmore status="nomore" nomore="没有更多了" />
```


## 加载图标

通过 `icon` 设置加载图标。

```vue
<hi-loadmore status="loading" loading="加载中..." icon="__loading" />
```


## 文字颜色

通过 `color` 属性设置文字颜色。

```vue
<hi-loadmore status="loading" color="#ff0000" />
```


## 文字大小

通过 `fontSize` 属性设置为文字大小。


```vue
<hi-loadmore status="loading" fontSize="1em" />
```


## 纵向布局

通过设置 `vertical` 属性设置是否开启纵向布局。

```vue
<hi-loadmore status="loading" vertical />
```


## Props

| 属性名     | 说明                                | 类型      | 默认值           | 可选值                          | 版本 |
| :--------- | :---------------------------------- | :-------- | :--------------- | :------------------------------ | :--- |
| `hover`    | 指定按下去的样式类                  | `String`  | -                | -                               | -    |
| `status`   | 状态                                | `String`  | `loading`        | `loadmore`, `loading`, `nomore` | -    |
| `more`     | `status` 为 `loadmore` 时的提示文本 | `String`  | `加载更多`       | -                               | -    |
| `loading`  | `status` 为 `loading` 时的提示文本  | `String`  | `正在加载...`    | -                               | -    |
| `nomore`   | `status` 为 `nomore` 时的提示文本   | `String`  | `— 没有更多了 —` | -                               | -    |
| `icon`     | `loading` 图标                      | `String`  | `__loading`      | -                               | -    |
| `color`    | 文本颜色                            | `String`  | -                | -                               | -    |
| `fontSize` | 文本大小                            | `String`  | -                | -                               | -    |
| `vertical` | 纵向布局                            | `Boolean` | `false`          | -                               | -    |


## Event

| 事件名 | 说明                                | 回调参数 | 版本 |
| :----- | :---------------------------------- | :------- | :--- |
| `more` | `status` 为 `loadmore` 时的点击事件 | -        | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                                    | 默认值         | 版本 |
| :---------------------------------------- | :------------- | :--- |
| `--hi-loadmore-display`                   | `flex`         | -    |
| `--hi-loadmore-align-items`               | `center`       | -    |
| `--hi-loadmore-flex-direction`            | `row`          | -    |
| `--hi-loadmore-justify-content`           | `center`       | -    |
| `--hi-loadmore-color`                     | `inherit`      | -    |
| `--hi-loadmore-font-size`                 | `0.8em`        | -    |
| `--hi-loadmore-body-display`              | `flex`         | -    |
| `--hi-loadmore-body-align-items`          | `center`       | -    |
| `--hi-loadmore-body-justify-content`      | `center`       | -    |
| `--hi-loadmor-body-flex-direction`        | `row / column` | -    |
| `--hi-loadmore-body-gap`                  | `5px`          | -    |
| `--hi-loadmore-icon-font-size`            | `1.5em`        | -    |
| `--hi-loadmore-icon-color`                | `inherit`      | -    |
| `--hi-loadmore-animation-duration`        | `1.5s`         | -    |
| `--hi-loadmore-animation-iteration-count` | `infinite`     | -    |
| `--hi-loadmore-animation-timing-function` | `linear`       | -    |
