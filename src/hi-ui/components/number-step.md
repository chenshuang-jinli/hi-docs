---
titleTemplate: hi-ui - NumberStep - 步进器
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/number-step
pageClass: demo-page
---

# NumberStep 步进器

此组件通常用于商品计数等场景。


## 基础使用

通过 `v-model` 绑定步进器的值。

```vue
<template>
    <hi-number-step v-model="number"></hi-number-step>
</template>

<script steup>
    import { ref } from "vue";

    // 数量
    const number = ref(1);
</script>
```


## 最大值/最小值

通过 `max` 和 `min` 属性设置最大值和最小值。

```vue
<hi-number-step v-model="number" :min="-10" :max="10"></hi-number-step>
```


## 步近幅度

通过 `step` 属性设置步进幅度，支持小数。

```vue
<hi-number-step v-model="number" :step="3"></hi-number-step>
```


## 整体禁用

通过 `disabled` 属性禁用整个组件。


```vue
<hi-number-step v-model="number" disabled></hi-number-step>
```


## 禁用减号按钮

通过 `disabledMinus` 属性禁用减号按钮。

```vue
<hi-number-step v-model="number" disabledMinus></hi-number-step>
```


## 禁用加号按钮

通过 `disabledPlus` 属性禁用加号按钮。

```vue
<hi-number-step v-model="number" disabledPlus></hi-number-step>
```


## 禁用输入框

通过 `disabledInput` 属性禁用输入框。

```vue
<hi-number-step v-model="number" disabledInput></hi-number-step>
```


## 异步变更

通过 `async` 属性设置是否开启异步变更。
<br/>
开启异步变更后，会在点击加/减按钮后触发 `asyncChange` 事件，需手动更新步进器值。

```vue
<template>
    <hi-number-step v-model="number" async @asyncChange="onAsyncChange"></hi-number-step>
</template>

<script steup>
    import { ref } from "vue";

    // 数量
    const number = ref(1);

    // 异步变更事件
    function onAsyncChange() {
        console.log("异步变更事件");
        number.value++;
    }
</script>
```


## 背景

通过 `bg` 属性设置组件背景。

```vue
<hi-number-step v-model="number" bg="#ffffff"></hi-number-step>
```


## 边框

通过 `border` 属性设置是否显示边框，默认为 `true`。

```vue
<hi-number-step v-model="number" :border="false"></hi-number-step>
```


## 边框颜色

通过 `borderColor` 属性设置边框的颜色。

```vue
<hi-number-step v-model="number" borderColor="blue"></hi-number-step>
```


## 聚焦边框颜色

通过 `focusBorderColor` 属性设置聚焦时边框的颜色。

```vue
<hi-number-step v-model="number" focusBorderColor="red"></hi-number-step>
```


## Props

| 属性名             | 说明                        | 类型      | 默认值      | 可选值  | 版本 |
| :----------------- | :-------------------------- | :-------- | :---------- | :------ | :--- |
| `hover`            | 指定加/减按钮按下去的样式类 | `String`  | `hi-hover`  | -       | -    |
| `v-model`          | 绑定值                      | `Number`  | `1`         | -       | -    |
| `step`             | 步进幅度                    | `Number`  | `1`         | -       | -    |
| `max`              | 最大值                      | `Number`  | `Infinity`  | -       | -    |
| `min`              | 最小值                      | `Number`  | `-Infinity` | -       | -    |
| `disabledMinus`    | 是否禁用减号按钮            | `Boolean` | `false`     | `true`  | -    |
| `disabledPlus`     | 是否禁用加号按钮            | `Boolean` | `false`     | `true`  | -    |
| `disabledInput`    | 是否禁用输入框              | `Boolean` | `false`     | `true`  | -    |
| `async`            | 是否开启异步变更            | `Boolean` | `false`     | `true`  | -    |
| `showMinus`        | 是否显示减号按钮            | `Boolean` | `true`      | `false` | -    |
| `showPlus`         | 是否显示加号按钮            | `Boolean` | `true`      | `false` | -    |
| `minusIcon`        | 减号按钮的图标              | `String`  | `__jian`    | -       | -    |
| `plusIcon`         | 加号按钮的图标              | `String`  | `__jia`     | -       | -    |
| `width`            | 组件宽度                    | `String`  | -           | -       | -    |
| `height`           | 组件高度                    | `String`  | -           | -       | -    |
| `radius`           | 组件圆角大小                | `String`  | -           | -       | -    |
| `color`            | 组件字体颜色                | `String`  | -           | -       | -    |
| `fontSize`         | 组件字体大小                | `String`  | -           | -       | -    |
| `bg`               | 组件背景                    | `String`  | -           | -       | -    |
| `border`           | 是否显示边框                | `Boolean` | `true`      | `false` | -    |
| `borderColor`      | 边框颜色                    | `String`  | -           | -       | -    |
| `focusBorderColor` | 聚焦时边框颜色              | `String`  | -           | -       | -    |
| `borderWidth`      | 边框宽度                    | `String`  | -           | -       | -    |
| `align`            | 文本对齐方式                | `String`  | -           | -       | -    |
| `iconColor`        | 按钮图标颜色                | `String`  | -           | -       | -    |
| `iconSize`         | 按钮图标大小                | `String`  | -           | -       | -    |
| `iconWidth`        | 按钮宽度                    | `String`  | -           | -       | -    |
| `iconBg`           | 按钮背景                    | `String`  | -           | -       | -    |
| `plusColor`        | 加号按钮颜色                | `String`  | -           | -       | -    |
| `minusColor`       | 减号按钮颜色                | `String`  | -           | -       | -    |
| `plusBg`           | 加号按钮背景                | `String`  | -           | -       | -    |
| `minusBg`          | 减号按钮背景                | `String`  | -           | -       | -    |
| `inputWidth`       | 输入框宽度                  | `String`  | -           | -       | -    |
| `inputBg`          | 输入框背景                  | `String`  | -           | -       | -    |


## Event

| 事件名                 | 说明                             | 回调参数                                                           | 版本 |
| :--------------------- | :------------------------------- | :----------------------------------------------------------------- | :--- |
| `change`               | 值更新事件                       | `newValue`: 更新后的值                                             | -    |
| `asyncChange`          | 异步变更事件                     | `newValue`: 目标（更新后）的值                                     | -    |
| `plusClick`            | 加号按钮点击事件                 | -                                                                  | -    |
| `minusClick`           | 减号按钮点击事件                 | -                                                                  | -    |
| `input`                | 当键盘输入时触发                 | `event`: [参考](https://uniapp.dcloud.net.cn/component/input.html) | -    |
| `focus`                | 输入框聚焦时触发                 | `event`: [参考](https://uniapp.dcloud.net.cn/component/input.html) | -    |
| `blur`                 | 输入框失去焦点时触发             | `event`: [参考](https://uniapp.dcloud.net.cn/component/input.html) | -    |
| `confirm`              | 点击完成按钮时触发               | `event`: [参考](https://uniapp.dcloud.net.cn/component/input.html) | -    |
| `keyboardheightchange` | 键盘高度发生变化的时候触发此事件 | `event`: [参考](https://uniapp.dcloud.net.cn/component/input.html) | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                                       | 默认值                                                                                | 版本 |
| :------------------------------------------- | :------------------------------------------------------------------------------------ | :--- |
| `--hi-number-step-transition`                | `border 0.2s`                                                                         | -    |
| `--hi-number-step-display`                   | `flex`                                                                                | -    |
| `--hi-number-step-height`                    | `2em`                                                                                 | -    |
| `--hi-number-step-border-width`              | `0 / 1px`                                                                             | -    |
| `--hi-number-step-border-style`              | `solid`                                                                               | -    |
| `--hi-number-step-border-color`              | `var(--hi-border-color) / var(--hi-number-step-border-color, var(--hi-border-color))` | -    |
| `--hi-number-step-overflow`                  | `hidden`                                                                              | -    |
| `--hi-number-step-align-items`               | `stretch`                                                                             | -    |
| `--hi-number-step-width`                     | `auto`                                                                                | -    |
| `--hi-number-step-border-radius`             | `0`                                                                                   | -    |
| `--hi-number-step-color`                     | `inherit`                                                                             | -    |
| `--hi-number-step-font-size`                 | `inherit`                                                                             | -    |
| `--hi-number-step-background`                | `var(--hi-background-default)`                                                        | -    |
| `--hi-number-step-icon-color`                | `inherit`                                                                             | -    |
| `--hi-number-step-icon-size`                 | `inherit`                                                                             | -    |
| `--hi-number-step-icon-flex-shrink`          | `0`                                                                                   | -    |
| `--hi-number-step-icon-display`              | `flex`                                                                                | -    |
| `--hi-number-step-icon-align-items`          | `center`                                                                              | -    |
| `--hi-number-step-icon-justify-content`      | `center`                                                                              | -    |
| `--hi-number-step-icon-width`                | `2em`                                                                                 | -    |
| `--hi-number-step-plus-icon-color`           | `var(--hi-number-step-icon-color, inherit)`                                           | -    |
| `--hi-number-step-plus-icon-background`      | `var(--hi-number-step-icon-background, transparent)`                                  | -    |
| `--hi-number-step-minus-icon-color`          | `var(--hi-number-step-icon-color, inherit)`                                           | -    |
| `--hi-number-step-minus-icon-background`     | `var(--hi-number-step-icon-background, transparent)`                                  | -    |
| `--hi-number-step-input-width`               | `4em`                                                                                 | -    |
| `--hi-number-step-input-text-align`          | `center`                                                                              | -    |
| `--hi-number-step-input-border-left-width`   | `0 / 1px`                                                                             | -    |
| `--hi-number-step-input-border-right-width`  | `0 / 1px`                                                                             | -    |
| `--hi-number-step-input-border-top-width`    | `0`                                                                                   | -    |
| `--hi-number-step-input-border-bottom-width` | `0`                                                                                   | -    |
| `--hi-number-step-input-flex`                | `1`                                                                                   | -    |
| `--hi-number-step-input-font-size`           | `inherit`                                                                             | -    |
| `--hi-number-step-input-color`               | `inherit`                                                                             | -    |
| `--hi-number-step-input-background`          | `transparent`                                                                         | -    |