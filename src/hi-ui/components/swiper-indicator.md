---
titleTemplate: hi-ui - SwiperIndicator - 轮播指示器
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/swiper-indicator
pageClass: demo-page
---

# SwiperIndicator 轮播指示器

此组件封装了一些常见的轮播指示器样式，目前支持的有：
* `dots` - 点指示器。
* `line` - 线指示器。


## 基础使用

* 通过 `mode` 属性设置指示器类型，可选值有：
  * `dots` - 点指示器，默认值。
  * `line` - 线指示器。
* 通过 `count` 属性设置轮播 `item` 数量。
* 通过 `current` 设置当前激活项下标。

```vue
<template>
    <hi-swiper :list="list" keyName="url" @change="onSwiperChange"></hi-swiper>
    <hi-swiper-indicator :count="list.length" :current="current"></hi-swiper-indicator>
</template>

<script setup>
    import { ref } from "vue";

    // 轮播数据
    const list = ref([
        { id: 1, url: "/static/images/temp.jpeg" },
        { id: 2, url: "/static/images/temp.jpeg" },
        { id: 3, url: "/static/images/temp.jpeg" },
        { id: 4, url: "/static/images/temp.jpeg" },
        { id: 5, url: "/static/images/temp.jpeg" }
    ]);

    // 当前下标
    const current = ref(0);

    // 轮播切换事件
    function onSwiperChange(event) {
        current.value = event.detail.current;
    }
</script>
```


## 内部使用

默认组件的定位方式为 `relative`，可以通过 `absolute` 属性开启绝对定位。
<br/>
可以结合自定义 `swiper` 或 `hi-swiper` 组件一起使用。

```vue
<template>
    <!-- 结合自定义 swiper 使用 -->
    <view class="swiper-container">
        <swiper class="swiper" :current="current" @change="onSwiperChange" autoplay>
            <swiper-item v-for="item in list" :key="item.id">
                <image :src="item.url" mode="aspectFill" class="swiper-image"></image>
            </swiper-item>
        <swiper>
        <hi-swiper-indicator :count="list.length" :current="current" absolute></hi-swiper-indicator>
    <view>

    <!-- 结合 hi-swiper 使用 -->
    <hi-swiper :list="list" keyName="url">
        <template #indicator="{current: index, count}">
            <hi-swiper-indicator :count="count" :current="index" absolute></hi-swiper-indicator>
        </template>
    </hi-swiper>
</template>

<script setup>
    import { ref } from "vue";

    // 轮播数据
    const list = ref([
        { id: 1, url: "/static/images/temp.jpeg" },
        { id: 2, url: "/static/images/temp.jpeg" },
        { id: 3, url: "/static/images/temp.jpeg" },
        { id: 4, url: "/static/images/temp.jpeg" },
        { id: 5, url: "/static/images/temp.jpeg" }
    ]);

    // 当前下标
    const current = ref(0);

    // 轮播切换事件
    function onSwiperChange(event) {
        current.value = event.detail.current;
    }
</script>

<style scoped>
.swiper-container {
    width: 100%;
    height: 220px;
    position: relative;
}

.swiper-image {
    width: 100%;
    height: 100%；
    display: block;
}
</style>
```


## 指示器位置

开启 absoulte 属性后，可以通过 `position` 属性设置指示器的位置，可选值有：
* `bottom-center`: 底部居中，默认值。
* `bottom-left`: 底部左侧。
* `bottom-right`: 底部右侧。
* `left-center`: 左侧居中。
* `left-bottom`: 左侧局下。
* `right-center`: 右侧居中。
* `right-bottom`: 右侧局下。

```vue
<template>
    <hi-swiper :list="list" keyName="url">
        <template #indicator="{index, count}">
            <hi-swiper-indicator :count="count" :current="index" absolute :position="position"></hi-swiper-indicator>
        </template>

        <hi-button text="bottom-center" @click="handleChangePosition('bottom-center')"></hi-button>
        <hi-button text="bottom-left" @click="handleChangePosition('bottom-left')"></hi-button>
        <hi-button text="bottom-right" @click="handleChangePosition('bottom-right')"></hi-button>
        <hi-button text="left-center" @click="handleChangePosition('left-center')"></hi-button>
        <hi-button text="left-bottom" @click="handleChangePosition('left-bottom')"></hi-button>
        <hi-button text="right-center" @click="handleChangePosition('right-center')"></hi-button>
        <hi-button text="right-bottom" @click="handleChangePosition('right-bottom')"></hi-button>
    </hi-swiper>
</template>

<script setup>
    import { ref } from "vue";

    // 轮播数据
    const list = ref([
        { id: 1, url: "/static/images/temp.jpeg" },
        { id: 2, url: "/static/images/temp.jpeg" },
        { id: 3, url: "/static/images/temp.jpeg" },
        { id: 4, url: "/static/images/temp.jpeg" },
        { id: 5, url: "/static/images/temp.jpeg" }
    ]);

    // 指示器位置
    const position = ref("bottom-center");

    // 切换指示器位置
    function handleChangePosition(_position) {
        position.value = _position;
    }
</script>
```


## Props

| 属性名          | 说明                            | 类型      | 默认值  | 可选值       | 版本 |
| :-------------- | :------------------------------ | :-------- | :------ | :----------- | :--- |
| `count`         | 指示器 `item` 数量              | `Number`  | `0`     | -            | -    |
| `current`       | 当前激活项下标                  | `Number`  | `0`     | -            | -    |
| `mode`          | 指示器类型                      | `String`  | `dots`  | 参考上方说明 | -    |
| `position`      | 指示器位置                      | `String`  | -       | 参考上方说明 | -    |
| `absolute`      | 是否开启绝对定位                | `Boolean` | `false` | `true`       | -    |
| `top`           | 绝对定位时顶部偏移量            | `String`  | -       | -            | -    |
| `left`          | 绝对定位时左侧偏移量            | `String`  | -       | -            | -    |
| `right`         | 绝对定位时右侧偏移量            | `String`  | -       | -            | -    |
| `bottom`        | 绝对定位时底部偏移量            | `String`  | -       | -            | -    |
| `inactiveColor` | 未激活颜色                      | `String`  | -       | -            | -    |
| `activeColor`   | 激活颜色                        | `String`  | -       | -            | -    |
| `radius`        | 圆角大小                        | `String`  | -       | -            | -    |
| `weight`        | 指示器粗细                      | `String`  | -       | -            | -    |
| `gap`           | `item` 之间的间距               | `String`  | -       | -            | -    |
| `inactiveSize`  | 点指示器未激活时大小            | `String`  | -       | -            | -    |
| `activeSize`    | 点指示器激活时大小              | `String`  | -       | -            | -    |
| `lineSize`      | 线指示器整体宽度/高度           | `String`  | -       | -            | -    |
| `lineItemSize`  | 线指示器单个 `item` 的宽度/高度 | `String`  | -       | -            | -    |


## Event

| 事件名      | 说明            | 回调参数                              | 版本 |
| :---------- | :-------------- | :------------------------------------ | :--- |
| `itemClick` | `item` 点击事件 | `index`: 当前点击的 `item` 的索引值。 | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                                       | 默认值                                                                | 版本 |
| :------------------------------------------- | :-------------------------------------------------------------------- | :--- |
| `--hi-swiper-indicator-display`              | `flex`                                                                | -    |
| `--hi-swiper-indicator-align-items`          | `center`                                                              | -    |
| `--hi-swiper-indicator-justify-content`      | `center`                                                              | -    |
| `--hi-swiper-indicator-position`             | `relative`                                                            | -    |
| `--hi-swiper-indicator-z-index`              | `10`                                                                  | -    |
| `--hi-swiper-indicator-bottom`               | `auto / 10px`                                                         | -    |
| `--hi-swiper-indicator-left`                 | `auto / 50% / 10px`                                                   | -    |
| `--hi-swiper-indicator-right`                | `auto / 10px`                                                         | -    |
| `--hi-swiper-indicator-top`                  | `auto / 50%`                                                          | -    |
| `--hi-swiper-indicator-transform`            | `initial / translate(-50%, 0) / translate(0, 0) / translate(0, -50%)` | -    |
| `--hi-swiper-indicator-dots-display`         | `flex`                                                                | -    |
| `--hi-swiper-indicator-dots-align-items`     | `center`                                                              | -    |
| `--hi-swiper-indicator-dots-justify-content` | `center`                                                              | -    |
| `--hi-swiper-indicator-gap`                  | `8px`                                                                 | -    |
| `--hi-swiper-indicator-dots-flex-direction`  | `row / column`                                                        | -    |
| `--hi-swiper-indicator-border-radius`        | `3px`                                                                 | -    |
| `--hi-swiper-indicator-inactive-color`       | `var(--hi-background-default)`                                        | -    |
| `--hi-swiper-indicator-transition`           | `500ms`                                                               | -    |
| `--hi-swiper-indicator-active-color`         | `var(--hi-theme-primary)`                                             | -    |
| `--hi-swiper-indicator-inactive-size`        | `8px / 3px`                                                           | -    |
| `--hi-swiper-indicator-weight`               | `3px / 8px`                                                           | -    |
| `--hi-swiper-indicator-active-size`          | `16px`                                                                | -    |
| `--hi-swiper-indicator-line-size`            | `25% / 40%`                                                           | -    |