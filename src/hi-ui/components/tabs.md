---
titleTemplate: hi-ui - Tabs - 选项卡
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/tabs
pageClass: demo-page
---

# Tabs 选项卡

一个普通的标签选项卡组件，支持激活项自动滚动到中间位置。


## 基础使用

* 通过 `list` 属性设置选项卡数据。
* 通过 `keyName` 属性设置数据中显示文本的字段名。
* 通过 `current` 属性设置激活项下标。

```vue
<template>
    <hi-tabs :list="tabs" keyName="name" :current="current" @change="onChange"></hi-tabs>
</template>

<script setup>
    import { ref } from 'vue';

    // 选项卡数据
    // 所有属性都为可选，为空则不显示或不设置
    /// icon: 图标。
    /// name: 显示文本
    /// disabled: 是否禁用。
    /// styles: 选项样式
    /// activeStyles: 选项激活态样式
    /// hover: 选项点击态类名
    const tabs = ref([
        { icon: "", name: "热门", disabled: false, styles: "", activeStyles: "", hover: "hi-hover" },
        { icon: "", name: "数码", disabled: false, styles: "", activeStyles: "", hover: "hi-hover" },
        { icon: "", name: "美妆", disabled: false, styles: "", activeStyles: "", hover: "hi-hover" },
        { icon: "", name: "礼品卡", disabled: false, styles: "", activeStyles: "", hover: "hi-hover" },
        { icon: "", name: "百货", disabled: false, styles: "", activeStyles: "", hover: "hi-hover" },
        { icon: "", name: "家电", disabled: false, styles: "", activeStyles: "", hover: "hi-hover" },
        { icon: "", name: "手表", disabled: false, styles: "", activeStyles: "", hover: "hi-hover" },
        { icon: "", name: "配饰", disabled: false, styles: "", activeStyles: "", hover: "hi-hover" },
        { icon: "", name: "潮玩", disabled: false, styles: "", activeStyles: "", hover: "hi-hover" },
        { icon: "", name: "食品", disabled: false, styles: "", activeStyles: "", hover: "hi-hover" },
    ]);

    // 激活项下标
    const current = ref(0);

    // 切换事件
    function onChange(index) {
        current.value = index;
    }
</script>
```


## 右侧固定菜单

此组件可以通过 `right` 属性设置右侧固定菜单数据，数据结构同 `list`。

```vue
<template>
    <hi-tabs :list="tabs" keyName="name" :current="current" @change="onChange" :right="menus" @rightClick="onRightClick"></hi-tabs>
</template>

<script setup>
    import { ref } from 'vue';

    // 选项卡数据
    /// icon: 图标，为空则不展示。
    /// name: 显示文本
    /// disabled: 是否禁用
    const tabs = ref([
        { icon: "", name: "热门", disabled: false },
        { icon: "", name: "数码", disabled: false },
        { icon: "", name: "美妆", disabled: false },
        { icon: "", name: "礼品卡", disabled: false },
        { icon: "", name: "百货", disabled: false },
        { icon: "", name: "家电", disabled: false },
        { icon: "", name: "手表", disabled: false },
        { icon: "", name: "配饰", disabled: false },
        { icon: "", name: "潮玩", disabled: false },
        { icon: "", name: "食品", disabled: false },
    ]);

    // 激活项下标
    const current = ref(0);

    // 右侧固定菜单
    const menus = ref([
        { icon: "filter", name: "筛选" },
    ])

    // 切换事件
    function onChange(index) {
        current.value = index;
    }

    // 右侧菜单点击事件
    function onRightClick(item, index) {
        console.log(item, index);
    }
</script>
```


## 自动滚动

通过 `autoScroll` 属性设置是否自动滚动。默认开启。

```vue
<hi-tabs :list="tabs" :current="current" :autoScroll="false"></hi-tabs>
```


## 等分布局

通过 `equal` 属性设置是否等分选项卡进行布局，此模式只适用于横向布局，并且开启等分后会禁止滚动。
<br/>
等分布局通常来说就是选项的宽度相同，选项的内容居中展示。

```vue
<hi-tabs :list="tabs" :current="current" equal></hi-tabs>
```


## 两端对齐

通过 `justify` 属性设置是否两端对齐选项卡进行布局，此模式只适用于横向布局，并且开启两端对齐后会禁止滚动。
<br/>
两端对齐布局通常来说就是选项靠两侧进行对齐，选项之间的间隔相同。

```vue
<hi-tabs :list="tabs" :current="current" justify></hi-tabs>
```


## 纵向布局

此组件支持纵向布局，通过 `column` 属性设置为纵向布局。

```vue
<hi-tabs :list="tabs" :current="current" column></hi-tabs>
```


## Props

| 属性名                | 说明                   | 类型                      | 默认值     | 可选值 | 版本 |
| :-------------------- | :--------------------- | :------------------------ | :--------- | :----- | :--- |
| `hover`               | 指定选项按下去的样式类 | `String`                  | `hi-hover` | -      | -    |
| `list`                | 选项卡数据             | `Array<Object>`           | `[]`       | -      | -    |
| `current`             | 激活项下标             | `Number`                  | `0`        | -      | -    |
| `keyName`             | 显示文字的字段名       | `String`                  | `name`     | -      | -    |
| `right`               | 右侧固定菜单数据       | `Array<Object>`           | `[]`       | -      | -    |
| `equal`               | 是否等分布局           | `Boolean`                 | `false`    | `true` | -    |
| `justify`             | 是否两端对齐布局       | `Boolean`                 | `false`    | `true` | -    |
| `scrollWithAnimation` | 是否开启滚动动画       | `Boolean`                 | `false`    | `true` | -    |
| `bg`                  | 组件背景               | `String`                  | -          | -      | -    |
| `radius`              | 组件圆角               | `String`                  | -          | -      | -    |
| `width`               | 组件宽度               | `String`                  | -          | -      | -    |
| `height`              | 组件高度               | `String`                  | -          | -      | -    |
| `color`               | 组件文字颜色           | `String`                  | -          | -      | -    |
| `activeColor`         | 激活项文字颜色         | `String`                  | -          | -      | -    |
| `fontSize`            | 组件文字大小           | `String`                  | -          | -      | -    |
| `activeFontSize`      | 激活项文字大小         | `String`                  | -          | -      | -    |
| `fontWeight`          | 组件文字粗细           | `String`                  | -          | -      | -    |
| `activeFontWeight`    | 激活项文字粗细         | `String`                  | -          | -      | -    |
| `gap`                 | 选项间距               | `String`                  | -          | -      | -    |
| `itemPadding`         | 选项内边距             | `String`                  | -          | -      | -    |
| `itemBg`              | 选项背景               | `String`                  | -          | -      | -    |
| `activeItemBg`        | 激活项背景             | `String`                  | -          | -      | -    |
| `barWidth`            | 指示器的宽度           | `String`                  | -          | -      | -    |
| `barHeight`           | 指示器的高度           | `String`                  | -          | -      | -    |
| `barColor`            | 指示器的颜色           | `String`                  | -          | -      | -    |
| `barRadius`           | 指示器的圆角大小       | `String`                  | -          | -      | -    |
| `barLeft`             | 指示器的 `left` 值     | `String`                  | -          | -      | -    |
| `barBottom`           | 指示器的 `bottom` 值   | `String`                  | -          | -      | -    |
| `itemStyle`           | 选项样式               | `[String, Object, Array]` | -          | -      | -    |
| `activeItemStyle`     | 激活项样式             | `[String, Object, Array]` | -          | -      | -    |
| `iconColor`           | 图标颜色               | `String`                  | -          | -      | -    |
| `activeIconColor`     | 激活项图标颜色         | `String`                  | -          | -      | -    |
| `iconSize`            | 图标大小               | `String`                  | -          | -      | -    |
| `activeIconSize`      | 激活项图标大小         | `String`                  | -          | -      | -    |

## Event

| 事件名       | 说明                 | 回调参数                                  | 版本 |
| :----------- | :------------------- | :---------------------------------------- | :--- |
| `itemClick`  | 选项点击事件         | `item`: 选项数据。`index`: 选项索引。     | -    |
| `rightClick` | 右键固定菜单点击事件 | `item`: 菜单项数据。`index`: 菜单项索引。 | -    |
| `change`     | 选项切换事件         | `item`: 选项数据。`index`: 选项索引。     | -    |


## Slots

| 名称        | 说明         | 参数 | 版本                                  |
| :---------- | :----------- | :--- | :------------------------------------ |
| `item`      | 选项插槽     | -    | `item`: 选项数据。`index`: 选项索引。 |
| `rightItem` | 右侧选项插槽 | -    | `item`: 选项数据。`index`: 选项索引。 |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                                 | 默认值                                                                             | 版本 |
| :------------------------------------- | :--------------------------------------------------------------------------------- | :--- |
| `--hi-tabs-background`                 | `none`                                                                             | -    |
| `--hi-tabs-border-radius`              | `0`                                                                                | -    |
| `--hi-tabs-display`                    | `flex`                                                                             | -    |
| `--hi-tabs-flex-direction`             | `row / column`                                                                     | -    |
| `--hi-tabs-align-items`                | `stretch`                                                                          | -    |
| `--hi-tabs-gap`                        | `0`                                                                                | -    |
| `--hi-tabs-width`                      | `100% / 8em`                                                                       | -    |
| `--hi-tabs-height`                     | `3em / 100%`                                                                       | -    |
| `--hi-tabs-text-align`                 | `center`                                                                           | -    |
| `--hi-tabs-color`                      | `inherit`                                                                          | -    |
| `--hi-tabs-active-color`               | `var(--hi-theme-primary)`                                                          | -    |
| `--hi-tabs-font-size`                  | `inherit`                                                                          | -    |
| `--hi-tabs-active-font-size`           | -                                                                                  | -    |
| `--hi-tabs-font-weight`                | `inherit`                                                                          | -    |
| `--hi-tabs-active-font-weight`         | -                                                                                  | -    |
| `--hi-tabs-left-flex`                  | `1`                                                                                | -    |
| `--hi-tabs-left-overflow`              | `hidden`                                                                           | -    |
| `--hi-tabs-left-position`              | `relative`                                                                         | -    |
| `--hi-tabs-items-display`              | `flex`                                                                             | -    |
| `--hi-tabs-items-gap`                  | `15px`                                                                             | -    |
| `--hi-tabs-items-flex-direction`       | `row / column`                                                                     | -    |
| `--hi-tabs-items-justify-content`      | `flex-start / space-between`                                                       | -    |
| `--hi-tabs-item-gap`                   | `2px`                                                                              | -    |
| `--hi-tabs-item-position`              | `relative`                                                                         | -    |
| `--hi-tabs-item-display`               | `flex`                                                                             | -    |
| `--hi-tabs-item-flex-direction`        | `row`                                                                              | -    |
| `--hi-tabs-item-align-items`           | `center`                                                                           | -    |
| `--hi-tabs-item-justify-content`       | `center`                                                                           | -    |
| `--hi-tabs-item-transition`            | `100ms`                                                                            | -    |
| `--hi-tabs-item-flex-shrink`           | `0`                                                                                | -    |
| `--hi-tabs-item-background`            | `none`                                                                             | -    |
| `--hi-tabs-item-active-background`     | `currentColor / var(--hi-tabs-item-background, none)`                              | -    |
| `--hi-tabs-item-padding`               | `0 5px / 10px`                                                                     | -    |
| `--hi-tabs-item-flex`                  | `none / 1`                                                                         | -    |
| `--hi-tabs-item-width`                 | `auto`                                                                             | -    |
| `--hi-tabs-item-icon-color`            | `inherit`                                                                          | -    |
| `--hi-tabs-item-active-icon-color`     | -                                                                                  | -    |
| `--hi-tabs-item-icon-font-size`        | `1.25em`                                                                           | -    |
| `--hi-tabs-item-active-icon-font-size` | -                                                                                  | -    |
| `--hi-tabs-item-background-opacity`    | `0.2`                                                                              | -    |
| `--hi-tabs-bar-width`                  | `1em / 3px`                                                                        | -    |
| `--hi-tabs-bar-height`                 | `3px / 1em`                                                                        | -    |
| `--hi-tabs-bar-background`             | `currentColor`                                                                     | -    |
| `--hi-tabs-bar-opacity`                | `0 / 1`                                                                            | -    |
| `--hi-tabs-bar-border-radius`          | `10px`                                                                             | -    |
| `--hi-tabs-bar-left`                   | `50% / 1px`                                                                        | -    |
| `--hi-tabs-bar-bottom`                 | `1px / 50%`                                                                        | -    |
| `--hi-tabs-bar-transform`              | `translateX(-50%) scale(0) / translateX(-50%) scale(1) / translateY(50%) scale(1)` | -    |
| `--hi-tabs-right-flex-shrink`          | `0`                                                                                | -    |
| `--hi-tabs-right-display`              | `flex`                                                                             | -    |
| `--hi-tabs-right-align-items`          | `center`                                                                           | -    |
| `--hi-tabs-right-font-size`            | `inherit`                                                                          | -    |
| `--hi-tabs-right-font-weight`          | `inherit`                                                                          | -    |
| `--hi-tabs-right-color`                | `inherit`                                                                          | -    |
| `--hi-tabs-right-gap`                  | `10px`                                                                             | -    |