---
titleTemplate: hi-ui - Loading - 加载
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/loading
pageClass: demo-page
---

# Loading 加载

此组件通常用于异步加载场景。


## 基础使用

通过 `icon` 属性设置加载图标。

```vue
<hi-loading icon="__loading" />
```


## 加载文字

通过 `text` 属性设置加载文本。

```vue
<hi-loading text="加载中..." />
```


## 文字颜色

通过 `color` 属性设置文字颜色。

```vue
<hi-loading text="加载中..." color="#ff0000" />
```


## 文字大小

通过 `fontSize` 属性设置为文字大小。


```vue
<hi-loading text="加载中..." fontSize="1em" />
```


## 纵向布局

通过设置 `vertical` 属性设置是否开启纵向布局。

```vue
<hi-loading text="加载中..." vertical />
```


## Props

| 属性名     | 说明         | 类型      | 默认值      | 可选值 | 版本 |
| :--------- | :----------- | :-------- | :---------- | :----- | :--- |
| `text`     | 加载提示文本 | `String`  | -           | -      | -    |
| `icon`     | 加载图标     | `String`  | `__loading` | -      | -    |
| `color`    | 文本颜色     | `String`  | -           | -      | -    |
| `fontSize` | 文本大小     | `String`  | -           | -      | -    |
| `vertical` | 纵向布局     | `Boolean` | `false`     | -      | -    |


## Event

| 事件名  | 说明         | 回调参数 | 版本 |
| :------ | :----------- | :------- | :--- |
| `click` | 组件点击事件 | -        | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                                   | 默认值         | 版本 |
| :--------------------------------------- | :------------- | :--- |
| `--hi-loading-color`                     | `inherit`      | -    |
| `--hi-loading-font-size`                 | `0.8em`        | -    |
| `--hi-loading-display`                   | `inline-flex`  | -    |
| `--hi-loading-flex-direction`            | `row / column` | -    |
| `--hi-loading-align-items`               | `center`       | -    |
| `--hi-loading-justify-content`           | `center`       | -    |
| `--hi-loading-text-aligin`               | `center`       | -    |
| `--hi-loading-gap`                       | `5px`          | -    |
| `--hi-loading-icon-color`                | `inherit`      | -    |
| `--hi-loading-icon-font-size`            | `1.5em`        | -    |
| `--hi-loading-animation-duration`        | `1.5s`         | -    |
| `--hi-loading-animation-iteration-count` | `infinite`     | -    |
| `--hi-loading-animation-timing-function` | `linear`       | -    |