---
titleTemplate: hi-ui - StatusBar - 状态栏
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/status-bar
pageClass: demo-page
---

# StatusBar 状态栏

此组件通常用于自定义状态栏。


## 基础使用

* 此组件默认高度为通过 `uni.getSystemInfoSync()` 获取的状态栏高度。

```vue
<hi-status-bar></hi-status-bar>
```


## 背景

可以通过 `bg` 属性设置状态栏背景，也可以通过 `autoBg` 属性来设置是否开启自动读取在 `pages.json` 中配置的 `navigationBarBackgroundColor` 值用作状态栏背景， `autoBg` 默认为 `true`。

```vue
<hi-status-bar autoBg></hi-status-bar>
<hi-status-bar bg="#ff0000"></hi-status-bar>
```


## Props

| 属性名   | 说明                                   | 类型      | 默认值 | 可选值  | 版本 |
| :------- | :------------------------------------- | :-------- | :----- | :------ | :--- |
| `height` | 高度                                   | `String`  | -      | -       | -    |
| `bg`     | 背景                                   | `String`  | -      | -       | -    |
| `autoBg` | 是否自动读取在 pages.json 中设置的背景 | `Boolean` | `true` | `false` | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                       | 默认值                  | 版本 |
| :--------------------------- | :---------------------- | :--- |
| `--hi-status-bar-height`     | `25px`                  | -    |
| `--hi-status-bar-background` | `transparent`           | -    |
| `--hi-status-bar-z-index`    | `var(--hi-index-upper)` | -    |