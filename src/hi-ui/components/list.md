---
titleTemplate: hi-ui - List - 列表
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/list
pageClass: demo-page
---

# List 列表

我们在开发项目时遇到列表页面，每次都需要 for 循环列表数据，并且需要添加空状态和加载数据的状态。此组件旨在解决这些重复工作，只需给此组件传递一些属性就可以自动判断什么时候显示空数据，应该显示何种数据加载状态。


## 基础使用

* 通过默认插槽插入列表项。

```vue
<template>
    <hi-list>
        <view class="item" v-for="i in 6" :key="i">列表项</view>
    </hi-list>
</template>

<style scoped>
.item {
    height: 120px;
    background: #ffffff;
    display: flex;
    align-items: center;
    justify-content: center;
    font-weight: 500;
}
</style>
```


## 数据长度

如果需要显示空状态或数据加载状态，需要设置数据长度。
<br/>
通过 `total` 属性设置数据长度。

```vue
<template>
    <hi-list :total="6">
        <view class="item" v-for="i in 6" :key="i">列表项</view>
    </hi-list>
</template>

<style scoped>
.item {
    height: 120px;
    background: #ffffff;
    display: flex;
    align-items: center;
    justify-content: center;
    font-weight: 500;
}
</style>
```


## 数据加载状态

通过 `showStatus` 设置是否显示数据加载状态，默认开启，并且只当 `status` 不为空时才会显示，`status` 可选的值请参考 [hi-loadmore 组件](/hi-ui/components/loadmore)：


```vue
<template>
    <hi-list :total="0" status="loading">
        <view class="item" v-for="i in 6" :key="i">列表项</view>
    </hi-list>
</template>

<style scoped>
.item {
    height: 120px;
    background: #ffffff;
    display: flex;
    align-items: center;
    justify-content: center;
    font-weight: 500;
}
</style>
```


## 空状态

通过 `showEmpty` 设置是否显示空状态，默认开启，并且只当 `total` 为 `0` 并且 `status = "nomore"` 时才会显示。

```vue
<template>
    <hi-list :total="0" status="nomore">
        <view class="item" v-for="i in 6" :key="i">列表项</view>
    </hi-list>
</template>

<style scoped>
.item {
    height: 120px;
    background: #ffffff;
    display: flex;
    align-items: center;
    justify-content: center;
    font-weight: 500;
}
</style>
```


## 设置列数

通过 `cols` 属性设置数据列数，默认两列。

```vue
<template>
    <hi-list :total="6" status="nomore" cols="3">
        <view class="item" v-for="i in 6" :key="i">3列</view>
    </hi-list>
</template>

<style scoped>
.item {
    height: 120px;
    background: #ffffff;
    display: flex;
    align-items: center;
    justify-content: center;
    font-weight: 500;
}
</style>
```


## 设置间距

通过 `gap` 属性列表项之间的间距，默认 `12px`。


```vue
<template>
    <hi-list :total="6" status="nomore" cols="3" gap="20px">
        <view class="item" v-for="i in 6" :key="i">自定义间距</view>
    </hi-list>
</template>

<style scoped>
.item {
    height: 120px;
    background: #ffffff;
    display: flex;
    align-items: center;
    justify-content: center;
    font-weight: 500;
}
</style>
```


## Props

| 属性名       | 说明                 | 类型               | 默认值 | 可选值                                | 版本 |
| :----------- | :------------------- | :----------------- | :----- | :------------------------------------ | :--- |
| `total`      | 列表数据长度         | `Number`           | `0`    | -                                     | -    |
| `cols`       | 列数                 | `[Number, String]` | -      | -                                     | -    |
| `gap`        | 列表项之间的间距     | `String`           | -      | -                                     | -    |
| `showEmpty`  | 是否显示空状态       | `Boolean`          | `true` | `false`                               | -    |
| `showStatus` | 是否显示加载状态     | `Boolean`          | `true` | `false`                               | -    |
| `status`     | 数据加载状态         | `String`           | -      | [参考](/hi-ui/components/hi-loadmore) | -    |
| `emptyOpts`  | 空状态组件的配置项   | `Object`           | `{}`   | [参考](/hi-ui/components/hi-empty)    | -    |
| `statusOpts` | 加载状态组件的配置项 | `Object`           | `{}`   | [参考](/hi-ui/components/hi-loadmore) | -    |


## Slots

| 名称      | 说明       | 参数 | 版本 |
| :-------- | :--------- | :--- | :--- |
| `default` | 列表项插槽 | -    | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                    | 默认值   | 版本 |
| :------------------------ | :------- | :--- |
| `--hi-list-items-display` | `grid`   | -    |
| `--hi-list-columns`       | `2`      | -    |
| `--hi-list-items-gap`     | `12px`   | -    |
| `--hi-list-status-margin` | `20px 0` | -    |