---
titleTemplate: hi-ui - Icon - 图标
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/icon
pageClass: demo-page
---

# Icon 图标

基于 `iconfont` 的图标组件，包含了常用的图标。


## 基本使用

通过 `name` 属性设置图标名称即可使用。图标默认宽、高和字体尺寸继承父元素的 `font-size` 大小，即 `1em`，颜色继承父元素的 `color` 颜色。

```vue
<hi-icon name="__tongzhi" />
```


## 图片图标

如果 `name` 属性的值包含 `"/"`，则会被当作图片路径，此时图标会被渲染成图片图标，由于是图片，所以 `color` 属性无效。

> [!TIP] 提示
> 图片路径建议使用网络路径或以 `"/static"` 开头的绝对路径。

```vue
<hi-icon name="/static/images/icon.png" />
```


## 扩展图标

由于内置的图标不能满足每个项目的需要，所以提供了扩展图标的功能，我们约定 `name` 属性的值以双下划线 `"__"` 开头的图标名称为内置图标，值中包含斜杠 `"/"` 的为图片图标，其余则被认为是扩展图标。

```vue
<!-- 内置图标 -->
<hi-icon name="__tongzhi" />

<!-- 图片图标 -->
<hi-icon name="/static/images/icon.png" />

<!-- 扩展图标 -->
<hi-icon name="wechat" />
```

### 配置扩展图标

1、首先在 `iconfont` 中定义好图标库。

* 定义好 `FontClass/Symbol 前缀` 和 `Font Family`，需定义成相同前缀的名称，否则不生效，区别在于 `FontClass/Symbol 前缀` 比 `Font Family` 多一个中划线 `"-"`。
* 配置字体格式，选择 `woff2` 和 `Base64` 两种。
* 配置好后保存。

![iconfont](/hi-ui/iconfont-1.png)

2、将项目下载至本地。

3、在 `App.vue` 的 `style` 标签中引入下载的扩展图标库 `css` 文件。

```scss
// 引入下载的扩展图标库 `css` 文件
@import "@/iconfonts/iconfont.css";
```

4、在 `main.js` 中设置扩展图标库的名称前缀。

```js
// 设置扩展图标名称前缀
uni.$hi.config.icon.prefix = "app-iconfont";
```

至此，扩展图标库配置完成，你就可以在 `name` 属性中直接使用扩展图标名称啦。


## Props

| 属性名   | 说明                                  | 类型     | 默认值      | 可选值                                                    | 版本 |
| :------- | :------------------------------------ | :------- | :---------- | :-------------------------------------------------------- | :--- |
| `hover`  | 指定按下去的样式类                    | `String` | -           | -                                                         | -    |
| `name`   | 字体图标的名称或图片图标的路径        | `String` | -           | -                                                         | -    |
| `size`   | 图标的大小，宽、高和 `font-size` 大小 | `String` | -           | -                                                         | -    |
| `color`  | 图标颜色，图片图标时不生效            | `String` | -           | -                                                         | -    |
| `mode`   | 图片图标的裁剪模式                    | `String` | `aspectFit` | [参考](https://uniapp.dcloud.net.cn/component/image.html) | -    |
| `width`  | 组件宽度，也是图片的宽度              | `String` | `100%`      | -                                                         | -    |
| `height` | 组件高度，也是图片的高度              | `String` | `100%`      | -                                                         | -    |

> [!TIP] 提示
> `width` 和 `height` 只影响组件的宽高，即组件的 `width` 和 `height` 属性，不影响组件的字体大小 `(font-size)`。<br/>
> `size` 属性影响组件的宽高和组件的字体大小 `(font-size)`。<br/>
> `width` 和 `height` 优先级高于 `size`。


## Event

| 事件名  | 说明         | 回调参数 | 版本 |
| :------ | :----------- | :------- | :--- |
| `click` | 组件点击事件 | -        | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                      | 默认值        | 版本 |
| :-------------------------- | :------------ | :--- |
| `--hi-icon-display`         | `inline-flex` | -    |
| `--hi-icon-flex-direction`  | `column`      | -    |
| `--hi-icon-align-items`     | `center`      | -    |
| `--hi-icon-justify-content` | `center`      | -    |
| `--hi-icon-color`           | `inherit`     | -    |
| `--hi-icon-font-size`       | `inherit`     | -    |
| `--hi-icon-width`           | `1em`         | -    |
| `--hi-icon-height`          | `1em`         | -    |
| `--hi-icon-image-width`     | `100%`        | -    |
| `--hi-icon-image-height`    | `100%`        | -    |
