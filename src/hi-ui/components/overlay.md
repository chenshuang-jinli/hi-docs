---
titleTemplate: hi-ui - Overlay - 遮罩层
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/overlay
pageClass: demo-page
---

# Overlay 遮罩层

弹出一个遮罩层，内容需自定义。需给内容增加 `@tap.stop` 用来阻止内容事件冒泡到遮罩层。


## 基础使用

通过 `show` 属性来控制遮罩层的显示状态。

```vue
<template>
    <hi-overlay :show="show"  @close="show = false"></hi-overlay>
    <hi-button text="点击显示" @click="show = true"></hi-button>
</template>

<script setup>
    import { ref } from "vue";

    // 显示状态
    const show = ref(false);
</script>
```


## 嵌入内容

通过默认插槽嵌入内容。

```vue
<template>
    <hi-overlay :show="show"  @close="show = false">
        <view class="overlay-content" @tap.stop>我是内容</view>
    </hi-overlay>
    <hi-button text="点击显示" @click="show = true"></hi-button>
</template>

<script setup>
    import { ref } from "vue";

    // 显示状态
    const show = ref(false);
</script>

<style scoped>
.overlay-content {
    width: 120px;
    height: 120px;
    display: flex;
    align-items: center;
    justify-content: center;
}
</style>
```

> [!TIP] 提示
> 需给内容增加 `@tap.stop` 用来阻止内容事件冒泡到遮罩层。


## 内容显示位置

通过 `align` 属性设置内容显示位置，可选的值有：
* `left-top`: 左上角；
* `left-center`: 左侧居中；
* `left-bottom`: 右下角；
* `center-top`: 顶部居中；
* `center-center`: 整体居中，默认值；
* `center-bottom`: 底部居中；
* `right-top`: 右上角；
* `right-center`: 右侧居中；
* `right-bottom`: 右下角；

```vue
<template>
    <hi-overlay :show="show"  @close="show = false" :align="align">
        <view class="overlay-content" @tap.stop>我是内容</view>
    </hi-overlay>
    <hi-button text="left-top" @click="handleShow('left-top')"></hi-button>
    <hi-button text="left-center" @click="handleShow('left-center')"></hi-button>
    <hi-button text="left-bottom" @click="handleShow('left-bottom')"></hi-button>
    <hi-button text="center-top" @click="handleShow('center-top')"></hi-button>
    <hi-button text="center-center" @click="handleShow('center-center')"></hi-button>
    <hi-button text="center-bottom" @click="handleShow('center-bottom')"></hi-button>
    <hi-button text="right-top" @click="handleShow('right-top')"></hi-button>
    <hi-button text="right-center" @click="handleShow('right-center')"></hi-button>
    <hi-button text="right-bottom" @click="handleShow('right-bottom')"></hi-button>
</template>

<script setup>
    import { ref } from "vue";

    // 显示状态
    const show = ref(false);

    // 显示位置
    const align = ref("center-center");

    // 点击显示
    function handleShow(_align) {
        align.value = _align;
        show.value = true;
    }
</script>

<style scoped>
.overlay-content {
    width: 120px;
    height: 120px;
    display: flex;
    align-items: center;
    justify-content: center;
}
</style>
```


## Props

| 属性名  | 说明         | 类型      | 默认值          | 可选值       | 版本 |
| :------ | :----------- | :-------- | :-------------- | :----------- | :--- |
| `show`  | 显示状态     | `Boolean` | `false`         | `true`       | -    |
| `align` | 内容显示位置 | `String`  | `center-center` | 参考上方说明 | -    |
| `bg`    | 背景         | `String`  | -               | -            | -    |
| `index` | 层级         | `String`  | -               | -            | -    |


## Event

| 事件名  | 说明         | 回调参数 | 版本 |
| :------ | :----------- | :------- | :--- |
| `click` | 组件点击事件 | -        | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                                   | 默认值                         | 版本 |
| :--------------------------------------- | :----------------------------- | :--- |
| `--hi-overlay-position`                  | `fixed`                        | -    |
| `--hi-overlay-width`                     | `100%`                         | -    |
| `--hi-overlay-height`                    | `100%`                         | -    |
| `--hi-overlay-top`                       | `0`                            | -    |
| `--hi-overlay-left`                      | `0`                            | -    |
| `--hi-overlay-right`                     | `0`                            | -    |
| `--hi-overlay-bottom`                    | `0`                            | -    |
| `--hi-overlay-background`                | `var(--hi-background-overlay)` | -    |
| `--hi-overlay-z-index`                   | `var(--hi-index-upper)`        | -    |
| `--hi-overlay-display`                   | `flex`                         | -    |
| `--hi-overlay-flex-direction`            | `column`                       | -    |
| `--hi-overlay-align-items`               | `center`                       | -    |
| `--hi-overlay-overflow`                  | `hidden`                       | -    |
| `--hi-overlay-animation-duration`        | `300ms`                        | -    |
| `--hi-overlay-animation-timing-function` | `linear`                       | -    |
| `--hi-overlay-animation-fill-mode`       | `forwards`                     | -    |