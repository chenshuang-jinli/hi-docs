---
titleTemplate: hi-ui - Radio - 单选框
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/radio
pageClass: demo-page
---

# Radio 单选框

一个简便易用的单选框。


## 单独使用

通过 `checked` 属性绑定组件的选中状态，然后监听组件的点击事件手动控制选中状态。

```vue
<template>
    <hi-radio :checked="isChecked" label="单独使用" @click="handleClick"></hi-radio>
</template>

<script setup>
    import { ref } from "vue";

    // 选中状态
    const isChecked = ref(false);

    // 点击事件
    function handleClick() {
        isChecked.value = !isChecked.value;
    }
</script>
```


## 选择框文本

选择框的文本可通过 `label` 属性设置。

```vue
<hi-radio :checked="isChecked" label="选择框文本"></hi-radio>
```


## 边框

选择框默认无边框，可通过 `border` 属性开启边框。

```vue
<hi-radio :checked="isChecked" label="显示边框" border></hi-radio>
```


## 主题

通过 `theme` 属性设置主题。

```vue
<hi-radio :checked="isChecked" label="主题" theme="primary"></hi-radio>
<hi-radio :checked="isChecked" label="主题" theme="success"></hi-radio>
<hi-radio :checked="isChecked" label="主题" theme="warning"></hi-radio>
<hi-radio :checked="isChecked" label="主题" theme="error"></hi-radio>
<hi-radio :checked="isChecked" label="主题" theme="info"></hi-radio>
```


## 镂空

通过 `plain` 属性设置为镂空。

```vue
<hi-radio :checked="isChecked" label="镂空" plain></hi-radio>
<hi-radio :checked="isChecked" label="镂空" plain theme="primary"></hi-radio>
```


## 浅化背景

通过 `tint` 属性设置为浅化背景。

```vue
<hi-radio :checked="isChecked" label="浅化背景" tint></hi-radio>
<hi-radio :checked="isChecked" label="浅化背景" tint theme="primary"></hi-radio>
```


## 形状

通过 `square` 属性可以将选择框设置为矩形。

```vue
<hi-radio :checked="isChecked" label="矩形" square></hi-radio>
<hi-radio :checked="isChecked" label="矩形" square theme="primary"></hi-radio>
```


## 禁用

通过 `disabled` 属性设置为禁用。

```vue
<hi-radio :checked="isChecked" label="禁用" disabled></hi-radio>
<hi-radio :checked="isChecked" label="禁用" disabled theme="primary"></hi-radio>
```


## 边框颜色

* 通过 `inactiveBorderColor` 属性设置未激活状态时的边框颜色。
* 通过 `activeBorderColor` 属性设置激活状态时的边框颜色。

```vue
<hi-radio :checked="isChecked" label="自定义边框颜色" inactiveBorderColor="red" activeBorderColor="blue"></hi-radio>
```


## 选择框颜色

* 通过 `inactiveColor` 属性设置未激活状态时的选择框颜色。
* 通过 `activeColor` 属性设置激活状态时的选择框颜色。

```vue
<hi-radio :checked="isChecked" label="自定义选择框颜色" inactiveColor="red" activeColor="blue"></hi-radio>
```


## 选择框大小

通过 `size` 属性设置选择框大小。

```vue
<hi-radio :checked="isChecked" label="自定义选择框大小" size="1.5em"></hi-radio>
```


## 文本颜色

* 通过 `labelColor` 属性设置未激活状态时的文本颜色。
* 通过 `activeLabelColor` 属性设置激活状态时的文本颜色。

```vue
<hi-radio :checked="isChecked" label="自定义文本颜色" labelColor="red" activeLabelColor="blue"></hi-radio>
```


## Props

| 属性名                | 说明                                                                                                       | 类型               | 默认值      | 可选值 | 版本 |
| :-------------------- | :--------------------------------------------------------------------------------------------------------- | :----------------- | :---------- | :----- | :--- |
| `hover`               | 指定按下去的样式类                                                                                         | `String`           | `hi-hover`  | -      | -    |
| `disabled`            | 是否禁用                                                                                                   | `Boolean`          | `false`     | `true` | -    |
| `value`               | 绑定的值，在 `hi-radio-group` 中时使用，[参考](/hi-ui/components/hi-radio-group)                           | `[String, Number]` | `undefined` | -      | -    |
| `checked`             | 选中状态，此属性为了解决抖音小程序 `provide/inject` 不生效的问题，[参考](/hi-ui/components/hi-radio-group) | `Boolean`          | `false`     | `true` | -    |
| `label`               | 选项文本                                                                                                   | `String`           | -           | -      | -    |
| `labelColor`          | 未激活状态下的文本颜色                                                                                     | `String`           | -           | -      | -    |
| `activeLabelColor`    | 激活状态下的文本颜色                                                                                       | `String`           | -           | -      | -    |
| `labelFontSize`       | 文本字体大小                                                                                               | `String`           | -           | -      | -    |
| `icon`                | 选择框图标                                                                                                 | `String`           | -           | -      | -    |
| `iconColor`           | 选择框图标颜色                                                                                             | `String`           | -           | -      | -    |
| `iconSize`            | 选择框图标大小                                                                                             | `String`           | -           | -      | -    |
| `size`                | 选择框大小                                                                                                 | `String`           | -           | -      | -    |
| `inactiveColor`       | 未激活状态下的选择框颜色                                                                                   | `String`           | -           | -      | -    |
| `activeColor`         | 激活状态下的选择框颜色                                                                                     | `String`           | -           | -      | -    |
| `border`              | 是否显示边框                                                                                               | `Boolean`          | `false`     | `true` | -    |
| `borderWidth`         | 边框宽度                                                                                                   | `String`           | -           | -      | -    |
| `inactiveBorderColor` | 未激活状态下的边框颜色                                                                                     | `String`           | -           | -      | -    |
| `activeBorderColor`   | 激活状态下的边框颜色                                                                                       | `String`           | -           | -      | -    |
| `radius`              | 选择框圆角大小                                                                                             | `String`           | -           | -      | -    |
| `square`              | 是否为矩形选择框                                                                                           | `Boolean`          | `false`     | `true` | -    |
| `theme`               | 主题                                                                                                       | `String`           | -           | -      | -    |
| `plain`               | 是否镂空                                                                                                   | `Boolean`          | `false`     | `true` | -    |
| `tint`                | 是否浅化背景                                                                                               | `String`           | -           | -      | -    |
| `tintOpacity`         | 浅化背景背景透明度                                                                                         | `String`           | -           | -      | -    |


## Event

| 事件名  | 说明         | 回调参数 | 版本 |
| :------ | :----------- | :------- | :--- |
| `click` | 组件点击事件 | -        | -    |


## Slots

| 名称      | 说明         | 参数 | 版本 |
| :-------- | :----------- | :--- | :--- |
| `default` | `label` 插槽 | -    | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                             | 默认值                                                                                                                             | 版本 |
| :--------------------------------- | :--------------------------------------------------------------------------------------------------------------------------------- | :--- |
| `--hi-radio-display`               | `inline-block`                                                                                                                     | -    |
| `--hi-radio-box-display`           | `inline-flex`                                                                                                                      | -    |
| `--hi-radio-box-vertical-align`    | `middle`                                                                                                                           | -    |
| `--hi-radio-size`                  | `1.25em`                                                                                                                           | -    |
| `--hi-radio-box-align-items`       | `center`                                                                                                                           | -    |
| `--hi-radio-box-justify-content`   | `center`                                                                                                                           | -    |
| `--hi-radio-box-flex-shrink`       | `0`                                                                                                                                | -    |
| `--hi-radio-inactive-color`        | `var(--hi-background-default) / var(--hi-radio-theme, var(--hi-radio-inactive-color, var(--hi-background-default))) / transparent` | -    |
| `--hi-radio-box-position`          | `relative`                                                                                                                         | -    |
| `--hi-radio-border-width`          | `0 / 1px`                                                                                                                          | -    |
| `--hi-radio-border-style`          | `solid`                                                                                                                            | -    |
| `--hi-radio-inactive-border-color` | `var(--hi-radio-theme, currentColor)`                                                                                              | -    |
| `--hi-radio-border-radius`         | `50% / 2px`                                                                                                                        | -    |
| `--hi-radio-icon-font-size`        | `calc(var(--hi-radio-size, 1.25em) * 0.8)`                                                                                         | -    |
| `--hi-radio-transition`            | `100ms`                                                                                                                            | -    |
| `--hi-radio-icon-color`            | `#ffffff / var(--hi-radio-theme, inherit) / var(--hi-radio-theme, #ffffff)`                                                        | -    |
| `--hi-radio-label-display`         | `inline-block`                                                                                                                     | -    |
| `--hi-radio-label-vertical-align`  | `middle`                                                                                                                           | -    |
| `--hi-radio-label-color`           | `inherit`                                                                                                                          | -    |
| `--hi-radio-label-font-size`       | `inherit`                                                                                                                          | -    |
| `--hi-radio-label-margin`          | `0 0 0 5px`                                                                                                                        | -    |
| `--hi-radio-active-color`          | `var(--hi-theme-primary)`                                                                                                          | -    |
| `--hi-radio-active-border-color`   | `var(--hi-radio-theme, currentColor)`                                                                                              | -    |
| `--hi-radio-active-label-color`    | `var(--hi-radio-label-color, inherit)`                                                                                             | -    |
| `--hi-radio-background-opacity`    | `0.2`                                                                                                                              | -    |