---
titleTemplate: hi-ui - NoticeBar - 滚动通知
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/notice-bar
pageClass: demo-page
---

# NoticeBar 滚动通知

该组件用于滚动通告场景，有多种模式可供选择。


## 滚动数据

通过 `list` 设置滚动数据。数据为一个对象数组，具体的数据格式请查看下方示例：

```vue
<template>
    <!-- 通过 keyName 设置滚动数据 item 中表示滚动文本数据的 key 名称 -->
    <hi-notice-bar :list="list" keyName="text"></hi-notice-bar>
</template>

<script setup>
    import { ref } from 'vue';

    // 滚动数据
    const list = ref([
        { text: "试问闲愁都几许？一川烟草，满城风絮，梅子黄时雨。" },
        { text: "万里归来颜愈少，微笑，笑时犹带岭梅香。试问岭南应不好，却道：此心安处是吾乡。" },
        { text: "春未绿，鬓先丝，人间别久不成悲。谁教岁岁红莲夜，两处沉吟各自知。" },
        { text: "双飞燕子几时回？夹岸桃花蘸水开。春雨断桥人不度，小舟撑出柳阴来。" },
        { text: "水且涸，荷叶出地寻丈，因列坐其下。上不见日，清风徐来，绿云自动。" },
        { text: "满室天香仙子家，一琴一剑一杯茶。羽衣常带烟霞色，不染人间桃李花。" }
    ]);
</script>
```


## 纵向滚动

组件默认是横向滚动，如果需要纵向滚动，可以设置 `vertical` 来开启纵向滚动。

```vue
<!-- 开启纵向滚动模式 -->
<hi-notice-bar :list="list" vertical></hi-notice-bar>
```


## 滚动时长/速度

此组件共有两种控制滚动速度的方式：

* 通过 `duration` 为每条通知设置一样的滚动总时长。默认为 `undefined`。
* 通过 `speed` 为通知设置每秒移动的固定距离，单位为 `px`。默认为 `undefined`。

> [!TIP] 提示
> 上方两种模式只能生效一个。`duration` 优先级高于 `speed`。如果需要开启 `speed` 模式，需保证 `duration` 的值为 `false（undefined、0、null 等等）`。

> [!TIP] 提示
> 组件默认内部的实现是 `duration` 模式，默认横向滚动时为 `5s（非步近模式）、1.5s（步近模式）` ，纵向滚动时为 `2.5s（非步近模式）、0.5s（步近模式）` 。可通过 `duration` 属性进行修改。

```vue
<hi-notice-bar :list="list" duration="10s"></hi-notice-bar>
<hi-notice-bar :list="list" vertical speed="10px"></hi-notice-bar>
```


## 步近模式

通过 `step` 属性设置是否开启步近模式。
<br/>
一般开启步近模式时，建议同步设置一下滚动间隔 `interval`

```vue
<hi-notice-bar :list="list" step interval="2s"></hi-notice-bar>
<hi-notice-bar :list="list" vertical step interval="2s"></hi-notice-bar>
```


## 通知图标

* 通过 `icon` 属性设置是否显示通知图标。
* 通过 `iconName` 属性设置通知图标名称。
* 通过 `iconColor` 属性设置通知图标颜色。
* 通过 `iconSize` 属性设置通知图标大小。


```vue
<hi-notice-bar :list="list" icon iconName="notice" iconColor="#ff0000" iconSize="1.5em"></hi-notice-bar>
```


## 更多按钮

* 通过 `arrow` 属性设置是否显示更多按钮。
* 通过 `arrowIcon` 属性设置更多按钮图标。
* 通过 `arrowColor` 属性设置更多按钮图标颜色。
* 通过 `arrowSize` 属性设置更多按钮图标大小。

```vue
<hi-notice-bar :list="list" arrow arrowIcon="more" arrowColor="#ff0000" arrowSize="1.5em"></hi-notice-bar>
```


## 关闭按钮

* 通过 `close` 属性设置是否显示关闭按钮。
* 通过 `closeIcon` 属性设置关闭按钮图标。
* 通过 `closeColor` 属性设置关闭按钮图标颜色。
* 通过 `closeSize` 属性设置关闭按钮图标大小。
*
```vue
<hi-notice-bar :list="list" close closeIcon="close" closeColor="#ff0000" closeSize="1.5em"></hi-notice-bar>
```


## Props

| 属性名       | 说明                                                       | 类型            | 默认值      | 可选值  | 版本 |
| :----------- | :--------------------------------------------------------- | :-------------- | :---------- | :------ | :--- |
| `hover`      | 指定按下去的样式类，生效于滚动文字、更多按钮和关闭按钮     | `String`        | `hi-hover`  | -       | -    |
| `show`       | 显示状态                                                   | `Boolean`       | `true`      | `false` | -    |
| `list`       | 数据列表                                                   | `Array<Object>` | `[]`        | -       | -    |
| `keyName`    | 显示的文本属性名                                           | `String`        | `text`      | -       | -    |
| `vertical`   | 是否开启纵向滚动模式                                       | `Boolean`       | `false`     | `true`  | -    |
| `duration`   | 单条通知滚动持续时长，单位为秒                             | `Number`        | `null`      | -       | -    |
| `speed`      | 每秒滚动距离，单位 `px`                                    | `Number`        | `null`      | -       | -    |
| `interval`   | 滚动间隔，即滚动结束和下条数据滚动开始之间的间隔，单位：秒 | `Number`        | `0`         | -       | -    |
| `step`       | 是否开启步近模式                                           | `Boolean`       | `false`     | `true`  | -    |
| `lines`      | 显示的行数，尽在开启步近模式时生效                         | `Number`        | `1`         | -       | -    |
| `icon`       | 是否显示通知图标                                           | `Boolean`       | `true`      | `false` | -    |
| `iconName`   | 通知图标名称                                               | `String`        | `__gonggao` | -       | -    |
| `iconColor`  | 通知图标颜色                                               | `String`        | -           | -       | -    |
| `iconSize`   | 通知图标大小                                               | `String`        | -           | -       | -    |
| `arrow`      | 是否显示右侧箭头（更多按钮）                               | `Boolean`       | `false`     | `true`  | -    |
| `arrowIcon`  | 右侧箭头图标名称                                           | `String`        | `__you`     | -       | -    |
| `arrowColor` | 右侧箭头图标颜色                                           | `String`        | -           | -       | -    |
| `arrowSize`  | 右侧箭头图标大小                                           | `String`        | -           | -       | -    |
| `close`      | 是否显示关闭按钮                                           | `Boolean`       | `false`     | `true`  | -    |
| `closeIcon`  | 关闭按钮图标名称                                           | `String`        | `__shanchu` | -       | -    |
| `closeColor` | 关闭按钮图标颜色                                           | `String`        | -           | -       | -    |
| `colseSize`  | 关闭按钮图标大小                                           | `String`        | -           | -       | -    |


## Event

| 事件名      | 说明                     | 回调参数                              | 版本 |
| :---------- | :----------------------- | :------------------------------------ | :--- |
| `itemClick` | 数据项点击事件           | item: 数据项数据。index: 数据项索引。 | -    |
| `arrow`     | 箭头（更多按钮）点击事件 | -                                     | -    |
| `close`     | 关闭按钮点击事件         | -                                     | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                                      | 默认值                    | 版本 |
| :------------------------------------------ | :------------------------ | :--- |
| `--hi-notice-bar-display`                   | `flex`                    | -    |
| `--hi-notice-bar-align-items`               | `center`                  | -    |
| `--hi-notice-bar-gap`                       | `5px`                     | -    |
| `--hi-notice-bar-animation-duration`        | `100ms`                   | -    |
| `--hi-notice-bar-animation-timing-function` | `linear`                  | -    |
| `--hi-notice-bar-transform-origin`          | `center center`           | -    |
| `--hi-notice-bar-content-flex`              | `1`                       | -    |
| `--hi-notice-bar-content-position`          | `relative`                | -    |
| `--hi-notice-bar-content-overflow`          | `hidden`                  | -    |
| `--hi-notice-bar-content-height`            | `1.5em`                   | -    |
| `--hi-notice-bar-content-line-height`       | `1.5`                     | -    |
| `--hi-notice-bar-icon-color`                | `inherit`                 | -    |
| `--hi-notice-bar-icon-font-size`            | `1.25em`                  | -    |
| `--hi-notice-bar-arrow-color`               | `inherit`                 | -    |
| `--hi-notice-bar-arrow-font-size`           | `1.25em`                  | -    |
| `--hi-notice-bar-close-color`               | `inherit`                 | -    |
| `--hi-notice-bar-close-font-size`           | `1.25em`                  | -    |
| `--hi-notice-bar-text-position`             | `absolute`                | -    |
| `--hi-notice-bar-text-z-index`              | `8`                       | -    |
| `--hi-notice-bar-text-transform`            | `none`                    | -    |
| `--hi-notice-bar-animation-duration`        | `5s / 2.5s / 1.5s / 0.5s` | -    |
| `--hi-notice-bar-animation-timing-function` | `linear`                  | -    |
| `--hi-notice-bar-animation-iteration-count` | `1`                       | -    |
| `--hi-notice-bar-animation-fill-mode`       | `forwards`                | -    |
