---
titleTemplate: hi-ui - Image - 图片
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/image
pageClass: demo-page
---

# Image 图片

对 `uni-app` 的 `<image>` 组件进行的二次封装，增加了加载状态等方便使用。


## 基础使用

通过给组件设置 `width`、`height` 和 `src` 属性即可使用。

```vue
<hi-image src="/static/images/logo.png" />
```

图片默认宽高为 `360rpx`。


## 加载状态

* 通过 `loading` 属性设置是否显示加载状态。默认为 `true`。
* 通过 `loadingIcon` 属性设置加载时的图标。
* 通过 `loadingText` 设置加载时的文本。
* 通过 `error` 属性设置是否显示加载失败状态。默认为 `true`。
* 通过 `errorIcon` 属性设置加载失败时的图标。
* 通过 `errorText` 设置加载失败时的文本。

```vue
<hi-image src="/static/images/logo.png" loading error />
```


## 裁剪模式

与 uni-app 的 `<image>` 组件用法完全一致，详见 [uni-app image](https://uniapp.dcloud.net.cn/component/image.html)。

```vue
<hi-image src="/static/images/logo.png" mode="aspectFill" />
```


## 圆形图片

通过 `circle` 属性设置图片为圆形。

```vue
<hi-image src="/static/images/logo.png" circle />
```


## Props

| 属性名                | 说明                                            | 类型      | 默认值      | 可选值                                                             | 版本 |
| :-------------------- | :---------------------------------------------- | :-------- | :---------- | :----------------------------------------------------------------- | :--- |
| `hover`               | 指定按下去的样式类                              | `String`  | ``          | -                                                                  | -    |
| `width`               | 图片宽度                                        | `String`  | `360rpx`    | -                                                                  | -    |
| `height`              | 图片高度                                        | `String`  | `360rpx`    | -                                                                  | -    |
| `src`                 | 图片地址                                        | `String`  | -           | -                                                                  | -    |
| `radius`              | 圆角大小                                        | `String`  | -           | -                                                                  | -    |
| `bg`                  | 背景                                            | `String`  | -           | -                                                                  | -    |
| `border`              | 是否显示边框                                    | `Boolean` | `false`     | `true`                                                             | -    |
| `borderWidth`         | 边框宽度                                        | `String`  | -           | -                                                                  | -    |
| `borderColor`         | 边框颜色                                        | `String`  | -           | -                                                                  | -    |
| `borderStyle`         | 边框类型                                        | `String`  | -           | -                                                                  | -    |
| `circle`              | 是否为圆形图片                                  | `Boolean` | `false`     | `true`                                                             | -    |
| `mode`                | 裁剪模式                                        | `String`  | `aspectFit` | [参考](https://uniapp.dcloud.net.cn/component/image.html)          | -    |
| `loading`             | 是否显示加载状态                                | `Boolean` | `true`      | `false`                                                            | -    |
| `loadingIcon`         | 加载图标                                        | `String`  | -           | `__loading`                                                        | -    |
| `loadingText`         | 加载状态文本                                    | `String`  | -           | -                                                                  | -    |
| `error`               | 是否显示加载失败状态                            | `Boolean` | `true`      | `false`                                                            | -    |
| `errorIcon`           | 加载失败图标                                    | `String`  | -           | `__image-error`                                                    | -    |
| `errorText`           | 加载失败状态文本                                | `String`  | -           | -                                                                  | -    |
| `lazyLoad`            | 是否懒加载图片                                  | `Boolean` | `true`      | `false`，[参考](https://uniapp.dcloud.net.cn/component/image.html) | -    |
| `fadeShow`            | 是否显示动画效果                                | `Boolean` | `true`      | `false`，[参考](https://uniapp.dcloud.net.cn/component/image.html) | -    |
| `webp`                | 在系统不支持 `webp` 的情况下是否单独启用 `webp` | `Boolean` | `true`      | `false`，[参考](https://uniapp.dcloud.net.cn/component/image.html) | -    |
| `showMenuByLongpress` | 开启长按图片显示识别小程序码菜单                | `Boolean` | `true`      | `false`，[参考](https://uniapp.dcloud.net.cn/component/image.html) | -    |
| `draggable`           | 是否能拖动图片                                  | `Boolean` | `false`     | `true`，[参考](https://uniapp.dcloud.net.cn/component/image.html)  | -    |


## Event

| 事件名  | 说明                                           | 回调参数                                                  | 版本 |
| :------ | :--------------------------------------------- | :-------------------------------------------------------- | :--- |
| `click` | 组件点击事件                                   | -                                                         | -    |
| `load`  | 当图片载入完毕时，发布到 `AppService` 的事件名 | [参考](https://uniapp.dcloud.net.cn/component/image.html) | -    |
| `error` | 当错误发生时，发布到 `AppService` 的事件名     | [参考](https://uniapp.dcloud.net.cn/component/image.html) | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                                   | 默认值         | 版本 |
| :--------------------------------------- | :------------- | :--- |
| `--hi-image-width`                       | `360rpx`       | -    |
| `--hi-image-height`                      | `360rpx`       | -    |
| `--hi-image-background`                  | `none`         | -    |
| `--hi-image-position`                    | `relative`     | -    |
| `--hi-image-border-width`                | `0 / 1px`      | -    |
| `--hi-image-border-style`                | `solid`        | -    |
| `--hi-image-border-color`                | `currentColor` | -    |
| `--hi-image-border-radius`               | `0 / 50%`      | -    |
| `--hi-image-status-display`              | `flex`         | -    |
| `--hi-image-status-align-items`          | `center`       | -    |
| `--hi-image-status-justify-content`      | `center`       | -    |
| `--hi-image-status-flex-direction`       | `column`       | -    |
| `--hi-image-status-gap`                  | `6px`          | -    |
| `--hi-image-status-icon-font-size`       | `2em`          | -    |
| `--hi-image-status-icon-color`           | `inherit`      | -    |
| `--hi-loading-animation-duration`        | `1.5s`         | -    |
| `--hi-loading-animation-iteration-count` | `infinite`     | -    |
| `--hi-loading-animation-timing-function` | `linear`       | -    |
| `--hi-image-status-text-font-size`       | `0.8em`        | -    |
| `--hi-image-status-text-color`           | `inherit`      | -    |
