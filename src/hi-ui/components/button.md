---
titleTemplate: hi-ui - Button - 按钮
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/button
pageClass: demo-page
---

# Button 按钮

按钮组件的根节点是一个 `uni-app` 的 `<button>` 标签，是对 `uni-app` `<button>` 组件的二次封装。


## 默认样式

按钮默认高度为 `45px`，宽度为 `auto`，背景为 `var(--hi-background-default)`，圆角为 `3px`，内边距为 `0 16px`，默认无边框。


## 按钮文本

通过 `text` 属性设置按钮文本。

```vue
<hi-button text="一个按钮" />
```

或者通过默认插槽设置文本。

```vue
<hi-button>一个按钮</hi-button>
```


## 按钮边框

按钮默认无边框，通过 `border` 属性设置边框。

- 默认边框宽度为：`1px`。
- 默认边框颜色为：`currentColor`。
- 默认边框类型为：`solid`。

```vue
<hi-button text="边框按钮" border />
```

可通过 `borderWidth`、`borderColor` 和 `borderStyle` 属性设置边框样式。


## 按钮主题

通过 `theme` 属性设置按钮主题。

```vue
<hi-button text="主要主题按钮" theme="primary" />
<hi-button text="成功主题按钮" theme="success" />
<hi-button text="警告主题按钮" theme="warning" />
<hi-button text="错误主题按钮" theme="error" />
<hi-button text="信息主题按钮" theme="info" />
```

> [!TIP] 提示
> 设置按钮主题时，按钮的文本颜色会自动设置为 `#ffffff`，如果需要修改，可以通过 `color` 属性设置。


## 镂空按钮

通过 `plain` 属性设置镂空按钮。
<br/>
按钮设置为镂空后，按钮的背景会自动设置为 `transparent`，如果需要修改，可以通过 `bg` 属性设置。
<br/>
同时按钮镂空时会默认显示边框。

```vue
<hi-button text="默认镂空" plain />
<hi-button text="主题镂空" plain theme="primary" />
```


## 浅化背景

通过 `tint` 属性设置浅化背景。
<br/>
浅化背景即是：为按钮的背景设置透明度，默认为 `0.2` 透明度。

```vue
<hi-button text="默认浅化" tint />
<hi-button text="主题浅化" tint theme="primary" />
```


## 禁用状态

通过 `disabled` 属性将按钮设置为禁用状态。


```vue
<hi-button text="禁用按钮" disabled theme="primary" />
```


## loading 状态

通过 `loading` 属性将按钮设置加载状态。
<br/>
通过 `loadingText` 属性设置加载时的文本。


```vue
<template>
    <hi-button text="加载按钮" loadingText="正在提交..." :loading="loading" theme="primary" @click="handleClick" />
</template>

<script setup>
    import { ref } from 'vue';

    // 按钮 loading 状态
    const loading = ref(false);

    // 按钮点击事件
    function handleClick() {
        loading.value = true;

        // 模拟提交
        setTimeout(() => {
            loading.value = false
        }, 3000);
    }
</script>
```

> [!TIP] 提示
> 按钮 `loading` 时，按钮同时也会被禁用。


## 按钮图标

为了方便使用，`<hi-button>` 设计了一个前置图标和一个后置图标，通过 `leftIcon` 和 `rightIcon` 属性设置前置图标和后置图标。

```vue
<hi-button text="前置图标" leftIcon="__gonggao" theme="primary"></hi-button>
<hi-button text="后置图标" rightIcon="__fuzhi" theme="primary"></hi-button>
```


## 圆角按钮

通过 `round` 属性设置按钮为圆角按钮。

```vue
<hi-button text="圆角按钮" round theme="primary"></hi-button>
```


## 圆形按钮

通过 `circle` 属性设置按钮为圆形按钮。

```vue
<hi-button text="圆形按钮" circle theme="primary"></hi-button>
```

> [!TIP] 提示
> 圆形按钮的宽度自动等于按钮的高度。


## 块级按钮

通过 `block` 属性设置按钮为块级元素，即宽度为 `100%` 的按钮。

```vue
<hi-button text="块级按钮" block theme="primary"></hi-button>
```


## 链接按钮

通过 `link` 属性设置按钮为链接按钮。
<br/>
通过 `underline` 设置是否显示下划线。
<br/>
链接按钮的 `padding` 为 `0`，宽度和高度为 `auto`，背景透明。

```vue
<hi-button text="链接按钮" link theme="primary"></hi-button>
<hi-button text="带下划线的链接按钮" link underline theme="primary"></hi-button>
```


## 按钮防抖

通过 `debounce` 属性设置按钮防抖。默认开启。
<br />
通过 `time` 属性可以设置防抖时间。单位毫秒，默认为 `800`。
<br />
通过 `tips` 属性可以设置防抖提示。为空时不提示。

```vue
<hi-button text="提交订单" debounce time="1000" tips="点击的太快啦" theme="primary"></hi-button>
```


## 按钮副文字

通过 `subText` 属性设置按钮副文字。

```vue
<hi-button text="提交订单" subText="副文字" theme="primary"></hi-button>
```


## Props

| 属性名                 | 说明                                   | 类型               | 默认值      | 可选值                                                     | 版本 |
| :--------------------- | :------------------------------------- | :----------------- | :---------- | :--------------------------------------------------------- | :--- |
| `hover`                | 指定按下去的样式类                     | `String`           | `hi-hover`  | -                                                          | -    |
| `text`                 | 按钮文本                               | `String`           | -           | -                                                          | -    |
| `disabled`             | 是否禁用                               | `Boolean`          | `false`     | `true`                                                     | -    |
| `theme`                | 按钮的主题                             | `String`           | -           | [参考](/hi-ui/style#主题)                                  | -    |
| `plain`                | 是否镂空按钮                           | `Boolean`          | `false`     | `true`                                                     | -    |
| `tint`                 | 是否浅化背景                           | `Boolean`          | `false`     | `true`                                                     | -    |
| `tintOpacity`          | 浅化背景透明度                         | `[String, Number]` | `0.2`       | -                                                          | -    |
| `fontSize`             | 文字大小                               | `String`           | -           | -                                                          | -    |
| `color`                | 文字颜色                               | `String`           | -           | -                                                          | -    |
| `width`                | 按钮宽度                               | `String`           | -           | -                                                          | -    |
| `height`               | 按钮高度                               | `String`           | -           | -                                                          | -    |
| `radius`               | 圆角大小                               | `String`           | -           | -                                                          | -    |
| `bg`                   | 背景                                   | `String`           | -           | -                                                          | -    |
| `border`               | 是否显示边框                           | `Boolean`          | `false`     | `true`                                                     | -    |
| `borderWidth`          | 边框宽度                               | `String`           | -           | -                                                          | -    |
| `borderColor`          | 边框颜色                               | `String`           | -           | -                                                          | -    |
| `borderStyle`          | 边框类型                               | `String`           | -           | -                                                          | -    |
| `round`                | 是否为圆角按钮                         | `Boolean`          | `false`     | `true`                                                     | -    |
| `circle`               | 是否为圆形按钮                         | `Boolean`          | `false`     | `true`                                                     | -    |
| `block`                | 是否为块级元素                         | `Boolean`          | `false`     | `true`                                                     | -    |
| `link`                 | 是否为链接按钮                         | `Boolean`          | `false`     | `true`                                                     | -    |
| `underline`            | 是否显示下划线                         | `Boolean`          | `false`     | `true`                                                     | -    |
| `iconSize`             | 图标大小                               | `String`           | -           | -                                                          | -    |
| `iconColor`            | 图标颜色                               | `String`           | -           | -                                                          | -    |
| `debounce`             | 是否开启防抖                           | `Boolean`          | `true`      | `false`                                                    | -    |
| `time`                 | 防抖时间                               | `[Number, String]` | `800`       | -                                                          | -    |
| `tips`                 | 防抖提示                               | `String`           | -           | -                                                          | -    |
| `loading`              | 是否为加载状态                         | `Boolean`          | `false`     | `true`                                                     | -    |
| `loadingText`          | 加载时的提示文本                       | `String`           | -           | -                                                          | -    |
| `loadingIcon`          | 加载图标                               | `String`           | `__loading` | -                                                          | -    |
| `leftIcon`             | 前置图标                               | `String`           | -           | -                                                          | -    |
| `rightIcon`            | 后置图标                               | `String`           | -           | -                                                          | -    |
| `subText`              | 副文本                                 | `String`           | -           | -                                                          | -    |
| `subColor`             | 副文本颜色                             | `String`           | -           | -                                                          | -    |
| `subFontSize`          | 副文本大小                             | `String`           | -           | -                                                          | -    |
| `formType`             | 表单提交类型                           | `String`           | -           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `openType`             | 开放能力                               | `String`           | -           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `hoverStartTime`       | 按住后多久出现点击态，单位毫秒         | `Number`           | `20`        | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `hoverStayTime`        | 手指松开后点击态保留时间，单位毫秒     | `Number`           | `70`        | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `appParameter`         | 打开 `APP` 时，向 `APP` 传递的参数     | `String`           | -           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `hoverStopPropagation` | 指定是否阻止本节点的祖先节点出现点击态 | `Boolean`          | `false`     | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `lang`                 | 指定返回用户信息的语言                 | `String`           | `zh_CN`     | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `sessionFrom`          | 会话来源                               | `String`           | -           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `sendMessageTitle`     | 会话内消息卡片标题                     | `String`           | -           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `sendMessagePath`      | 会话内消息卡片点击跳转小程序路径       | `String`           | -           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `sendMessageImg`       | 会话内消息卡片图片                     | `String`           | -           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `showMessageCard`      | 是否显示会话内消息卡片                 | `Boolean`          | `false`     | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `groupId`              | 打开群资料卡时，传递的群号             | `String`           | -           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `guildId`              | 打开群资料卡时，传递的群号             | `String`           | -           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `publicId`             | 打开公众号资料卡时，传递的号码         | `String`           | -           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `dataImId`             | 客服的抖音号                           | `String`           | -           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `dataImType`           | `IM`卡片类型                           | `String`           | -           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `dataGoodsId`          | 商品的`id`                             | `String`           | -           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `dataOrderId`          | 订单的`id`                             | `String`           | -           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `dataBizLine`          | 商品类型                               | `String`           | -           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |


## Event

| 事件名                      | 说明                             | 回调参数                                                   | 版本 |
| :-------------------------- | :------------------------------- | :--------------------------------------------------------- | :--- |
| `click`                     | 组件点击事件                     | -                                                          | -    |
| `getphonenumber`            | 获取用户手机号的回调             | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `getuserinfo`               | 获取用户信息的回调               | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `error`                     | 当使用开放能力时，发生错误的回调 | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `opensetting`               | 在打开授权设置页并关闭后回调     | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `launchapp`                 | 从小程序打开 `App` 成功的回调    | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `contact`                   | 客服消息回调                     | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `chooseavatar`              | 获取用户头像回调                 | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `agreeprivacyauthorization` | 用户同意隐私协议事件回调         | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `addgroupapp`               | 添加群应用的回调                 | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `chooseaddress`             | 调起用户编辑并选择收货地址的回调 | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `chooseinvoicetitle`        | 用户选择发票抬头的回调           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `subscribe`                 | 订阅消息授权回调                 | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `login`                     | 登录回调                         | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |
| `im`                        | 监听跳转`IM`的成功回调           | [参考](https://uniapp.dcloud.net.cn/component/button.html) | -    |


## Slots

| 名称      | 说明         | 参数 | 版本 |
| :-------- | :----------- | :--- | :--- |
| `default` | 文本内容插槽 | -    | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                             | 默认值                         | 版本 |
| :--------------------------------- | :----------------------------- | :--- |
| `--hi-button-position`             | `relative`                     | -    |
| `--hi-button-display`              | `inline-flex / flex`           | -    |
| `--hi-button-align-items`          | `center`                       | -    |
| `--hi-button-justify-content`      | `center`                       | -    |
| `--hi-button-color`                | `inherit`                      | -    |
| `--hi-button-font-size`            | `inherit`                      | -    |
| `--hi-button-border-radius`        | `3px`                          | -    |
| `--hi-button-background`           | `var(--hi-background-default)` | -    |
| `--hi-button-margin`               | `0`                            | -    |
| `--hi-button-padding`              | `0 16px`                       | -    |
| `--hi-button-gap`                  | `3px`                          | -    |
| `--hi-button-line-height`          | `inherit / 1.25`               | -    |
| `--hi-button-width`                | `auto / 100%`                  | -    |
| `--hi-button-height`               | `45px`                         | -    |
| `--hi-button-border-width`         | `0 / 1px`                      | -    |
| `--hi-button-border-style`         | `solid`                        | -    |
| `--hi-button-border-color`         | `currentColor`                 | -    |
| `--hi-button-after-display`        | `100%`                         | -    |
| `--hi-icon-image-height`           | `100%`                         | -    |
| `--hi-button-text-display`         | `flex`                         | -    |
| `--hi-button-text-flex-direction`  | `column`                       | -    |
| `--hi-button-text-justify-content` | `center`                       | -    |
| `--hi-button-text-align`           | `center`                       | -    |
| `--hi-button-sub-text-font-size`   | `0.7em`                        | -    |
| `--hi-button-sub-text-color`       | `inherit`                      | -    |
| `--hi-button-icon-font-size`       | `1.15em`                       | -    |
| `--hi-button-icon-color`           | `inherit`                      | -    |
| `--hi-button-background-opacity`   | `0.2`                          | -    |
| `--hi-button-text-decoration`      | `underline`                    | -    |
