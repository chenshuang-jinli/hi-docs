---
titleTemplate: hi-ui - Grid - 宫格
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/grid
pageClass: demo-page
---

# Grid 宫格

用此组件可以快速创建宫格布局。


## 基础使用

通过默认插槽设置宫格项

```vue
<template>
    <hi-grid>
        <view class="item" v-for="i in 6" :key="i">宫格项</view>
    </hi-grid>
</template>
```


## 设置列数

通过 `cols` 属性设置宫格列数，默认 `5` 列。

```vue
<template>
    <hi-grid cols="3">
        <view class="item" v-for="i in 6" :key="i">宫格项</view>
    </hi-grid>
</template>
```


## 设置间距

通过 `gap` 属性设置宫格项之间的间距，默认 `5px`。

```vue
<template>
    <hi-grid cols="3" gap="15px">
        <view class="item" v-for="i in 6" :key="i">宫格项</view>
    </hi-grid>
</template>
```


## Props

| 属性名 | 说明             | 类型             | 默认值 | 可选值 | 版本 |
| :----- | :--------------- | :--------------- | :----- | :----- | :--- |
| `cols` | 列数             | [Number, String] | -      | -      | -    |
| `gap`  | 宫格项之间的间距 | `String`         | -      | -      | -    |


## Slots

| 名称      | 说明       | 参数 | 版本 |
| :-------- | :--------- | :--- | :--- |
| `default` | 宫格项插槽 | -    | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名              | 默认值 | 版本 |
| :------------------ | :----- | :--- |
| `--hi-grid-display` | `grid` | -    |
| `--hi-grid-columns` | `5`    | -    |
| `--hi-grid-gap`     | `5px`  | -    |