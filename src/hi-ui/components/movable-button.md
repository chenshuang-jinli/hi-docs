---
titleTemplate: hi-ui - MovableButton - 可拖动按钮
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/movable-button
pageClass: demo-page
---

# MovableButton 可拖动按钮

此组件通常用于全局悬浮按钮。


## 基础使用

* 按钮默认显示在右侧居中位置。
* 默认可全方向拖动。
* 默认大小为 `45px * 45px`。
* 通过 `icon` 属性设置按钮图标，默认为 `__zhuye`。
* 通过 `text` 属性设置按钮文字，默认为空。

```vue
<hi-movable-button></hi-movable-button>
```


## 按钮位置

通过 `position` 属性设置按钮初始化位置，可选的值有：
* `left-top`: 左上角；
* `left-center`: 左侧居中；
* `left-bottom`: 左下角；
* `center-top`: 顶部居中；
* `cenetr-center`: 中部居中；
* `center-bottom`: 底部居中；
* `right-top`: 右上角；
* `right-center`: 右侧居中，默认值；
* `right-bottom`: 右下角。

```vue
<hi-movable-button position="left-top"></hi-movable-button>
<hi-movable-button position="left-center"></hi-movable-button>
<hi-movable-button position="left-bottom"></hi-movable-button>
<hi-movable-button position="center-top"></hi-movable-button>
<hi-movable-button position="center-center"></hi-movable-button>
<hi-movable-button position="center-bottom"></hi-movable-button>
<hi-movable-button position="right-top"></hi-movable-button>
<hi-movable-button position="right-center"></hi-movable-button>
<hi-movable-button position="right-bottom"></hi-movable-button>
```

> [!TIP] 提示
> 位置需在初始化时设置，虽然可以动态修改，但是动态修改后可拖动方向可能会失效。


## 按钮边框

通过 `border` 属性设置是否开启按钮边框。

- 默认边框宽度为：`1px`。
- 默认边框颜色为：`currentColor`。
- 默认边框类型为：`solid`。


```vue
<hi-movable-button border></hi-movable-button>
```


## 按钮主题

该组件支持设置主题，通过 `theme` 属性设置。

```vue
<hi-movable-button theme="primary"></hi-movable-button>
<hi-movable-button theme="success"></hi-movable-button>
<hi-movable-button theme="warning"></hi-movable-button>
<hi-movable-button theme="error"></hi-movable-button>
<hi-movable-button theme="info"></hi-movable-button>
```


## 按钮镂空

通过 `plain` 属性设置镂空按钮。
<br/>
按钮设置为镂空后，按钮的背景会自动设置为 `transparent`，如果需要修改，可以通过 `bg` 属性设置。
<br/>
同时按钮镂空时会默认显示边框。

```vue
<hi-movable-button plain></hi-movable-button>
<hi-movable-button plain theme="primary"></hi-movable-button>
```


## 浅化背景

通过 `tint` 属性设置浅化背景。
<br/>
浅化背景即是：为按钮的背景设置透明度，默认为 `0.2` 透明度。

```vue
<hi-movable-button tint></hi-movable-button>
<hi-movable-button tint theme="primary"></hi-movable-button>
```


## Props

| 属性名        | 说明                                                                                                 | 类型               | 默认值       | 可选值                                                           | 版本 |
| :------------ | :--------------------------------------------------------------------------------------------------- | :----------------- | :----------- | :--------------------------------------------------------------- | :--- |
| `hover`       | 指定按下去的样式类                                                                                   | `String`           | `hi-hover`   | -                                                                | -    |
| `icon`        | 按钮图标                                                                                             | `String`           | `__zhuye`    | -                                                                | -    |
| `text`        | 按钮文字                                                                                             | `String`           | -            | -                                                                | -    |
| `disabled`    | 是否禁用                                                                                             | `Boolean`          | `false`      | `true`                                                           | -    |
| `bg`          | 按钮背景                                                                                             | `String`           | -            | -                                                                | -    |
| `color`       | 按钮文字颜色                                                                                         | `String`           | -            | -                                                                | -    |
| `fontSize`    | 按钮文字大小                                                                                         | `String`           | -            | -                                                                | -    |
| `width`       | 按钮宽度                                                                                             | `String`           | -            | -                                                                | -    |
| `height`      | 按钮高度                                                                                             | `String`           | -            | -                                                                | -    |
| `radius`      | 按钮圆角                                                                                             | `String`           | -            | -                                                                | -    |
| `border`      | 是否显示按钮边框                                                                                     | `Boolean`          | `false`      | `true`                                                           | -    |
| `borderColor` | 按钮边框颜色                                                                                         | `String`           | -            | -                                                                | -    |
| `borderWidth` | 按钮边框宽度                                                                                         | `String`           | -            | -                                                                | -    |
| `iconColor`   | 按钮图标颜色                                                                                         | `String`           | -            | -                                                                | -    |
| `iconSize`    | 按钮图标大小                                                                                         | `String`           | -            | -                                                                | -    |
| `shadow`      | 按钮阴影                                                                                             | `String`           | -            | -                                                                | -    |
| `top`         | 按钮距离顶部距离                                                                                     | `String`           | -            | -                                                                | -    |
| `left`        | 按钮距离左侧距离                                                                                     | `String`           | -            | -                                                                | -    |
| `bottom`      | 按钮距离底部距离                                                                                     | `String`           | -            | -                                                                | -    |
| `right`       | 按钮距离右侧距离                                                                                     | `String`           | -            | -                                                                | -    |
| `theme`       | 按钮主题                                                                                             | `String`           | -            | 参考上方说明                                                     | -    |
| `plain`       | 按钮是否镂空                                                                                         | `Boolean`          | `false`      | `true`                                                           | -    |
| `tint`        | 是否开启浅化背景                                                                                     | `Boolean`          | `false`      | `true`                                                           | -    |
| `tintOpacity` | 浅化背景透明度                                                                                       | `[String, Number]` | `0.2`        | -                                                                | -    |
| `scaleArea`   | 当里面的 `movable-view` 设置为支持双指缩放时，设置此值可将缩放手势生效区域修改为整个 `movable-area`  | `Boolean`          | `false`      | [参考](https://uniapp.dcloud.net.cn/component/movable-area.html) | -    |
| `position`    | 按钮初始位置                                                                                         | `String`           | 参考上方说明 | -                                                                | -    |
| `direction`   | `movable-view` 的移动方向，属性值有 `all`、`vertical`、`horizontal`、`none`                          | `String`           | `all`        | [参考](https://uniapp.dcloud.net.cn/component/movable-view.html) | -    |
| `inertia`     | `movable-view` 是否带有惯性                                                                          | `Boolean`          | `false`      | [参考](https://uniapp.dcloud.net.cn/component/movable-view.html) | -    |
| `outOfBounds` | 超过可移动区域后，`movable-view` 是否还可以移动                                                      | `Boolean`          | `false`      | [参考](https://uniapp.dcloud.net.cn/component/movable-view.html) | -    |
| `damping`     | 阻尼系数，用于控制 `x` 或 `y` 改变时的动画和过界回弹的动画，值越大移动越快                           | `Number`           | `20`         | [参考](https://uniapp.dcloud.net.cn/component/movable-view.html) | -    |
| `friction`    | 摩擦系数，用于控制惯性滑动的动画，值越大摩擦力越大，滑动越快停止；必须大于 `0`，否则会被设置成默认值 | `Number`           | `2`          | [参考](https://uniapp.dcloud.net.cn/component/movable-view.html) | -    |
| `scale`       | 是否支持双指缩放，默认缩放手势生效区域是在 `movable-view` 内                                         | `Boolean`          | `false`      | [参考](https://uniapp.dcloud.net.cn/component/movable-view.html) | -    |
| `scaleMin`    | 定义缩放倍数最小值                                                                                   | `Number`           | `1 / 0.5`    | [参考](https://uniapp.dcloud.net.cn/component/movable-view.html) | -    |
| `scaleMax`    | 定义缩放倍数最大值                                                                                   | `Number`           | `10`         | [参考](https://uniapp.dcloud.net.cn/component/movable-view.html) | -    |
| `scaleValue`  | 定义缩放倍数，支持小数                                                                               | `Number`           | `1`          | [参考](https://uniapp.dcloud.net.cn/component/movable-view.html) | -    |
| `animation`   | 是否开启动画                                                                                         | `Boolean`          | `true`       | [参考](https://uniapp.dcloud.net.cn/component/movable-view.html) | -    |


## Event

| 事件名   | 说明                 | 回调参数                                                         | 版本 |
| :------- | :------------------- | :--------------------------------------------------------------- | :--- |
| `click`  | 按钮点击事件         | -                                                                | -    |
| `change` | 拖动过程中触发的事件 | [参考](https://uniapp.dcloud.net.cn/component/movable-view.html) | -    |
| `scale`  | 缩放过程中触发的事件 | [参考](https://uniapp.dcloud.net.cn/component/movable-view.html) | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                                         | 默认值                                                         | 版本 |
| :--------------------------------------------- | :------------------------------------------------------------- | :--- |
| `--hi-movable-button-container-width `         | `100%`                                                         | -    |
| `--hi-movable-button-container-height`         | `100% / calc(100% - var(--window-top) - var(--window-bottom))` | -    |
| `--hi-movable-button-container-top`            | `0 / var(--window-top)`                                        | -    |
| `--hi-movable-button-container-left`           | `0`                                                            | -    |
| `--hi-movable-button-container-z-index`        | `var(--hi-index-default)`                                      | -    |
| `--hi-movable-button-container-background`     | `transparent`                                                  | -    |
| `--hi-movable-button-container-position`       | `fixed`                                                        | -    |
| `--hi-movable-button-container-pointer-events` | `none`                                                         | -    |
| `--hi-movable-button-pointer-events`           | `all`                                                          | -    |
| `--hi-movable-button-width`                    | `45px`                                                         | -    |
| `--hi-movable-button-height`                   | `45px`                                                         | -    |
| `--hi-movable-button-left`                     | `auto / 10px / 50%`                                            | -    |
| `--hi-movable-button-top`                      | `auto / 10px / 50%`                                            | -    |
| `--hi-movable-button-right`                    | `auto / 10px`                                                  | -    |
| `--hi-movable-button-bottom`                   | `auto / 10px`                                                  | -    |
| `--hi-movable-button-background`               | `var(--hi-background-default)`                                 | -    |
| `--hi-movable-button-border-radius`            | `50%`                                                          | -    |
| `--hi-movable-button-display`                  | `flex`                                                         | -    |
| `--hi-movable-button-flex-direction`           | `column`                                                       | -    |
| `--hi-movable-button-align-items`              | `center`                                                       | -    |
| `--hi-movable-button-justify-content`          | `center`                                                       | -    |
| `--hi-movable-button-line-height`              | `1.25`                                                         | -    |
| `--hi-movable-button-color`                    | `inherit`                                                      | -    |
| `--hi-movable-button-font-size`                | `0.7em`                                                        | -    |
| `--hi-movable-button-box-shadow`               | `none`                                                         | -    |
| `--hi-movable-button-border-width`             | `0`                                                            | -    |
| `--hi-movable-button-border-style`             | `solid`                                                        | -    |
| `--hi-movable-button-border-color`             | `currentColor`                                                 | -    |
| `--hi-movable-button-z-index`                  | `var(--hi-index-default)`                                      | -    |
| `--hi-movable-button-margin`                   | 参考下方说明                                                   | -    |
| `--hi-movable-button-icon-font-size`           | `24px`                                                         | -    |
| `--hi-movable-button-icon-color`               | `inherit`                                                      | -    |
| `--hi-movable-button-background-opacity`       | `0.2`                                                          | -    |

**--hi-movable-button-margin 可能的值：**
* `0`
* `calc(-1 * var(--hi-movable-button-height)) 0 0 0`
* `0 0 0 calc(-1 * var(--hi-movable-button-width))`
* `calc(-1 * var(--hi-movable-button-height)) 0 0 calc(-1 * var(--hi-movable-button-width))`