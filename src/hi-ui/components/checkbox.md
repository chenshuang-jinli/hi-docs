---
titleTemplate: hi-ui - Checkbox - 复选框
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/checkbox
pageClass: demo-page
---

# Checkbox 复选框

一个简便易用的复选框。


## 单独使用

该组件可以单独使用，也可以放到 `<hi-checkbox-group>` 组件中进行组合使用，组合使用请查看 [hi-checkbox-group 组件](/hi-ui/components/checkbox-group)。
<br/>
单独使用此组件时，需将该组件的 `alone` 属性设置为 `true`，并且通过 `v-model` 绑定组件的选中状态。

```vue
<template>
    <hi-checkbox v-model="isChecked" alone label="单独使用"></hi-checkbox>
</template>

<script setup>
    import { ref } from "vue";

    // 选中状态
    const isChecked = ref(false);
</script>
```

另外一种单独使用的方式是通过 `checked` 属性绑定组件的选中状态，然后监听组件的点击事件手动控制选中状态。

```vue
<template>
    <hi-checkbox :checked="isChecked" label="单独使用" @click="handleClick"></hi-checkbox>
</template>

<script setup>
    import { ref } from "vue";

    // 选中状态
    const isChecked = ref(false);

    // 点击事件
    function handleClick() {
        isChecked.value = !isChecked.value;
    }
</script>
```


## 选择框文本

选择框的文本可通过 `label` 属性设置。

```vue
<hi-checkbox v-model="isChecked" alone label="选择框文本"></hi-checkbox>
```


## 边框

选择框默认无边框，可通过 `border` 属性开启边框。

```vue
<hi-checkbox v-model="isChecked" alone label="显示边框" border></hi-checkbox>
```


## 主题

通过 `theme` 属性设置主题。

```vue
<hi-checkbox v-model="isChecked" alone label="主题" theme="primary"></hi-checkbox>
<hi-checkbox v-model="isChecked" alone label="主题" theme="success"></hi-checkbox>
<hi-checkbox v-model="isChecked" alone label="主题" theme="warning"></hi-checkbox>
<hi-checkbox v-model="isChecked" alone label="主题" theme="error"></hi-checkbox>
<hi-checkbox v-model="isChecked" alone label="主题" theme="info"></hi-checkbox>
```


## 镂空

通过 `plain` 属性设置为镂空。

```vue
<hi-checkbox v-model="isChecked" alone label="镂空" plain></hi-checkbox>
<hi-checkbox v-model="isChecked" alone label="镂空" plain theme="primary"></hi-checkbox>
```


## 浅化背景

通过 `tint` 属性设置为浅化背景。

```vue
<hi-checkbox v-model="isChecked" alone label="浅化背景" tint></hi-checkbox>
<hi-checkbox v-model="isChecked" alone label="浅化背景" tint theme="primary"></hi-checkbox>
```


## 形状

通过 `circle` 属性可以将选择框设置为圆形。

```vue
<hi-checkbox v-model="isChecked" alone label="圆形" circle></hi-checkbox>
<hi-checkbox v-model="isChecked" alone label="圆形" circle theme="primary"></hi-checkbox>
```


## 禁用

通过 `disabled` 属性设置为禁用。

```vue
<hi-checkbox v-model="isChecked" alone label="禁用" disabled></hi-checkbox>
<hi-checkbox v-model="isChecked" alone label="禁用" disabled theme="primary"></hi-checkbox>
```


## 异步变更

通过 `async` 属性设置是否开启异步变更。
<br/>
开启异步变更后，会在变更完选中状态后，会触发 `asyncChange` 事件，需手动更新选中状态。

```vue
<template>
    <hi-checkbox v-model="isChecked" alone label="异步变更" async @asyncChange="onAsyncChange"></hi-checkbox>
</template>

<script setup>
    import { ref } from "vue";

    // 选中状态
    const isChecked = ref(false);

    // 异步变更事件
    function onAsyncChange(_isChecked) {
        console.log("当前选中状态 ->", _isChecked);
        uni.showModal({
            title: "提示",
            content: `是否更新选中状态`,
            showCancel: true,
            confirmText: "更新状态",
            cancelText: "保持不变",
            success: ({ confirm, cancel }) => {
                if (confirm) {
                    isChecked.value = !isChecked.value;
                }
            }
        });
    }
</script>
```


## 边框颜色

* 通过 `inactiveBorderColor` 属性设置未激活状态时的边框颜色。
* 通过 `activeBorderColor` 属性设置激活状态时的边框颜色。

```vue
<hi-checkbox v-model="isChecked" alone label="自定义边框颜色" inactiveBorderColor="red" activeBorderColor="blue"></hi-checkbox>
```


## 选择框颜色

* 通过 `inactiveColor` 属性设置未激活状态时的选择框颜色。
* 通过 `activeColor` 属性设置激活状态时的选择框颜色。

```vue
<hi-checkbox v-model="isChecked" alone label="自定义选择框颜色" inactiveColor="red" activeColor="blue"></hi-checkbox>
```


## 选择框大小

通过 `size` 属性设置选择框大小。

```vue
<hi-checkbox v-model="isChecked" alone label="自定义选择框大小" size="1.5em"></hi-checkbox>
```


## 文本颜色

* 通过 `labelColor` 属性设置未激活状态时的文本颜色。
* 通过 `activeLabelColor` 属性设置激活状态时的文本颜色。

```vue
<hi-checkbox v-model="isChecked" alone label="自定义文本颜色" labelColor="red" activeLabelColor="blue"></hi-checkbox>
```


## Props

| 属性名                | 说明                                                                                                          | 类型               | 默认值      | 可选值 | 版本 |
| :-------------------- | :------------------------------------------------------------------------------------------------------------ | :----------------- | :---------- | :----- | :--- |
| `hover`               | 指定按下去的样式类                                                                                            | `String`           | `hi-hover`  | -      | -    |
| `v-model`             | 选中状态                                                                                                      | `Boolean`          | `false`     | `true` | -    |
| `value`               | 绑定的值，在 `hi-checkbox-group` 中时使用，[参考](/hi-ui/components/hi-checkbox-group)                        | `[String, Number]` | `undefined` | -      | -    |
| `checked`             | 选中状态，此属性为了结局抖音小程序 `provide/inject` 不生效的问题，[参考](/hi-ui/components/hi-checkbox-group) | `Boolean`          | `false`     | `true` | -    |
| `alone`               | 是否单独使用                                                                                                  | `Boolean`          | `false`     | `true` | -    |
| `disabled`            | 是否禁用                                                                                                      | `Boolean`          | `false`     | `true` | -    |
| `async`               | 是否开启异步变更                                                                                              | `Boolean`          | `false`     | `true` | -    |
| `icon`                | 选择框图标                                                                                                    | `String`           | -           | -      | -    |
| `iconColor`           | 选择框图标颜色                                                                                                | `String`           | -           | -      | -    |
| `iconSize`            | 选择框图标大小                                                                                                | `String`           | -           | -      | -    |
| `label`               | 选项文本                                                                                                      | `String`           | -           | -      | -    |
| `labelColor`          | 未激活状态下的文本颜色                                                                                        | `String`           | -           | -      | -    |
| `activeLabelColor`    | 激活状态下的文本颜色                                                                                          | `String`           | -           | -      | -    |
| `labelFontSize`       | 文本字体大小                                                                                                  | `String`           | -           | -      | -    |
| `size`                | 选择框大小                                                                                                    | `String`           | -           | -      | -    |
| `inactiveColor`       | 未激活状态下的选择框颜色                                                                                      | `String`           | -           | -      | -    |
| `activeColor`         | 激活状态下的选择框颜色                                                                                        | `String`           | -           | -      | -    |
| `border`              | 是否显示边框                                                                                                  | `Boolean`          | `false`     | `true` | -    |
| `borderWidth`         | 边框宽度                                                                                                      | `String`           | -           | -      | -    |
| `inactiveBorderColor` | 未激活状态下的边框颜色                                                                                        | `String`           | -           | -      | -    |
| `activeBorderColor`   | 激活状态下的边框颜色                                                                                          | `String`           | -           | -      | -    |
| `radius`              | 选择框圆角大小                                                                                                | `String`           | -           | -      | -    |
| `circle`              | 是否为圆形选择框                                                                                              | `Boolean`          | `false`     | `true` | -    |
| `theme`               | 主题                                                                                                          | `String`           | -           | -      | -    |
| `plain`               | 是否镂空                                                                                                      | `Boolean`          | `false`     | `true` | -    |
| `tint`                | 是否浅化背景                                                                                                  | `String`           | -           | -      | -    |
| `tintOpacity`         | 浅化背景背景透明度                                                                                            | `String`           | -           | -      | -    |


## Event

| 事件名        | 说明             | 回调参数                      | 版本 |
| :------------ | :--------------- | :---------------------------- | :--- |
| `click`       | 点击事件         | -                             | -    |
| `change`      | 选中状态变更事件 | `isChecked`: 变更后的选中状态 | -    |
| `asyncChange` | 异步变更事件     | `isChecked`: 当前选中状态     | -    |


## Slots

| 名称      | 说明         | 参数 | 版本 |
| :-------- | :----------- | :--- | :--- |
| `default` | `label` 插槽 | -    | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                                | 默认值                                                                                                                                   | 版本 |
| :------------------------------------ | :--------------------------------------------------------------------------------------------------------------------------------------- | :--- |
| `--hi-checkbox-display`               | `inline-block`                                                                                                                           | -    |
| `--hi-checkbox-box-display`           | `inline-flex`                                                                                                                            | -    |
| `--hi-checkbox-box-vertical-align`    | `middle`                                                                                                                                 | -    |
| `--hi-checkbox-size`                  | `1.25em`                                                                                                                                 | -    |
| `--hi-checkbox-box-align-items`       | `center`                                                                                                                                 | -    |
| `--hi-checkbox-box-justify-content`   | `center`                                                                                                                                 | -    |
| `--hi-checkbox-box-flex-shrink`       | `0`                                                                                                                                      | -    |
| `--hi-checkbox-inactive-color`        | `var(--hi-background-default) / var(--hi-checkbox-theme, var(--hi-checkbox-inactive-color, var(--hi-background-default))) / transparent` | -    |
| `--hi-checkbox-box-position`          | `relative`                                                                                                                               | -    |
| `--hi-checkbox-border-width`          | `0 / 1px`                                                                                                                                | -    |
| `--hi-checkbox-border-style`          | `solid`                                                                                                                                  | -    |
| `--hi-checkbox-inactive-border-color` | `var(--hi-checkbox-theme, currentColor)`                                                                                                 | -    |
| `--hi-checkbox-border-radius`         | `2px / 50%`                                                                                                                              | -    |
| `--hi-checkbox-icon-font-size`        | `calc(var(--hi-checkbox-size, 1.25em) * 0.8)`                                                                                            | -    |
| `--hi-checkbox-transition`            | `100ms`                                                                                                                                  | -    |
| `--hi-checkbox-icon-color`            | `#ffffff / var(--hi-checkbox-theme, inherit) / var(--hi-checkbox-theme, #ffffff)`                                                        | -    |
| `--hi-checkbox-label-display`         | `inline-block`                                                                                                                           | -    |
| `--hi-checkbox-label-vertical-align`  | `middle`                                                                                                                                 | -    |
| `--hi-checkbox-label-color`           | `inherit`                                                                                                                                | -    |
| `--hi-checkbox-label-font-size`       | `inherit`                                                                                                                                | -    |
| `--hi-checkbox-label-margin`          | `0 0 0 5px`                                                                                                                              | -    |
| `--hi-checkbox-active-color`          | `var(--hi-theme-primary)`                                                                                                                | -    |
| `--hi-checkbox-active-border-color`   | `var(--hi-checkbox-theme, currentColor)`                                                                                                 | -    |
| `--hi-checkbox-active-label-color`    | `var(--hi-checkbox-label-color, inherit)`                                                                                                | -    |
| `--hi-checkbox-background-opacity`    | `0.2`                                                                                                                                    | -    |