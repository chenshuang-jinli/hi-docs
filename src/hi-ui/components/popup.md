---
titleTemplate: hi-ui - Popup - 弹出层
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/popup
pageClass: demo-page
---

# Popup 弹出层

弹出层容器，用于展示弹窗、信息提示等内容，支持上、下、左、右和中部弹出。


## 基础使用

* 通过 `show` 属性控制弹出层是否显示。
* 通过 `title` 属性设置标题。
* 通过 `默认 slot` 插入内容。
* 通过 `footer slot` 插入底部内容。

```vue
<template>
    <!-- 弹窗 -->
    <hi-popup :show="show" @close="show = false" title="将进酒·君不见">
        <view class="popup-content">
            <view>君不见，黄河之水天上来，奔流到海不复回。</view>
            <view>君不见，高堂明镜悲白发，朝如青丝暮成雪。</view>
            <view>人生得意须尽欢，莫使金樽空对月。</view>
            <view>天生我材必有用，千金散尽还复来。</view>
            <view>烹羊宰牛且为乐，会须一饮三百杯。</view>
            <view>岑夫子，丹丘生，将进酒，杯莫停。</view>
            <view>与君歌一曲，请君为我倾耳听。</view>
            <view>钟鼓馔玉不足贵，但愿长醉不愿醒。</view>
            <view>古来圣贤皆寂寞，惟有饮者留其名。</view>
            <view>陈王昔时宴平乐，斗酒十千恣欢谑。</view>
            <view>主人何为言少钱，径须沽取对君酌。</view>
            <view>五花马，千金裘，呼儿将出换美酒，与尔同销万古愁。</view>
        </view>
        <template #footer>
            <view class="footer">【作者】李白 【朝代】唐</view>
        </template>
    </hi-popup>

    <!-- 按钮 -->
    <hi-button @click="show = true">显示弹窗</hi-button>
</template>

<script setup>
    import { ref } from 'vue'

    // 显示状态
    const show = ref(false);
</script>

<style scoped>
.popup-content {
    padding: 2em;
    font-size: 14px;
}
</style>
```


## Header

弹窗 `Header` 包含左侧内容、标题和右侧内容。

* 左侧内容可以通过 `headerLeft` 插槽来自定义内容。
* 标题可以通过 `title` 属性设置或通过 `title` 插槽自定义内容。
* 右侧内容可以通过 `headerRight` 插槽自定义内容。

如果不想显示 `Header` ，可以设置 `header` 为 `false`。

```vue{2-8}
<template>
    <hi-popup :show="show" @close="show = false" title="将进酒·君不见" :header="false">
        <template #headerLeft>
            <text class="cancel">取消</text>
        </template>
        <template #headerRight>
            <text class="confirm">确定</text>
        </template>
        <view class="popup-content">
            <view>君不见，黄河之水天上来，奔流到海不复回。</view>
            <!-- ... -->
        </view>
    </hi-popup>
</template>
```


## Footer

* 通过 `footer` 插槽可以自定义底部内容。
* 通过 `footer` 属性可以控制是否显示底部内容。

```vue{7-9}
<template>
    <hi-popup :show="show" @close="show = false" title="将进酒·君不见">
        <view class="popup-content">
            <view>君不见，黄河之水天上来，奔流到海不复回。</view>
            <!-- ... -->
        </view>
        <template #footer>
            <view class="footer">【作者】李白 【朝代】唐</view>
        </template>
    </hi-popup>
</template>
```


## 内容显示位置

通过 `mode` 属性设置弹窗内容显示的位置，可设置的值有：
* `top`: 顶部居中；
* `bottom`: 底部居中；
* `left`: 左侧居中；
* `right`: 右侧居中。
* `center`: 中部居中。默认值。

```vue
<template>
    <!-- 弹窗 -->
    <hi-popup :show="show" @close="show = false" title="将进酒·君不见" :mode="mode">
        <view class="popup-content">
            <view>君不见，黄河之水天上来，奔流到海不复回。</view>
            <!-- ... -->
        </view>
    </hi-popup>

    <!-- 切换内容显示位置 -->
    <hi-button text="top" @click="handleClick('top')"></hi-button>
    <hi-button text="bottom" @click="handleClick('bottom')"></hi-button>
    <hi-button text="left" @click="handleClick('left')"></hi-button>
    <hi-button text="bottom" @click="handleClick('bottom')"></hi-button>
    <hi-button text="center" @click="handleClick('center')"></hi-button>
</template>

<script setup>
    import { ref } from 'vue'

    // 显示状态
    const show = ref(false);

    // 内容显示位置
    const mode = ref('center');

    // 按钮点击事件
    function handleClick(_mode) {
        mode.value = _mode;
        show.value = true;
    }
</script>
```


## 关闭按钮

* 通过 `close` 属性设置是否显示关闭按钮。
* 通过 `closeIcon` 属性设置关闭按钮图标。
* 通过 `closeColor` 属性设置关闭按钮颜色。
* 通过 `closeSize` 属性设置关闭按钮大小。
* 通过 `closeRight`、`closeTop`、`closeLeft`、`closeBottom` 属性设置关闭按钮距离右边、上边、左边、下边的距离。

```vue
<template>
    <!-- 弹窗 -->
    <hi-popup :show="show" @close="show = false" title="将进酒·君不见" closeIcon="close" coloseColor="#ff0000" closeSize="1.5em">
        <view class="popup-content">
            <view>君不见，黄河之水天上来，奔流到海不复回。</view>
            <!-- ... -->
        </view>
    </hi-popup>
</template>
```


## Props

| 属性名              | 说明                       | 类型      | 默认值      | 可选值     | 版本 |
| :------------------ | :------------------------- | :-------- | :---------- | :--------- | :--- |
| `hover`             | 指定关闭按钮按下去的样式类 | `String`  | `hi-hover`  | -          | -    |
| `show`              | 显示状态                   | `Boolean` | `false`     | `true`     | -    |
| `mode`              | 内容显示位置               | `String`  | `center`    | 见上方说明 | -    |
| `height`            | 内容高度                   | `String`  | -           | -          | -    |
| `maxHeight`         | 内容最大高度               | `String`  | -           | -          | -    |
| `width`             | 内容宽度                   | `String`  | -           | -          | -    |
| `maxWidth`          | 内容最大宽度               | `String`  | -           | -          | -    |
| `mask`              | 是否显示遮罩层             | `Boolean` | `true`      | `false`    | -    |
| `maskClickable`     | 遮罩层是否可点击           | `Boolean` | `true`      | `false`    | -    |
| `maskBg`            | 遮罩层背景颜               | `String`  | -           | -          | -    |
| `header`            | 是否显示 Header            | `Boolean` | `true`      | `false`    | -    |
| `title`             | 标题                       | `String`  | -           | -          | -    |
| `titleColor`        | 标题颜色                   | `String`  | -           | -          | -    |
| `titleFontSize`     | 标题字体大小               | `String`  | -           | -          | -    |
| `titleFontWeight`   | 标题字体粗细               | `String`  | -           | -          | -    |
| `close`             | 是否显示关闭按钮           | `Boolean` | `true`      | `false`    | -    |
| `closeIcon`         | 关闭按钮图标               | `String`  | `__shanchu` | -          | -    |
| `closeColor`        | 关闭按钮颜色               | `String`  | -           | -          | -    |
| `closeSize`         | 关闭按钮大小               | `String`  | -           | -          | -    |
| `closeRight`        | 关闭按钮距离右边距离       | `String`  | -           | -          | -    |
| `closeTop`          | 关闭按钮距离上边距离       | `String`  | -           | -          | -    |
| `closeLeft`         | 关闭按钮距离左边距离       | `String`  | -           | -          | -    |
| `closeBottom`       | 关闭按钮距离下边距离       | `String`  | -           | -          | -    |
| `footer`            | 是否显示 `Footer`          | `Boolean` | `true`      | `false`    | -    |
| `bg`                | 内容背景                   | `String`  | -           | -          | -    |
| `radius`            | 内容圆角大小               | `String`  | -           | -          | -    |
| `headerHeight`      | `Header` 高度              | `String`  | -           | -          | -    |
| `headerBorder`      | 是否显示 `Header` 下边框   | `Boolean` | `false`     | `true`     | -    |
| `headerBorderColor` | `Header` 下边框颜色        | `String`  | -           | -          | -    |
| `headerBorderWidth` | `Header` 下边框宽度        | `String`  | -           | -          | -    |
| `headerBorderStyle` | `Header` 下边框类型        | `String`  | -           | -          | -    |
| `footerHeight`      | `Footer` 高度              | `String`  | -           | -          | -    |
| `footerBorder`      | 是否显示 `Footer` 上边框   | `Boolean` | `false`     | `true`     | -    |
| `footerBorderColor` | `Footer` 上边框颜色        | `String`  | -           | -          | -    |
| `footerBorderWidth` | `Footer` 上边框宽度        | `String`  | -           | -          | -    |
| `footerBorderStyle` | `Footer` 上边框类型        | `String`  | -           | -          | -    |


## Event

| 事件名          | 说明                             | 回调参数 | 版本 |
| :-------------- | :------------------------------- | :------- | :--- |
| `close`         | 点击关闭按钮或遮罩触发的关闭事件 | -        | -    |
| `scroll`        | 内容滚动事件                     | -        | -    |
| `scrolltolower` | 内容滚动到底部触发事件           | -        | -    |


## Slots

| 名称          | 说明                                  | 参数 | 版本 |
| :------------ | :------------------------------------ | :--- | :--- |
| `default`     | 内容插槽                              | -    | -    |
| `headerLeft`  | `Header` 左侧内容插槽                 | -    | -    |
| `headerRight` | `Header` 右侧内容插槽                 | -    | -    |
| `title`       | 标题插槽                              | -    | -    |
| `close`       | 关闭按钮插槽                          | -    | -    |
| `footer`      | `Footer` 内容插槽                     | -    | -    |
| `top`         | 顶部（`Header` 和内容之间的部分）插槽 | -    | -    |
| `bottom`      | 底部（`Footer` 和内容之间的部分）插槽 | -    | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                                         | 默认值                                                                      | 版本 |
| :--------------------------------------------- | :-------------------------------------------------------------------------- | :--- |
| `--hi-popup-mask-position`                     | `fixed`                                                                     | -    |
| `--hi-popup-mask-width`                        | `100%`                                                                      | -    |
| `--hi-popup-mask-height`                       | `100%`                                                                      | -    |
| `--hi-popup-mask-top`                          | `0`                                                                         | -    |
| `--hi-popup-mask-left`                         | `0`                                                                         | -    |
| `--hi-popup-mask-right`                        | `0`                                                                         | -    |
| `--hi-popup-mask-bottom`                       | `0`                                                                         | -    |
| `--hi-popup-mask-z-index`                      | `var(--hi-index-upper)`                                                     | -    |
| `--hi-popup-mask-background`                   | `var(--hi-background-overlay)`                                              | -    |
| `--hi-popup-mask-overflow`                     | `hidden`                                                                    | -    |
| `--hi-popup-mask-animation-duration`           | `300ms`                                                                     | -    |
| `--hi-popup-mask-animation-timing-function`    | `linear`                                                                    | -    |
| `--hi-popup-mask-animation-fill-mode`          | `forwards`                                                                  | -    |
| `--hi-popup-content-width`                     | `100% / auto`                                                               | -    |
| `--hi-popup-content-max-width`                 | `80% / 100%`                                                                | -    |
| `--hi-popup-content-height`                    | `auto`                                                                      | -    |
| `--hi-popup-content-max-height`                | `80% / 100%`                                                                | -    |
| `--hi-popup-content-background`                | `var(--hi-background-element)`                                              | -    |
| `--hi-popup-content-border-radius`             | `10px / 0 0 10px 10px / 10px 10px 0 0 / 0 10px 10px 0 / 10px 0 0 10px`      | -    |
| `--hi-popup-content-display`                   | `flex`                                                                      | -    |
| `--hi-popup-content-flex-direction`            | `column`                                                                    | -    |
| `--hi-popup-content-overflow`                  | `hidden`                                                                    | -    |
| `--hi-popup-content-position`                  | `fixed`                                                                     | -    |
| `--hi-popup-content-left`                      | `auto / 50% / 0`                                                            | -    |
| `--hi-popup-content-top`                       | `auto / 50% / 0`                                                            | -    |
| `--hi-popup-content-right`                     | `auto / 0`                                                                  | -    |
| `--hi-popup-content-bottom`                    | `auto / 0`                                                                  | -    |
| `--hi-popup-content-transform`                 | `initial / translate(-50%, -50%) / translate(-50%, 0) / translate(0, -50%)` | -    |
| `--hi-popup-content-z-index`                   | `var(--hi-index-upper)`                                                     | -    |
| `--hi-popup-content-padding`                   | `0`                                                                         | -    |
| `--hi-popup-content-animation-duration`        | `300ms`                                                                     | -    |
| `--hi-popup-content-animation-timing-function` | `linear`                                                                    | -    |
| `--hi-popup-content-animation-fill-mode`       | `forwards`                                                                  | -    |
| `--hi-popup-header-height`                     | `auto`                                                                      | -    |
| `--hi-popup-header-padding`                    | `12px 15px`                                                                 | -    |
| `--hi-popup-header-text-align`                 | `center`                                                                    | -    |
| `--hi-popup-header-font-size`                  | `1.25em`                                                                    | -    |
| `--hi-popup-header-font-weight`                | `700`                                                                       | -    |
| `--hi-popup-header-position`                   | `relative`                                                                  | -    |
| `--hi-popup-header-display`                    | `flex`                                                                      | -    |
| `--hi-popup-header-align-items`                | `center`                                                                    | -    |
| `--hi-popup-header-justify-content`            | `center`                                                                    | -    |
| `--hi-popup-header-flex-shrink`                | `0`                                                                         | -    |
| `--hi-popup-header-border-width`               | `0`                                                                         | -    |
| `--hi-popup-header-border-style`               | `solid`                                                                     | -    |
| `--hi-popup-header-border-color`               | `var(--hi-border-color)`                                                    | -    |
| `--hi-popup-title-color`                       | `inherit`                                                                   | -    |
| `--hi-popup-title-font-size`                   | `inherit`                                                                   | -    |
| `--hi-popup-title-font-weight`                 | `inherit`                                                                   | -    |
| `--hi-popup-title-flex`                        | `1`                                                                         | -    |
| `--hi-popup-close-position`                    | `absolute`                                                                  | -    |
| `--hi-popup-close-color`                       | `inherit`                                                                   | -    |
| `--hi-popup-close-size`                        | `1.25em`                                                                    | -    |
| `--hi-popup-close-right`                       | `10px`                                                                      | -    |
| `--hi-popup-close-left`                        | `auto`                                                                      | -    |
| `--hi-popup-close-top`                         | `15px`                                                                      | -    |
| `--hi-popup-close-bottom`                      | `auto`                                                                      | -    |
| `--hi-popup-close-z-index`                     | `8`                                                                         | -    |
| `--hi-popup-close-display`                     | `flex`                                                                      | -    |
| `--hi-popup-close-align-items`                 | `center`                                                                    | -    |
| `--hi-popup-close-justify-content`             | `center`                                                                    | -    |
| `--hi-popup-close-font-weight`                 | `700`                                                                       | -    |
| `--hi-popup-body-flex`                         | `1`                                                                         | -    |
| `--hi-popup-body-overflow`                     | `hidden`                                                                    | -    |
| `--hi-popup-body-display`                      | `flex`                                                                      | -    |
| `--hi-popup-scroll-view-width`                 | `100%`                                                                      | -    |
| `--hi-popup-footer-flex-shrink`                | `0`                                                                         | -    |
| `--hi-popup-footer-display`                    | `flex`                                                                      | -    |
| `--hi-popup-footer-align-items`                | `center`                                                                    | -    |
| `--hi-popup-footer-height`                     | `auto`                                                                      | -    |
| `--hi-popup-footer-border-width`               | `0`                                                                         | -    |
| `--hi-popup-footer-border-style`               | `solid`                                                                     | -    |
| `--hi-popup-footer-border-color`               | `var(--hi-border-color)`                                                    | -    |
