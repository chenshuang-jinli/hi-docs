---
titleTemplate: hi-ui - AvatarGroup - 头像组
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/avatar-group
pageClass: demo-page
---

# AvatarGroup 头像组

本组件一般用于展示一组用户头像的地方，如活动参与用户头像，群组用户头像列表等。


## 基础使用

* 通过 `list` 属性设置头像列表。
* 通过 `keyName` 属性设置头像图片资源地址属性的名称。

```vue
<template>
    <hi-avatar-group :list="avatars" keyName="avatar"></hi-avatar-group>
</template>

<script setup>
    import { ref } from "vue";

    // 头像列表
    const avatars = ref([
        { avatar: "/static/images/avatar1.jpeg" },
        { avatar: "/static/images/avatar2.jpeg" },
        { avatar: "/static/images/avatar3.jpeg" },
        { avatar: "/static/images/avatar4.jpeg" },
        { avatar: "/static/images/avatar5.jpeg" },
        { avatar: "/static/images/avatar6.jpeg" },
        { avatar: "/static/images/avatar7.jpeg" },
        { avatar: "/static/images/avatar8.jpeg" },
        { avatar: "/static/images/avatar9.jpeg" },
    ]);
</script>
```


## 展示的数量

通过 `count` 属性设置展示的数量。

```vue
<hi-avatar-group :list="avatars" keyName="avatar" :count="5"></hi-avatar-group>
```


## 显示更多

通过 `more` 属性设置是否显示更多按钮。

```vue
<hi-avatar-group :list="avatars" keyName="avatar" more></hi-avatar-group>
```


## 开启动画

通过 `animation` 属性设置是否开启动画。

```vue
<hi-avatar-group :list="avatars" keyName="avatar" more animation></hi-avatar-group>
```

> [!TIP] 提示
> 有的平台动画可能会有抖动。如果出现抖动，可以通过调整 `time` 属性值的大小来消除抖动。


## 头像大小

通过 `size` 属性设置头像大小。


```vue
<hi-avatar-group :list="avatars" keyName="avatar" size="20px"></hi-avatar-group>
```


## 头像偏移量

通过 `offset` 属性设置头像偏移量，一般为一个负值。

```vue
<hi-avatar-group :list="avatars" keyName="avatar" size="20px" offset="-8px"></hi-avatar-group>
```


## Props

| 属性名            | 说明                                                               | 类型               | 默认值        | 可选值                                                    | 版本 |
| :---------------- | :----------------------------------------------------------------- | :----------------- | :------------ | :-------------------------------------------------------- | :--- |
| `list`            | 头像列表                                                           | `Array<Object>`    | `[]`          | -                                                         | -    |
| `keyName`         | 头像图片资源地址属性的名称                                         | `String`           | `avatar`      | -                                                         | -    |
| `count`           | 展示的数量                                                         | `Number`           | `5`           | -                                                         | -    |
| `animation`       | 是否开启动画                                                       | `Boolean`          | `false`       | `true`                                                    | -    |
| `delay`           | 动画间隔时间，单位毫秒                                             | `[Number, String]` | `3000`        | -                                                         | -    |
| `mode`            | 图片的裁剪模式                                                     | `String`           | `scaleToFill` | [参考](https://uniapp.dcloud.net.cn/component/image.html) | -    |
| `more`            | 是否显示更多按钮                                                   | `Boolean`          | `false`       | `true`                                                    | -    |
| `time`            | 优化开启动画后抖动的一个属性，一般可以通过调整此值的大小来消除抖动 | `[Number, String]` | `0`           | -                                                         | -    |
| `size`            | 头像大小                                                           | `String`           | -             | -                                                         | -    |
| `offset`          | 头像偏移量                                                         | `String`           | -             | -                                                         | -    |
| `moreIcon`        | 更多按钮的图标                                                     | `String`           | `__gengduo`   | -                                                         | -    |
| `itemBorder`      | 是否显示头像边框                                                   | `Boolean`          | `false`       | `true`                                                    | -    |
| `itemBorderColor` | 头像边框颜色                                                       | `String`           | -             | -                                                         | -    |
| `itemRadius`      | 头像圆角大小                                                       | `String`           | -             | -                                                         | -    |
| `itemBg`          | 头像背景                                                           | `String`           | -             | -                                                         | -    |
| `moreBorder`      | 是否显示更多按钮边框                                               | `Boolean`          | `false`       | `true`                                                    | -    |
| `moreBorderColor` | 更多按钮边框颜色                                                   | `String`           | -             | -                                                         | -    |
| `moreRadius`      | 更多按钮圆角大小                                                   | `String`           | -             | -                                                         | -    |
| `moreColor`       | 更多按钮颜色                                                       | `String`           | -             | -                                                         | -    |
| `moreFontSize`    | 更多按钮字体大小                                                   | `String`           | -             | -                                                         | -    |
| `moreBg`          | 更多按钮背景                                                       | `String`           | -             | -                                                         | -    |


## Event

| 事件名  | 说明             | 回调参数                              | 版本 |
| :------ | :--------------- | :------------------------------------ | :--- |
| `click` | 头像点击事件     | `item`: 头像数据。`index`: 头像下标。 | -    |
| `more`  | 更多按钮点击事件 | -                                     | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                                             | 默认值                         | 版本 |
| :------------------------------------------------- | :----------------------------- | :--- |
| `--hi-avatar-group-display`                        | `inline-flex`                  | -    |
| `--hi-avatar-group-align-items`                    | `center`                       | -    |
| `--hi-avatar-group-size`                           | `25px`                         | -    |
| `--hi-avatar-group-item-border-radius`             | `50%`                          | -    |
| `--hi-avatar-group-item-overflow`                  | `hidden`                       | -    |
| `--hi-avatar-group-item-position`                  | `relative`                     | -    |
| `--hi-avatar-group-item-z-index`                   | `1`                            | -    |
| `--hi-avatar-group-item-flex-shrink`               | `0`                            | -    |
| `--hi-avatar-group-item-background`                | `var(--hi-background-element)` | -    |
| `--hi-avatar-group-item-border-width`              | `0 / 1px`                      | -    |
| `--hi-avatar-group-item-border-style`              | `solid`                        | -    |
| `--hi-avatar-group-item-border-color`              | `currentColor`                 | -    |
| `--hi-avatar-group-item-animation-timing-function` | `linear`                       | -    |
| `--hi-avatar-group-item-animation-duration`        | `500ms`                        | -    |
| `--hi-avatar-group-item-animation-fill-mode`       | `forwards`                     | -    |
| `--hi-avatar-group-offset`                         | `-10px`                        | -    |
| `--hi-avatar-group-more-background`                | `var(--hi-background-default)` | -    |
| `--hi-avatar-group-more-display`                   | `flex`                         | -    |
| `--hi-avatar-group-more-align-items`               | `center`                       | -    |
| `--hi-avatar-group-more-justify-content`           | `center`                       | -    |
| `--hi-avatar-group-more-border-width`              | `0 / 1px`                      | -    |
| `--hi-avatar-group-more-border-style`              | `solid`                        | -    |
| `--hi-avatar-group-more-border-color`              | `currentColor`                 | -    |
| `--hi-avatar-group-more-border-radius`             | `50%`                          | -    |
| `--hi-avatar-group-more-font-size`                 | `inerit`                       | -    |
| `--hi-avatar-group-more-color`                     | `inherit`                      | -    |