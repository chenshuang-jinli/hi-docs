---
titleTemplate: hi-ui - Swiper - 轮播
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/swiper
pageClass: demo-page
---

# Swiper 轮播

此组件只是对 `uni-app swiper` 组件的再简单封装，可以方便的通过数据快速创建一个轮播。


## 基础使用

* 通过 `list` 属性设置轮播数据。
* 通过 `keyName` 设置数据中表示图片路径的字段名。

```vue
<template>
    <hi-swiper :list="list" keyName="url"></hi-swiper>
</template>

<script setup>
    import { ref } from "vue";

    // 轮播数据
    const list = ref([
        { id: 1, url: "/static/images/temp.jpeg" },
        { id: 2, url: "/static/images/temp.jpeg" },
        { id: 3, url: "/static/images/temp.jpeg" },
        { id: 4, url: "/static/images/temp.jpeg" },
        { id: 5, url: "/static/images/temp.jpeg" }
    ]);
</script>
```


## 圆角大小

通过 `radius` 属性设置组件圆角大小。

```vue
<template>
    <hi-swiper :list="list" keyName="url" radius="10px"></hi-swiper>
</template>
```


## Props

| 属性名                         | 说明                                                                            | 类型            | 默认值                    | 可选值                                                     | 版本 |
| :----------------------------- | :------------------------------------------------------------------------------ | :-------------- | :------------------------ | :--------------------------------------------------------- | :--- |
| `hover`                        | 指定按下去的样式类                                                              | `String`        | `hi-hover`                | -                                                          | -    |
| `width`                        | 宽度                                                                            | `String`        | -                         | -                                                          | -    |
| `height`                       | 高度                                                                            | `String`        | -                         | -                                                          | -    |
| `radius`                       | 圆角大小                                                                        | `String`        | -                         | -                                                          | -    |
| `list`                         | 轮播数据                                                                        | `Array<Object>` | -                         | -                                                          | -    |
| `keyName`                      | 数据中表示图片路径的字段名                                                      | `String`        | `url`                     | -                                                          | -    |
| `current`                      | 当前轮播的索引                                                                  | `Number`        | `0`                       | -                                                          | -    |
| `indicatorDots`                | 是否显示指示点                                                                  | `Boolean`       | `false`                   | `true`                                                     | -    |
| `indicatorColor`               | 指示点颜色                                                                      | `String`        | `rgba(0, 0, 0, .3)`       | -                                                          | -    |
| `indicatorActiveColor`         | 指示点激活颜色                                                                  | `String`        | `var(--hi-theme-primary)` | -                                                          | -    |
| `mode`                         | 图片                                                                            | `String`        | `aspectFill`              | [参考](https://uniapp.dcloud.net.cn/component/image.html)  | -    |
| `lazyLoad`                     | 是否懒加载图片                                                                  | `Boolean`       | `true`                    | [参考](https://uniapp.dcloud.net.cn/component/image.html)  | -    |
| `fadeShow`                     | 是否显示动画效果                                                                | `Boolean`       | `true`                    | [参考](https://uniapp.dcloud.net.cn/component/image.html)  | -    |
| `webp`                         | 在系统不支持 `webp` 的情况下是否单独启用 `webp`                                 | `Boolean`       | `true`                    | [参考](https://uniapp.dcloud.net.cn/component/image.html)  | -    |
| `showMenuByLongpress`          | 开启长按图片显示识别小程序码菜单                                                | `Boolean`       | `true`                    | [参考](https://uniapp.dcloud.net.cn/component/image.html)  | -    |
| `draggable`                    | 是否能拖动图片                                                                  | `Boolean`       | `false`                   | [参考](https://uniapp.dcloud.net.cn/component/image.html)  | -    |
| `activeClass`                  | `swiper-item` 可见时的 `class`                                                  | `String`        | -                         | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |
| `changingClass`                | `acceleration` 设置为 `true` 时且处于滑动过程中，中间若干屏处于可见时的 `class` | `String`        | -                         | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |
| `autoplay`                     | 是否自动播放                                                                    | `Boolean`       | `false`                   | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |
| `currentItemId`                | 当前所在滑块的 `item-id` ，不能与 `current` 被同时指定                          | `String`        | -                         | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |
| `interval`                     | 自动播放间隔时间，单位 `ms`                                                     | `Number`        | `5000`                    | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |
| `duration`                     | 滑动动画时长，单位 `ms`                                                         | `Number`        | `500`                     | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |
| `circular`                     | 是否衔接滑动                                                                    | `Boolean`       | `true`                    | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |
| `vertical`                     | 滑动方向是否为纵向                                                              | `Boolean`       | `false`                   | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |
| `previousMargin`               | 前边距，可用于露出前一项的一小部分，接受 `px` 和 `rpx` 值                       | `Number`        | `0rpx`                    | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |
| `nextMargin`                   | 后边距，可用于露出后一项的一小部分，接受 `px` 和 `rpx` 值                       | `Number`        | `0rpx`                    | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |
| `acceleration`                 | 当开启时，会根据滑动速度，连续滑动多屏                                          | `Boolean`       | `false`                   | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |
| `disableProgrammaticAnimation` | 是否禁用代码变动触发 `swiper` 切换时使用动画                                    | `Boolean`       | `false`                   | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |
| `displayMultipleItems`         | 同时显示的滑块数量                                                              | `Number`        | `1`                       | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |
| `skipHiddenItemLayout`         | 是否跳过未显示的滑块布局                                                        | `Boolean`       | `false`                   | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |
| `disableTouch`                 | 是否禁止用户 `touch` 操作                                                       | `Boolean`       | `false`                   | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |
| `touchable`                    | 是否监听用户的触摸事件，只在初始化时有效，不能动态变更                          | `Boolean`       | `true`                    | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |
| `easingFunction`               | 指定 `swiper` 切换缓动动画类型                                                  | `String`        | `default`                 | [参考](https://uniapp.dcloud.net.cn/component/swiper.html) | -    |


## Event

| 事件名            | 说明                                                   | 回调参数                                                                            | 版本 |
| :---------------- | :----------------------------------------------------- | :---------------------------------------------------------------------------------- | :--- |
| `itemClick`       | `swiper-item` 点击事件                                 | `item`: 当前点击的 `item` 的数据。`index`: 当前点击的 `item` 的索引值。             | -    |
| `change`          | `current` 改变时会触发 `change` 事件                   | [参考](https://uniapp.dcloud.net.cn/component/swiper.html)                          | -    |
| `transition`      | `swiper-item` 的位置发生改变时会触发 `transition` 事件 | [参考](https://uniapp.dcloud.net.cn/component/swiper.html)                          | -    |
| `animationfinish` | 动画结束时会触发 `animationfinish` 事件                | [参考](https://uniapp.dcloud.net.cn/component/swiper.html)                          | -    |
| `error`           | 图片加载失败时触发                                     | `item`: 加载失败图片所属 `item` 的数据。`index`: 加载失败图片所属 `item` 的索引值。 | -    |
| `load`            | 图片加载完成时触发                                     | `item`: 加载完成图片所属 `item` 的数据。`index`: 加载完成图片所属 `item` 的索引值。 | -    |


## Slots

| 名称        | 说明                | 参数                                                       | 版本 |
| :---------- | :------------------ | :--------------------------------------------------------- | :--- |
| `default`   | `absolute` 内容插槽 | `item`: 当前 `item` 数据。`index`: 当前 `item` 索引。      | -    |
| `indicator` | 轮播指示器插槽      | `current`: 当前激活项的索引。`count`: `swiper-item` 数量。 | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                          | 默认值     | 版本 |
| :------------------------------ | :--------- | :--- |
| `--hi-swiper-width`             | `100%`     | -    |
| `--hi-swiper-height`            | `360rpx`   | -    |
| `--hi-swiper-position`          | `relative` | -    |
| `--hi-swiper-display`           | `flex`     | -    |
| `--hi-swiper-border-radius`     | `0`        | -    |
| `--hi-swiper-overflow`          | `initial`  | -    |
| `--hi-swiper-content-left`      | `0`        | -    |
| `--hi-swiper-content-top`       | `0`        | -    |
| `--hi-swiper-content-right`     | `0`        | -    |
| `--hi-swiper-content-bottom`    | `0`        | -    |
| `--hi-swiper-content-transform` | `initial`  | -    |
| `--hi-swiper-content-width`     | `100%`     | -    |
| `--hi-swiper-content-height`    | `100%`     | -    |
| `--hi-swiper-content-z-index`   | `8`        | -    |
