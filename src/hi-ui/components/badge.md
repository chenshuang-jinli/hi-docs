---
titleTemplate: hi-ui - Badge - 徽标数
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/badge
pageClass: demo-page
---

# Badge 徽标数

该组件一般用于图标右上角显示未读的消息数量，该组件有多种模式，具体查看下方的文档。
<br/>
为了用户更好的自定义布局，该组件默认未开启 `absolute` 模式。


## 基础使用

通过 `value` 属性设置显示的数字/文字。

```vue
<hi-badge :value="10" />
```


## 边框

通过 border 属性设置是否显示边框。

```vue
<hi-badge :value="10" border />
```

* 边框默认宽度：`1px`
* 边框默认颜色：`currentColor`


## 主题

通过 `theme` 属性设置组件主题。

```vue
<hi-badge :value="10" theme="primary" />
<hi-badge :value="10" theme="success" />
<hi-badge :value="10" theme="warning" />
<hi-badge :value="10" theme="error" />
<hi-badge :value="10" theme="info" />
```

> [!TIP] 提示
> 设置主题后，文本颜色会自动设置为 `#ffffff`，如果需要修改，可以通过 `color` 属性设置。


## 镂空

通过 `plain` 属性设置为镂空。
<br/>
设置为镂空后，背景会自动设置为 `transparent`，如果需要修改，可以通过 `bg` 属性设置。
<br/>
同时镂空时会默认显示边框。

```vue
<hi-badge :value="10" plain />
<hi-badge :value="10" plain theme="primary" />
```


## 浅化

通过 `tint` 属性设置浅化背景。
<br/>
浅化背景即是：为组件的背景设置透明度，默认为 `0.2` 透明度。

```vue
<hi-badge :value="10" tint />
<hi-badge :value="10" tint theme="primary" />
```


## 圆点模式

通过将 `mode` 属性设置为 `dot` 开启圆点模式。

```vue
<hi-badge :value="10" mode="dot" theme="primary" />
```

* 圆点模式下，内容不显示。
* 圆点模式下，默认大小为 `8px`，可以通过 `dotSize` 或 `height` 属性调整大小。


## overflow 模式

通过将 `mode` 属性设置为 `overflow` 开启 `overflow` 模式。

`overflow` 下，除了需要设置 `value` 外，还需要设置 `max` 属性，`max` 表示显示的最大值，超过这个值会显示为 `{max}+`。`max` 默认为 `99`。

```vue
<hi-badge :value="108" mode="overflow" :limit="99" theme="primary" />
```


## ellipsis 模式

通过将 `mode` 属性设置为 `ellipsis` 开启 `ellipsis` 模式。`ellipsis` 下，除了需要设置 `value` 外，还需要设置 `max` 属性，`max` 表示显示的最大值，超过这个值会显示为 `{max}...`。`max` 默认为 `99`。

```vue
<hi-badge :value="108" mode="ellipsis" :limit="99" theme="primary" />
```


## limit 模式

通过将 `mode` 属性设置为 `limit` 开启 `limit` 模式。`limit` 下，除了需要设置 `value` 外，还需要设置 `limit` 和 `suffix` 属性，以 `limit` 属性的值作为判断条件，值大于 `limit` 时显示 `${value / limit} + ${suffix}`。

* `limit` 默认值：`1000`
* `suffix` 默认值：`k`

```vue
<hi-badge :value="1568" mode="limit" theme="primary" />
<hi-badge :value="15688" mode="limit" :limit="10000" suffix="w" theme="success" />
```


## Props

| 属性名            | 说明                                    | 类型               | 默认值  | 可选值                    | 版本 |
| :---------------- | :-------------------------------------- | :----------------- | :------ | :------------------------ | :--- |
| `hover`           | 指定按下去的样式类                      | `String`           | ``      | -                         | -    |
| `value`           | 显示的数字/文字                         | `String`           | -       | -                         | -    |
| `zero`            | 当 `value` 为 `0` 时，是否显示          | `Boolean`          | `false` | `true`                    | -    |
| `mode`            | 显示模式                                | `String`           | -       | 参考上方的模式说明        | -    |
| `max`             | 最大值                                  | `[Number, String]` | `99`    | 参考上方的模式说明        | -    |
| `limit`           | 分割值                                  | `[Number, String]` | `1000`  | 参考上方的模式说明        | -    |
| `suffix`          | 分割值后缀                              | `String`           | `k`     | 参考上方的模式说明        | -    |
| `decimals`        | `limit` 后要保留的小数位数              | `Number`           | `2`     | -                         | -    |
| `discardLastZero` | `limit` 后是否舍弃末尾为 `0` 的小数位数 | `Boolean`          | `true`  | `false`                   | -    |
| `absolute`        | 是否开启绝对定位模式                    | `Boolean`          | `false` | `true`                    | -    |
| `top`             | 组件的 `top` 样式属性的值               | `String`           | -       | -                         | -    |
| `bottom`          | 组件的 `bottom` 样式属性的值            | `String`           | -       | -                         | -    |
| `left`            | 组件的 `left` 样式属性的值              | `String`           | -       | -                         | -    |
| `right`           | 组件的 `right` 样式属性的值             | `String`           | -       | -                         | -    |
| `transform`       | 组件的 `transform` 样式属性的值         | `String`           | -       | -                         | -    |
| `theme`           | 组件的主题                              | `String`           | -       | [参考](/hi-ui/style#主题) | -    |
| `plain`           | 是否镂空                                | `Boolean`          | `false` | `true`                    | -    |
| `tint`            | 是否浅化背景                            | `Boolean`          | `false` | `true`                    | -    |
| `tintOpacity`     | 背景透明度                              | `[Number, String]` | `0.2`   | -                         | -    |
| `bg`              | 背景                                    | `String`           | -       | -                         | -    |
| `color`           | 文本颜色                                | `String`           | -       | -                         | -    |
| `fontSize`        | 文本大小                                | `String`           | -       | -                         | -    |
| `radius`          | 圆角大小                                | `String`           | -       | -                         | -    |
| `width`           | 宽度                                    | `String`           | -       | -                         | -    |
| `height`          | 高度                                    | `String`           | -       | -                         | -    |
| `dotSize`         | 圆点大小                                | `String`           | -       | -                         | -    |
| `border`          | 是否显示边框                            | `Boolean`          | `false` | `true`                    | -    |
| `borderWidth`     | 边框宽度                                | `String`           | -       | -                         | -    |
| `borderColor`     | 边框颜色                                | `String`           | -       | -                         | -    |
| `borderStyle`     | 边框类型                                | `String`           | -       | -                         | -    |


## Event

| 事件名  | 说明         | 回调参数 | 版本 |
| :------ | :----------- | :------- | :--- |
| `click` | 组件点击事件 | -        | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                          | 默认值                               | 版本 |
| :------------------------------ | :----------------------------------- | :--- |
| `--hi-badge-background`         | `var(--hi-background-default, none)` | -    |
| `--hi-badge-color`              | `inherit`                            | -    |
| `--hi-badge-border-radius`      | `100px / 50%`                        | -    |
| `--hi-badge-padding`            | `0 10px / 0`                         | -    |
| `--hi-badge-position`           | `relative / absolute`                | -    |
| `--hi-badge-display`            | `inline-flex`                        | -    |
| `--hi-badge-align-items`        | `center`                             | -    |
| `--hi-badge-justify-content`    | `center`                             | -    |
| `--hi-badge-font-size`          | `0.8em`                              | -    |
| `--hi-badge-width`              | `auto / 8px`                         | -    |
| `--hi-badge-height`             | `auto / 8px`                         | -    |
| `--hi-badge-z-index`            | `8`                                  | -    |
| `--hi-badge-left`               | `auto`                               | -    |
| `--hi-badge-right`              | `auto`                               | -    |
| `--hi-badge-top`                | `auto`                               | -    |
| `--hi-badge-bottom`             | `auto`                               | -    |
| `--hi-badge-transform`          | `initial`                            | -    |
| `--hi-badge-background-opacity` | `0.2`                                | -    |
