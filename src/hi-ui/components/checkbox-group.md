---
titleTemplate: hi-ui - CheckboxGroup - 复选框组
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/checkbox-group
pageClass: demo-page
---

# CheckboxGroup 复选框组

此组件通常用于组合一组复选框进行组合使用。

> [!WARNING]
> 由于在抖音小程序中 `provide/inject` 存在 `BUG`，所以在抖音小程序中无法通过 `v-model` 来绑定组件中复选框的的选中状态，需手动通过 `hi-checkbox` 的 `checked` 属性来控制组中复选框的选中状态。<br/>
> [关于此 `BUG` 的帖子](https://ask.dcloud.net.cn/question/193727)


## 基础使用

通过 `v-model` 属性绑定组中选中复选框的值。

```vue
<template>
    <hi-checkbox-group v-model="values">
        <hi-checkbox
            v-for="item in options"
            :key="item.value"
            :label="item.label"
            :value="item.value"
        ></hi-checkbox>
    </hi-checkbox-group>
</template>

<script setup>
    import { ref } from "vue";

    // 选项数据
    const options = ref([
        { label: "鸡蛋", value: 1 },
        { label: "鸭蛋", value: 2 },
        { label: "鹅蛋", value: 3 },
        { label: "大米", value: 4 },
        { label: "小米", value: 5 },
        { label: "苹果", value: 6 },
        { label: "香蕉", value: 7 },
        { label: "橘子", value: 8 },
        { label: "葡萄", value: 9 },
        { label: "草莓", value: 10 },
    ]);

    // 选中项的值
    const values = ref([2, 3]);
</script>
```


## 异步变更

通过 `async` 属性设置是否开启异步变更。
<br/>
开启异步变更后，会在变更完选中状态后，会触发 `asyncChange` 事件，需手动更新选中状态。

```vue
<template>
    <hi-checkbox-group v-model="values" @asyncChange="onAsyncChange">
        <hi-checkbox
            v-for="item in options"
            :key="item.value"
            :label="item.label"
            :value="item.value"
        ></hi-checkbox>
    </hi-checkbox-group>
</template>

<script setup>
    import { ref } from "vue";

    // 选项数据
    const options = ref([
        { label: "鸡蛋", value: 1 },
        { label: "鸭蛋", value: 2 },
        { label: "鹅蛋", value: 3 },
        { label: "大米", value: 4 },
        { label: "小米", value: 5 },
        { label: "苹果", value: 6 },
        { label: "香蕉", value: 7 },
        { label: "橘子", value: 8 },
        { label: "葡萄", value: 9 },
        { label: "草莓", value: 10 },
    ]);

    // 选中项的值
    const values = ref([2, 3]);

    // 异步变更事件
    function onAsyncChange(value, isChecked) {
        console.log("选项的值 ->", value);
        console.log("选项的选中状态 ->", isChecked);
        uni.showModal({
            title: "提示",
            content: `是否更新选中状态`,
            showCancel: true,
            confirmText: "更新状态",
            cancelText: "保持不变",
            success: ({ confirm, cancel }) => {
                if (confirm) {
                    if(isChecked) {
                        values.value.splice(values.value.indexOf(value), 1);
                    } else {
                        values.value.push(value);
                    }
                }
            }
        });
    }
</script>
```


## Props

| 属性名    | 说明             | 类型                    | 默认值  | 可选值 | 版本 |
| :-------- | :--------------- | :---------------------- | :------ | :----- | :--- |
| `v-model` | 选中项的值       | `Array<String, Number>` | `[]`    | -      | -    |
| `async`   | 是否开启异步变更 | `Boolean`               | `false` | `true` | -    |
| `gap`     | 选项之间的间距   | `String`                | -       |


## Event

| 事件名        | 说明             | 回调参数                                   | 版本 |
| :------------ | :--------------- | :----------------------------------------- | :--- |
| `change`      | 选中的值更新事件 | values: 选中项的值                         | -    |
| `asyncChange` | 异步变更事件     | value: 选项的值, isChecked: 选项的选中状态 | -    |


## Slots

| 名称      | 说明                                                     | 参数 | 版本 |
| :-------- | :------------------------------------------------------- | :--- | :--- |
| `default` | 默认插槽，用于放置 `hi-checkbox`，但是也可以放置其他内容 | -    | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                          | 默认值 | 版本 |
| :------------------------------ | :----- | :--- |
| `--hi-checkbox-group-display`   | `flex` | -    |
| `--hi-checkbox-group-flex-wrap` | `wrap` | -    |
| `--hi-checkbox-group-gap`       | `10px` | -    |