---
titleTemplate: hi-ui - Cell - 单元格
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/cell
pageClass: demo-page
---

# Cell 单元格

此组件通常用于菜单列表。


## 基础使用

通过 `title` 属性设置单元格标题可以定义一个基础的单元格。

```vue
<hi-cell title="基础使用"></hi-cell>
```


## 左侧图标

通过 `leftIcon` 属性可以设置左侧图标。

```vue
<hi-cell title="左侧图标" leftIcon="__tishi"></hi-cell>
```


## 单元格提示

通过 `tips` 属性可以设置单元格提示。

```vue
<hi-cell title="显示提示" leftIcon="__menu" tips="我是单元格提示"></hi-cell>
```


## 右侧图标

通过 `rightIcon` 属性可以设置右侧图标。


```vue
<hi-cell title="显示描述" leftIcon="__gonggao" tips="我是单元格提示" rightIcon="__you"></hi-cell>
```


## 单元格描述

通过 `desc` 属性可以设置单元格描述。

```vue
<hi-cell title="显示描述" leftIcon="__gonggao" tips="我是单元格提示" rightIcon="__you" desc="单元格描述"></hi-cell>
```


## Props

| 属性名           | 说明               | 类型      | 默认值     | 可选值 | 版本 |
| :--------------- | :----------------- | :-------- | :--------- | :----- | :--- |
| `hover`          | 指定按下去的样式类 | `String`  | `hi-hover` | -      | -    |
| `title`          | 单元格标题         | `String`  | -          | -      | -    |
| `tips`           | 单元格提示         | `String`  | -          | -      | -    |
| `desc`           | 单元格描述         | `String`  | -          | -      | -    |
| `leftIcon`       | 左侧图标名称       | `String`  | -          | -      | -    |
| `rightIcon`      | 右侧图标名称       | `String`  | -          | -      | -    |
| `bg`             | 背景               | `String`  | -          | -      | -    |
| `radius`         | 圆角大小           | `String`  | -          | -      | -    |
| `padding`        | 内边距             | `String`  | -          | -      | -    |
| `color`          | 文字颜色           | `String`  | -          | -      | -    |
| `fontSize`       | 文字大小           | `String`  | -          | -      | -    |
| `titleColor`     | 标题颜色           | `String`  | -          | -      | -    |
| `titleFontSize`  | 标题大小           | `String`  | -          | -      | -    |
| `bold`           | 标题是否加粗       | `Boolean` | `false`    | `true` | -    |
| `leftIconColor`  | 左侧图标颜色       | `String`  | -          | -      | -    |
| `leftIconSize`   | 左侧图标大小       | `String`  | -          | -      | -    |
| `tipsColor`      | 提示颜色           | `String`  | -          | -      | -    |
| `tipsFontSize`   | 提示大小           | `String`  | -          | -      | -    |
| `descColor`      | 描述颜色           | `String`  | -          | -      | -    |
| `descFontSize`   | 描述大小           | `String`  | -          | -      | -    |
| `rightIconColor` | 右侧图标颜色       | `String`  | -          | -      | -    |
| `rightIconSize`  | 右侧图标大小       | `String`  | -          | -      | -    |
| `rotate`         | 右侧图标旋转角度   | `String`  | -          | -      | -    |


## Event

| 事件名  | 说明         | 回调参数 | 版本 |
| :------ | :----------- | :------- | :--- |
| `click` | 组件点击事件 | -        | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                           | 默认值                         | 版本 |
| :------------------------------- | :----------------------------- | :--- |
| `--hi-cell-background`           | `var(--hi-background-default)` | -    |
| `--hi-cell-border-radius`        | `5px`                          | -    |
| `--hi-cell-padding`              | `12px`                         | -    |
| `--hi-cell-color`                | `inherit`                      | -    |
| `--hi-cell-font-size`            | `inherit`                      | -    |
| `--hi-cell-display`              | `flex`                         | -    |
| `--hi-cell-align-items`          | `center`                       | -    |
| `--hi-cell-gap`                  | `8px`                          | -    |
| `--hi-cell-left-flex-shrink`     | `0`                            | -    |
| `--hi-cell-left-icon-color`      | `inherit`                      | -    |
| `--hi-cell-left-icon-font-size`  | `1.25em`                       | -    |
| `--hi-cell-body-flex`            | `1`                            | -    |
| `--hi-cell-body-display`         | `flex`                         | -    |
| `--hi-cell-body-align-items`     | `flex-start`                   | -    |
| `--hi-cell-body-flex-direction`  | `column`                       | -    |
| `--hi-cell-body-gap`             | `5px`                          | -    |
| `--hi-cell-content-display`      | `flex`                         | -    |
| `--hi-cell-content-align-items`  | `center`                       | -    |
| `--hi-cell-content-gap`          | `8px`                          | -    |
| `--hi-cell-title-color`          | `inherit`                      | -    |
| `--hi-cell-title-font-size`      | `inherit`                      | -    |
| `--hi-cell-title-font-weight`    | `inherit / 700`                | -    |
| `--hi-cell-title-flex`           | `1`                            | -    |
| `--hi-cell-tips-color`           | `inherit`                      | -    |
| `--hi-cell-tips-font-size`       | `0.85em`                       | -    |
| `--hi-cell-tips-opacity`         | `0.6`                          | -    |
| `--hi-cell-desc-color`           | `inherit`                      | -    |
| `--hi-cell-desc-font-size`       | `0.85em`                       | -    |
| `--hi-cell-desc-flex-shrink`     | `0`                            | -    |
| `--hi-cell-desc-opacity`         | `0.6`                          | -    |
| `--hi-cell-right-color`          | `inherit`                      | -    |
| `--hi-cell-right-font-size`      | `inherit`                      | -    |
| `--hi-cell-right-flex-shrink`    | `0`                            | -    |
| `--hi-cell-right-opacity`        | `0.6`                          | -    |
| `--hi-cell-right-icon-color`     | `inherit`                      | -    |
| `--hi-cell-right-icon-font-size` | `inherit`                      | -    |
| `--hi-cell-right-icon-rotate`    | `0deg`                         | -    |