---
titleTemplate: hi-ui - Price - 价格
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/price
pageClass: demo-page
---

# Price 价格

此组件通常用于商品价格展示。


## 基础使用

通过 `value` 属性设置显示的值。

```vue
<hi-price value="158.58"></hi-price>
```


## 符号

通过 `unit` 属性设置价格单位（符号），为空则不显示。

```vue
<hi-price value="158.58" unit="$"></hi-price>
```


## 小数位数

此组件默认会舍弃末尾为 `0` 的小数尾数位，可以通过 `fixed` 设置保留固定 `decimals` 位数的小数位数。`decimals` 默认为 2。

```vue
<hi-price value="158.58" fixed></hi-price>
<hi-price value="158.58" fixed :decimals="3"></hi-price>
```


## 千位符

通过 `thousands` 属性设置千位符，默认为英文逗号 `","`。

```vue
<hi-price value="158658" thousands="_"></hi-price>
```


## 删除线

通过 `line` 属性设置是否显示删除线。

```vue
<hi-price value="158.58" line></hi-price>
```

## 主题

通过 `theme` 属性设置文本颜色主题。

```vue
<hi-price value="158.58" theme="primary"></hi-price>
<hi-price value="158.58" theme="success"></hi-price>
<hi-price value="158.58" theme="warning"></hi-price>
<hi-price value="158.58" theme="error"></hi-price>
<hi-price value="158.58" theme="info"></hi-price>
```

## 对齐方式

文本默认靠底对齐，可通过 `align` 属性设置文本对齐方式，可选值为 `top`、`midlle`、`bottom`。

```vue
<hi-price value="158.58" align="top"></hi-price>
<hi-price value="158.58" align="middle"></hi-price>
<hi-price value="158.58" align="bottom"></hi-price>
```


## Props

| 属性名       | 说明                   | 类型               | 默认值   | 可选值          | 版本 |
| :----------- | :--------------------- | :----------------- | :------- | :-------------- | :--- |
| `unit`       | 价格单位（符号）       | `String`           | `¥`      | -               | -    |
| `value`      | 价格                   | `[String, Number]` | `0`      | -               | -    |
| `decimals`   | 小数位数               | `[String, Number]` | `2`      | -               | -    |
| `thousands`  | 千位符                 | `String`           | `,`      | -               | -    |
| `fixed`      | 是否显示固定的小数位数 | `Boolean`          | `false`  | `true`          | -    |
| `line`       | 是否显示删除线         | `Boolean`          | `false`  | `true`          | -    |
| `theme`      | 文本颜色主题           | `String`           | -        | -               | -    |
| `gap`        | 符号和价格之间的间距   | `String`           | -        | -               | -    |
| `color`      | 文本颜色               | `String`           | -        | -               | -    |
| `fontSize`   | 文本大小               | `String`           | -        | -               | -    |
| `fontWeight` | 文本粗细               | `String`           | -        | -               | -    |
| `unitSize`   | 符号大小               | `String`           | -        | -               | -    |
| `unitColor`  | 符号颜色               | `String`           | -        | -               | -    |
| `unitWeight` | 符号粗细               | `String`           | -        | -               | -    |
| `lineColor`  | 删除线颜色             | `String`           | -        | -               | -    |
| `lineHeight` | 删除线粗细             | `String`           | -        | -               | -    |
| `align`      | 对齐方式               | `String`           | `bottom` | `top`、`middle` | -    |


## Event

| 事件名  | 说明         | 回调参数 | 版本 |
| :------ | :----------- | :------- | :--- |
| `click` | 组件点击事件 | -        | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                             | 默认值                           | 版本 |
| :--------------------------------- | :------------------------------- | :--- |
| `--hi-price-display`               | `inline-block`                   | -    |
| `--hi-price-color`                 | `var(--hi-price-theme, inherit)` | -    |
| `--hi-price-font-size`             | `inherit`                        | -    |
| `--hi-price-font-weight`           | `700`                            | -    |
| `--hi-price-position`              | `relative`                       | -    |
| `--hi-price-line-height`           | `1`                              | -    |
| `--hi-price-vertical-align`        | `bottom`                         | -    |
| `--hi-price-unit-display`          | `inline-block`                   | -    |
| `--hi-price-unit-color`            | `inherit`                        | -    |
| `--hi-price-unit-font-size`        | `0.8em`                          | -    |
| `--hi-price-unit-font-weight`      | `inherit`                        | -    |
| `--hi-price-unit-margin`           | `0 2px 0 0`                      | -    |
| `--hi-price-value-display`         | `inline-block`                   | -    |
| `--hi-price-delete-line-height`    | `2px`                            | -    |
| `--hi-price-delete-line-color`     | `currentColor`                   | -    |
| `--hi-price-delete-line-left`      | `50%`                            | -    |
| `--hi-price-delete-line-top`       | `50%`                            | -    |
| `--hi-price-delete-line-transform` | `translate(-50%, -50%)`          | -    |
| `--hi-price-delete-line-opacity`   | `0 / 1`                          | -    |