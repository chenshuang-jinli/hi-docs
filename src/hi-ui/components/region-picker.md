---
titleTemplate: hi-ui - RegionPicker - 省市区选择器
layout: doc
aside: left
demoShow: true
demoLink: https://h5.hiui.jinanchenshuang.com/#/pages/components/region-picker
pageClass: demo-page
---

# RegionPicker 省市区选择器

此组件通常用于选择收货地址等场景。

> [!TIP]
> 由于每个项目对于省市区数据的要求不同，完整的省市区数据并不是每个项目都需要，所以该组件并没有内置完整的省市区数据。
> 如果需要完整的省市区数据可以在 [插件市场 - 省市区 `json` 数据](https://ext.dcloud.net.cn/plugin?name=hi-region-json) 上获取。


## 基础使用

* 通过 `show` 属性设置组件显示状态。
* 通过 `values` 属性设置选中项的下标。
* 通过 `region` 或 `provinces、cities、areas` 设置省市区数据。
* 通过 `@change` 或 `@confirm` 获取选中数据。

```vue
<template>
    <hi-region-picker :region="regionJson" :values="values" :show="show" @close="show = false" @cancel="show = false" @confirm="handleConfirm"></hi-region-picker>
    <hi-button text="显示省市区选择器" @click="show = true"></hi-button>
</template>

<script setup>
    import { ref } from "vue";
    import regionJson from "@/uni_modules/hi-region-json";

    // 显示状态
    const show = ref(false);

    // 选中项下标
    const values = ref([0, 0, 0]);

    // 选择器确认按钮点击事件
    function handleConfirm(indexes, data) {
        console.log(indexes, data);
        values.value = indexes;
        show.value = false;
    }
</script>
```


## 配置数据

### 整体配置

通过 `region` 配置省市区整体数据。

```vue
<template>
    <!-- 选择器 -->
    <hi-region-picker :region="regionJson" :values="values" :show="show" @close="show = false" @cancel="show = false" @confirm="handleConfirm"></hi-region-picker>

    <!-- 展示 -->
    <hi-cell :title="address" @click="show = true" rightIcon="__you"></hi-cell>
</template>

<script setup>
    import { ref } from "vue";
    import regionJson from "@/uni_modules/hi-region-json";

    // 显示状态
    const show = ref(false);

    // 选中项下标
    const values = ref([0, 0, 0]);

    // 选择的地址
    const address = ref("");

    // 选择器确认按钮点击事件
    function handleConfirm(indexes, data) {
        console.log(indexes, data);
        address.value = data?.province?.name + " " + data?.city?.name + " " + data?.area?.name;
        values.value = indexes;
        show.value = false;
    }
</script>
```


### 单独配置

* 通过 `provinces` 配置省份数据。
* 通过 `cities` 配置城市数据。
* 通过 `areas` 配置县区数据。
* 通过 `@changeProvince` 监听省份变化事件。
* 通过 `@changeCity` 监听城市变化事件。
* 通过 `@changeArea` 监听县区变化事件。
* 通过 `loading` 设置是否显示加载状态。

> [!TIP] 提示
> 此种方式一般用于省市区数据是通过接口动态获取的场景。

```vue
<template>
    <!-- 选择器 -->
    <hi-region-picker
        :provinces="provinces"
        :cities="cities"
        :areas="areas"
        :values="values"
        :show="show"
        :loading="loading"
        @close="show = false"
        @cancel="show = false"
        @changeProvince="onChangePrivice"
        @changeCity="onChangeCity"
        @confirm="handleConfirm"
    ></hi-region-picker>

    <!-- 展示 -->
    <hi-cell :title="address" @click="handleCellClick" rightIcon="__you"></hi-cell>
</template>

<script setup>
    import { ref } from "vue";

    // 省、市、区数据
    const provinces = ref([]);
    const cities = ref([]);
    const areas = ref([]);

    // 数据加载状态
    const loading = ref(false);

    // 显示状态
    const show = ref(false);

    // 选中项下标
    const values = ref([0, 0, 0]);

    // 选择的地址
    const address = ref("");

    // 选择器确认按钮点击事件
    function handleConfirm(indexes, data) {
        console.log(indexes, data);
        address.value = data?.province?.name + " " + data?.city?.name + " " + data?.area?.name;
        values.value = indexes;
        show.value = false;
    }

    // 单元格点击事件
    async function handleCellClick() {
        // 显示选择器
        show.value = true;

        // 判断是否有省份数据，没有的话就请求接口获取省份数据
        if(!provinces.value.length) {
            // 显示加载状态
            loading.value = true;

            // 请求接口获取省份数据
            // await getProvince(); // 此处为模拟请求

            // 此处模拟数据
            provinces.value = [{ name: "北京市" }, { name: "上海市" }, { name: "广东省" }];
        }

        // 获取数据后关闭加载状态
        loading.value = false;
    }

    // 省份变化事件
    async function onChangePrivice(index, data) {
        // 显示加载状态
        loading.value = true;

        // 查询对应的城市数据
        // await getCity(data); // 此处为模拟请求

        // 此处模拟数据
        cities.value = [{ name: "北京市" }, { name: "上海市" }, { name: "广州市" }];

        // 获取数据后关闭加载状态
        loading.value = false;
    }

    // 城市变化事件
    async function onChangeCity(index, data) {
        // 显示加载状态
        loading.value = true;

        // 查询对应的区数据
        // await getArea(data); // 此处为模拟请求

        // 此处模拟数据
        areas.value = [{ name: "通州区" }, { name: "闵行区" }, { name: "白云区" }];

        // 获取数据后关闭加载状态
        loading.value = false;
    }
</script>
```


## Props

| 属性名                | 说明                   | 类型      | 默认值      | 可选值                            | 版本 |
| :-------------------- | :--------------------- | :-------- | :---------- | :-------------------------------- | :--- |
| `show`                | 是否显示               | `Boolean` | `false`     | `true`                            | -    |
| `values`              | 选中项下标             | `Array`   | `[0, 0, 0]` | -                                 | -    |
| `region`              | 完整的省市区数据       | `Array`   | `[]`        | -                                 | -    |
| `provinces`           | 省份数据               | `Array`   | `[]`        | -                                 | -    |
| `cities`              | 城市数据               | `Array`   | `[]`        | -                                 | -    |
| `areas`               | 区数据                 | `Array`   | `[]`        | -                                 | -    |
| `keyName`             | 显示字段的属性名称     | `String`  | `name`      | -                                 | -    |
| `closeOnClickOverlay` | 点击蒙层时是否关闭     | `Boolean` | `true`      | `false`                           | -    |
| `loading`             | 是否显示加载状态       | `Boolean` | `false`     | `true`                            | -    |
| `returnZero`          | 切换后子项下标是否归零 | `Boolean` | `false`     | `true`                            | -    |
| `title`               | 标题                   | `String`  | -           | -                                 | -    |
| `cancelText`          | 取消按钮文本           | `String`  | `取消`      | -                                 | -    |
| `confirmText`         | 确认按钮文本           | `String`  | `确认`      | -                                 | -    |
| `titleColor`          | 标题颜色               | `String`  | -           | -                                 | -    |
| `titleFontSize`       | 标题字体大小           | `String`  | -           | -                                 | -    |
| `titleFontWeight`     | 标题字体粗细           | `String`  | -           | -                                 | -    |
| `cancelColor`         | 取消按钮颜色           | `String`  | -           | -                                 | -    |
| `cancelFontSize`      | 取消按钮字体大小       | `String`  | -           | -                                 | -    |
| `confirmColor`        | 确认按钮颜色           | `String`  | -           | -                                 | -    |
| `confirmFontSize`     | 确认按钮字体大小       | `String`  | -           | -                                 | -    |
| `itemHeight`          | 列表项高度             | `String`  | -           | -                                 | -    |
| `itemFontSize`        | 列表项字体大小         | `String`  | -           | -                                 | -    |
| `itemColor`           | 列表项字体颜色         | `String`  | -           | -                                 | -    |
| `bg`                  | 弹窗内容背景           | `String`  | -           | -                                 | -    |
| `radius`              | 弹窗内容圆角大小       | `String`  | -           | -                                 | -    |
| `height`              | 弹窗内容高度           | `String`  | -           | -                                 | -    |
| `maxHeight`           | 弹窗内容最大高度       | `String`  | -           | -                                 | -    |
| `mask`                | 是否显示蒙层           | `Boolean` | `true`      | `false`                           | -    |
| `maskBg`              | 蒙层背景               | `String`  | -           | -                                 | -    |
| `maskOpts`            | 蒙层配置               | `Object`  | `undefined` | [参考](/hi-ui/components/overlay) | -    |
| `loadingOpts`         | 加载配置               | `Object`  | `undefined` | [参考](/hi-ui/components/loading) | -    |


## Event

| 事件名           | 说明                   | 回调参数                                                 | 版本 |
| :--------------- | :--------------------- | :------------------------------------------------------- | :--- |
| `close`          | 点击遮罩触发的关闭事件 | -                                                        | -    |
| `cancel`         | 点击取消按钮触发的事件 | -                                                        | -    |
| `confirm`        | 点击确认按钮触发的事件 | `indexes`: 当前项的下标数据。`datas`: 当前项的省市区数据 | -    |
| `change`         | 切换事件               | `indexes`: 当前项的下标数据。`datas`: 当前项的省市区数据 | -    |
| `changeProvince` | 省份切换事件           | `index`: 省份当前项的下标。`province`: 当前省份项数据    | -    |
| `changeCity`     | 城市切换事件           | `index`: 城市当前项的下标。`city`: 当前城市项数据        | -    |
| `changeDistrict` | 区县切换事件           | `index`: 区县当前项的下标。`area`: 当前区项数据          | -    |
| `maskClick`      | 点击蒙层触发的事件     | -                                                        | -    |


## 样式变量

组件提供了以下样式变量，可以在使用时设置这些变量来修改组件的样式。

| 变量名                                         | 默认值                                            | 版本 |
| :--------------------------------------------- | :------------------------------------------------ | :--- |
| `--hi-region-picker-background`                | `var(--hi-background-element)`                    | -    |
| `--hi-region-picker-border-radius`             | `8px`                                             | -    |
| `--hi-region-picker-max-height`                | `80%`                                             | -    |
| `--hi-region-picker-height`                    | `50%`                                             | -    |
| `--hi-region-picker-width`                     | `100%`                                            | -    |
| `--hi-region-picker-display`                   | `flex`                                            | -    |
| `--hi-region-picker-flex-direction`            | `column`                                          | -    |
| `--hi-region-picker-padding`                   | `0 20px`                                          | -    |
| `--hi-region-picker-position`                  | `fixed`                                           | -    |
| `--hi-region-picker-left`                      | `0`                                               | -    |
| `--hi-region-picker-bottom`                    | `0`                                               | -    |
| `--hi-region-picker-z-index`                   | `var(--hi-index-upper)`                           | -    |
| `--hi-region-picker-animation-duration`        | `300ms`                                           | -    |
| `--hi-region-picker-animation-fill-mode`       | `forwards`                                        | -    |
| `--hi-region-picker-animation-timing-function` | `linear`                                          | -    |
| `--hi-region-picker-header-display`            | `flex`                                            | -    |
| `--hi-region-picker-header-align-items`        | `center`                                          | -    |
| `--hi-region-picker-header-flex-shrink`        | `0`                                               | -    |
| `--hi-region-picker-header-margin`             | `0 0 30px 0`                                      | -    |
| `--hi-region-picker-header-padding`            | `30rpx 0`                                         | -    |
| `--hi-region-picker-button-flex-shrink`        | `0`                                               | -    |
| `--hi-region-picker-button-font-weight`        | `500`                                             | -    |
| `--hi-region-picker-button-flex`               | `1`                                               | -    |
| `--hi-region-picker-button-display`            | `flex`                                            | -    |
| `--hi-region-picker-button-flex-direction`     | `row`                                             | -    |
| `--hi-region-picker-cancel-font-size`          | `inherit`                                         | -    |
| `--hi-region-picker-cancel-color`              | `var(--hi-color-middle)`                          | -    |
| `--hi-region-picker-cancel-font-weight`        | `var(--hi-region-picker-button-font-weight, 500)` | -    |
| `--hi-region-picker-cancel-justify-content`    | `flex-start`                                      | -    |
| `--hi-region-picker-confirm-font-size`         | `inherit`                                         | -    |
| `--hi-region-picker-confirm-color`             | `var(--hi-theme-primary)`                         | -    |
| `--hi-region-picker-confirm-font-weight`       | `var(--hi-region-picker-button-font-weight, 500)` | -    |
| `--hi-region-picker-confirm-justify-content`   | `flex-end`                                        | -    |
| `--hi-region-picker-title-flex`                | `2`                                               | -    |
| `--hi-region-picker-title-font-weight`         | `700`                                             | -    |
| `--hi-region-picker-title-color`               | `inherit`                                         | -    |
| `--hi-region-picker-title-font-size`           | `1.25em`                                          | -    |
| `--hi-region-picker-title-text-align`          | `center`                                          | -    |
| `--hi-region-picker-title-flex-shrink`         | `0`                                               | -    |
| `--hi-region-picker-picker-view-width`         | `100%`                                            | -    |
| `--hi-region-picker-picker-view-flex`          | `1`                                               | -    |
| `--hi-region-picker-item-height`               | `3.5em`                                           | -    |
| `--hi-region-picker-item-display`              | `flex`                                            | -    |
| `--hi-region-picker-item-align-items`          | `center`                                          | -    |
| `--hi-region-picker-item-justify-content`      | `center`                                          | -    |
| `--hi-region-picker-item-color`                | `inherit`                                         | -    |
| `--hi-region-picker-item-font-size`            | `inherit`                                         | -    |
| `--hi-region-picker-item-font-weight`          | `500`                                             | -    |
| `--hi-region-picker-loading-position`          | `absolute`                                        | -    |
| `--hi-region-picker-loading-width`             | `100% `                                           | -    |
| `--hi-region-picker-loading-height`            | `100%`                                            | -    |
| `--hi-region-picker-loading-left`              | `0`                                               | -    |
| `--hi-region-picker-loading-top`               | `0`                                               | -    |
| `--hi-region-picker-loading-right`             | `0`                                               | -    |
| `--hi-region-picker-loading-bottom`            | `0`                                               | -    |
| `--hi-region-picker-loading-display`           | `flex`                                            | -    |
| `--hi-region-picker-loading-flex-direction`    | `column`                                          | -    |
| `--hi-region-picker-loading-align-items`       | `center`                                          | -    |
| `--hi-region-picker-loading-justify-content`   | `center`                                          | -    |
| `--hi-region-picker-loading-background`        | `var(--hi-background-element)`                    | -    |
| `--hi-region-picker-loading-z-index`           | `6`                                               | -    |
| `--hi-region-picker-loading-font-size`         | `1.5em`                                           | -    |
| `--hi-region-picker-loading-opacity`           | `0.8`                                             | -    |