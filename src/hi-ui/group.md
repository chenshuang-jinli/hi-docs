---
titleTemplate: hi-ui - 加群交流反馈
layout: doc
---

# 加群交流反馈

## QQ群

<Qrcodes :list="list" />

<script setup>
    import { ref } from 'vue';
    import Qrcodes from '../components/Qrcodes.vue';

    // 列表
    const list = ref([
        { text: "1️⃣群", src: "/hi-ui/qrcode/qq-group-01.png" }
    ]);
</script>