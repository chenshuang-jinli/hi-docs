---
titleTemplate: hi-ui - 效果演示
layout: doc
---

# 扫码预览

<br/>

<Qrcodes :list="list" />

<script setup>
    import { ref } from 'vue';
    import Qrcodes from '../components/Qrcodes.vue';

    // 列表
    const list = ref([
        { text: "H5", src: "/hi-ui/qrcode/h5.png" }
    ]);
</script>