---
titleTemplate: hi-ui - 安装配置
layout: doc
---

# 安装配置

安装配置遇到问题？欢迎加群交流。

## 安装

前往 [插件市场](https://ext.dcloud.net.cn/plugin?name=hi-ui) 下载并导入插件。

> [!TIP] 提示
> 当前只支持通过插件市场安装。


## 配置

### 配置 main.js

```js
// 引入 hi-ui
import "@/uni_modules/hi-ui/index";

// 配置扩展图标，如果需要的话，具体如何配置请查看图标组件说明
uni.$hi.config.icon.prefix = ["app-iconfont"];
```

### 配置 App.vue

```vue
<style lang="scss">
    /* 引入 hi-ui  */
    @import "@/uni_modules/hi-ui/index.scss";

    /* 引入扩展图标文件，如果需要的话，具体如何配置请查看图标组件说明 */
    @import "@/styles/app-iconfont.css";
</style>
```

### 配置 manifest.json

为了方便使用，建议开启下面的配置。

```json
/* 微信小程序 */
"mp-weixin": {
    // 合并组件虚拟节点外层属性（目前仅支持 style、class 属性），uni-app 3.5.1+ 开始支持
    "mergeVirtualHostAttributes": true
},

/* 支付宝小程序 */
"mp-alipay": {
    // 合并组件虚拟节点外层属性（目前仅支持 style、class 属性），uni-app 3.5.1+ 开始支持
    "mergeVirtualHostAttributes": true,
    // 组件样式隔离方式，具体配置选项参见：[组件样式隔离](https://developers.weixin.qq.com/miniprogram/dev/framework/custom-component/wxml-wxss.html#%E7%BB%84%E4%BB%B6%E6%A0%B7%E5%BC%8F%E9%9A%94%E7%A6%BB)
    // - isolated 表示启用样式隔离，在自定义组件内外，使用 class 指定的样式将不会相互影响（一般情况下的默认值）；
    // - apply-shared 表示页面 wxss 样式将影响到自定义组件，但自定义组件 wxss 中指定的样式不会影响页面；
    // - shared 表示页面 wxss 样式将影响到自定义组件，自定义组件 wxss 中指定的样式也会影响页面和其他设置了 apply-shared 或 shared 的自定义组件。（这个选项在插件中不可用。）
    // 微信小程序在组件 options 中配置，支付宝小程序在 manifest.json -> mp-alipay 中配置
    "styleIsolation": "apply-shared"
},

/* 头条小程序 */
"mp-toutiao": {
    // 合并组件虚拟节点外层属性（目前仅支持 style、class 属性），uni-app 3.5.1+ 开始支持
    "mergeVirtualHostAttributes": true
},
```

## 配置完毕

至此，配置完毕，你就可以使用了，组件使用方式如下：

```vue
<template>
    <!-- 图标组件 -->
    <hi-icon name="wechat"></hi-icon>

    <!-- 按钮组件 -->
    <hi-button text="查看详情" theme="primary"></hi-button>
</template>
```