---
titleTemplate: hi-ui - 工具函数 - Validate
layout: doc
outline: [1, 2]
---

# 工具函数 Validate

`Validate` 验证工具函数集。


## 函数列表

| 函数名                                            | 说明                               | 版本 |
| :------------------------------------------------ | :--------------------------------- | :--- |
| [isString](#isString)                             | 验证数据是否是 `String` 类型数据   | -    |
| [isNumber](#isNumber)                             | 验证数据是否是 `Number` 类型数据   | -    |
| [isArray](#isArray)                               | 验证数据是否是 `Array` 类型数据    | -    |
| [isObject](#isObject)                             | 验证数据是否是 `Object` 类型数据   | -    |
| [isDate](#isDate)                                 | 验证数据是否是 `Date` 类型数据     | -    |
| [isFunction](#isFunction)                         | 验证数据是否是 `Function` 类型数据 | -    |
| [isSecondTimestamp](#isSecondTimestamp)           | 验证数据是否是秒级时间戳           | -    |
| [isMillisecondTimestamp](#isMillisecondTimestamp) | 验证数据是否是毫秒级时间戳         | -    |
| [isAbsoluteURL](#isAbsoluteURL)                   | 校验 `URL` 是否是绝对 `URL`        | -    |


## isString {#isString}

验证数据是否是 `String` 类型数据。

### 参数

| 参数       | 说明         | 类型  | 默认值 | 版本 |
| :--------- | :----------- | :---- | :----- | :--- |
| `variable` | 要验证的数据 | `Any` | -      | -    |

### 返回值

`true`: 是 `String` 类型数据；`false`: 不是 `String` 类型数据；


<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------->


## isNumber {#isNumber}

验证数据是否是 `Number` 类型数据。

### 参数

| 参数       | 说明         | 类型  | 默认值 | 版本 |
| :--------- | :----------- | :---- | :----- | :--- |
| `variable` | 要验证的数据 | `Any` | -      | -    |

### 返回值

`true`: 是 `Number` 类型数据；`false`: 不是 `Number` 类型数据；


<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------->


## isArray {#isArray}

验证数据是否是 `Array` 类型数据。

### 参数

| 参数       | 说明         | 类型  | 默认值 | 版本 |
| :--------- | :----------- | :---- | :----- | :--- |
| `variable` | 要验证的数据 | `Any` | -      | -    |

### 返回值

`true`: 是 `Array` 类型数据；`false`: 不是 `Array` 类型数据；


<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------->


## isObject {#isObject}

验证数据是否是 `Object` 类型数据。

> [!TIP] 提示
> 不包括数组、函数、`null` 等

### 参数

| 参数       | 说明         | 类型  | 默认值 | 版本 |
| :--------- | :----------- | :---- | :----- | :--- |
| `variable` | 要验证的数据 | `Any` | -      | -    |

### 返回值

`true`: 是 `Object` 类型数据；`false`: 不是 `Object` 类型数据；


<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------->


## isDate {#isDate}

验证数据是否是 `Date` 类型数据。

### 参数

| 参数       | 说明         | 类型  | 默认值 | 版本 |
| :--------- | :----------- | :---- | :----- | :--- |
| `variable` | 要验证的数据 | `Any` | -      | -    |

### 返回值

`true`: 是 `Date` 类型数据；`false`: 不是 `Date` 类型数据；


<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------->


## isFunction {#isFunction}

验证数据是否是 `Function` 类型数据。

### 参数

| 参数       | 说明         | 类型  | 默认值 | 版本 |
| :--------- | :----------- | :---- | :----- | :--- |
| `variable` | 要验证的数据 | `Any` | -      | -    |

### 返回值

`true`: 是 `Function` 类型数据；`false`: 不是 `Function` 类型数据；


<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------->


## isSecondTimestamp {#isSecondTimestamp}

判断时间戳是否是 “秒” 级时间戳。

> [!TIP] 提示
> 通过判断时间戳的长度（10位）来判断的

### 参数

| 参数        | 说明         | 类型               | 默认值 | 版本 |
| :---------- | :----------- | :----------------- | :----- | :--- |
| `timestamp` | 要验证的数据 | `[String, Number]` | -      | -    |

### 返回值

`true`: 是秒时间戳；`false`: 不是秒时间戳；


<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------->


## isMillisecondTimestamp {#isMillisecondTimestamp}

判断时间戳是否是 “毫秒” 级时间戳。

> [!TIP] 提示
> 通过判断时间戳的长度（13位）来判断的

### 参数

| 参数        | 说明         | 类型               | 默认值 | 版本 |
| :---------- | :----------- | :----------------- | :----- | :--- |
| `timestamp` | 要验证的数据 | `[String, Number]` | -      | -    |

### 返回值

`true`: 是毫秒时间戳；`false`: 不是毫秒时间戳；


<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------->


## isAbsoluteURL {#isAbsoluteURL}

校验 `URL` 是否是绝对 `URL`。

> [!TIP] 提示
> 如果 URL 以 `“<scheme>://”` 或 `“//”`（协议相对URL）开头，则认为它是绝对的，`RFC 3986` 将方案名称定义为以字母开头的字符序列，然后是字母，数字，加号，句点或连字符的任意组合

### 参数

| 参数  | 说明             | 类型     | 默认值 | 版本 |
| :---- | :--------------- | :------- | :----- | :--- |
| `url` | 需要校验的 `URL` | `String` | `""`   | -    |

### 返回值

`true`: 是绝对 `URL`；`false`: 不是绝对 `URL`；


<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------->