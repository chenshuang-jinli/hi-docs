---
titleTemplate: hi-ui - 工具函数 - URL
layout: doc
outline: [1, 2]
---

# 工具函数 String

`URL` 处理工具函数集。


## 函数列表

| 函数名                                              | 说明                                           | 版本 |
| :-------------------------------------------------- | :--------------------------------------------- | :--- |
| [urlCombine](#urlCombine)                           | 合并基准 `URL` 和相对 `URL` 成一个完整的 `URL` | -    |
| [urlGetMainPart](#urlGetMainPart)                   | 获取 `URL` 问号之前的部分                      | -    |
| [urlGetParamsObject](#urlGetParamsObject)           | 将 `URL` 字符串中的参数解析为对象              | -    |
| [urlParamsObjectToString](#urlParamsObjectToString) | 将对象转换为 `URL` 参数字符串                  | -    |
| [urlAppendParams](#urlAppendParams)                 | 将参数追加到 `URL` 中                          | -    |


## urlCombine {#urlCombine}

合并基准 `URL` 和相对 `URL` 成一个完整的 `URL`。

### 参数

| 参数          | 说明       | 类型     | 默认值 | 版本 |
| :------------ | :--------- | :------- | :----- | :--- |
| `baseURL`     | 基准 `URL` | `String` | `""`   | -    |
| `relativeURL` | 相对 `URL` | `String` | `""`   | -    |

### 返回值

返回合并后的完整 `URL`。


<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------->


## urlGetMainPart {#urlGetMainPart}

获取 `URL` 问号之前的部分。

### 参数

| 参数  | 说明         | 类型     | 默认值 | 版本 |
| :---- | :----------- | :------- | :----- | :--- |
| `url` | `URL` 字符串 | `String` | `""`   | -    |

### 返回值

返回 `URL` 主要部分（问号之前的部分）字符串。


<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------->


## urlGetParamsObject {#urlGetParamsObject}

将 `URL` 字符串中的参数解析为对象。

### 参数

| 参数  | 说明         | 类型     | 默认值 | 版本 |
| :---- | :----------- | :------- | :----- | :--- |
| `url` | `URL` 字符串 | `String` | `""`   | -    |

### 返回值

返回解析后的参数对象。


<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------->


## urlParamsObjectToString {#urlParamsObjectToString}

将对象转换为 `URL` 参数字符串。

### 参数

| 参数        | 说明         | 类型      | 默认值  | 版本 |
| :---------- | :----------- | :-------- | :------ | :--- |
| `obj`       | 参数对象     | `Object`  | -       | -    |
| `separator` | 是否拼接问号 | `Boolean` | `false` | -    |

### 返回值

返回参数字符串。


<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------->


## urlAppendParams {#urlAppendParams}

将参数追加到 `URL` 中。

### 参数

| 参数     | 说明                 | 类型               | 默认值 | 版本 |
| :------- | :------------------- | :----------------- | :----- | :--- |
| `url`    | `URL` 字符串         | `String`           | `""`   | -    |
| `params` | 参数对象或参数字符串 | `[Object, String]` | -      | -    |

### 返回值

返回追加参数后的 `URL`。


<!-------------------------------------------------------------------------------------------------------------------------------------------------------------------->

