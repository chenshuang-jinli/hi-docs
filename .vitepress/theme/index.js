import { h } from "vue";
import DefaultTheme from "vitepress/theme";

// 自定义样式
import "./style.css";

// 文档底部插槽
import DocBottom from "./DocBottom.vue";

// 主题
export default {
    extends: DefaultTheme,
    Layout: () => {
        return h(DefaultTheme.Layout, null, {
            // 文档底部插槽
            "doc-bottom": () => h(DocBottom),
        });
    },
    enhanceApp({ app, router, siteData }) {},
};
