/**
 * 站点配置
 */
import { defineConfig } from "vitepress";
import nav from "./nav";
import sidebar from "./sidebar";
import socialLinks from "./social-links";
import { chineseSearchOptimize, pagefindPlugin } from "vitepress-plugin-pagefind";

// 站点配置可以定义站点的全局设置。
export default defineConfig({
    // 站点的标题。使用默认主题时，这将显示在导航栏中。
    title: "Hi Docs",

    // 站点的描述。这将呈现为页面 HTML 中的 <meta> 标签。
    description: "晨霜开源项目文档",

    // 要在页面 HTML 的 <head> 标签中呈现的其他元素。用户添加的标签在结束 head 标签之前呈现，在 VitePress 标签之后。
    head: [
        // 站点图标
        ["link", { rel: "icon", href: "/favicon.ico" }],
    ],

    // 站点的 lang 属性
    lang: "zh-cn",

    // 站点将部署到的 base URL。
    base: "/",

    // 相对于项目根目录的 markdown 文件所在的文件夹。
    srcDir: "./src",

    // 是否启用深色模式
    appearance: true,

    // 是否使用 Git 获取每个页面的最后更新时间戳。时间戳将包含在每个页面的页面数据中，可通过 useData 访问。
    lastUpdated: true,

    // 当设置为 true 时，VitePress 不会因为死链而导致构建失败。
    ignoreDeadLinks: true,

    // 主题相关配置
    themeConfig: {
        // 导航栏上显示的 Logo，位于站点标题前。可以接受一个路径字符串，或者一个对象来设置在浅色/深色模式下不同的 Logo。
        logo: "/logo.png",

        // 可以自定义此项以替换导航中的默认站点标题 (应用配置中的 title)。当设置为 false 时，导航中的标题将被禁用。这在当 logo 已经包含站点标题文本时很有用。
        siteTitle: false,

        // 导航菜单项的配置
        nav,

        // 侧边栏菜单项的配置
        sidebar,

        // 定义侧边栏组件在 doc 布局中的位置
        aside: true,

        // 大纲中显示的标题级别
        outline: {
            // outline 中要显示的标题级别。
            // 单个数字表示只显示该级别的标题。
            // 如果传递的是一个元组，第一个数字是最小级别，第二个数字是最大级别。
            // `'deep'` 与 `[2, 6]` 相同，将显示从 `<h2>` 到 `<h6>` 的所有标题。
            level: [2, 3],

            // 显示在 outline 上的标题。
            label: "页面导航",
        },

        // 可以定义此选项以在导航栏中展示带有图标的社交帐户链接
        socialLinks,

        // 页脚配置。可以添加 message 和 copyright。由于设计原因，仅当页面不包含侧边栏时才会显示页脚。
        footer: {
            message: "基于 MIT 许可发布",
            copyright: "版权所有 © 2024-2029 济南晨霜信息技术有限公司",
        },

        // 编辑链接可让显示链接以编辑 Git 管理服务 (例如 GitHub 或 GitLab) 上的页面
        editLink: {
            pattern: "https://gitee.com/chenshuang-jinli/hi-docs/tree/master/src/:path",
            text: "在 Gitee 上编辑此页面",
        },

        // 自定义上次更新的文本和日期格式
        lastUpdated: {
            text: "上次更新时间",
            formatOptions: {
                dateStyle: "full",
                timeStyle: "medium",
            },
        },

        // 自定义出现在上一页和下一页链接上方的文本
        docFooter: {
            prev: "上一页",
            next: "下一页",
        },

        // 用于自定义深色模式开关标签，该标签仅在移动端视图中显示
        darkModeSwitchLabel: "深色模式",

        // 用于自定义悬停时显示的浅色模式开关标题
        lightModeSwitchTitle: "切换到浅色模式",

        // 用于自定义悬停时显示的深色模式开关标题
        darkModeSwitchTitle: "切换到深色模式",

        // 用于自定义侧边栏菜单标签，该标签仅在移动端视图中显示
        sidebarMenuLabel: "导航",

        // 用于自定义返回顶部按钮的标签，该标签仅在移动端视图中显示
        returnToTopLabel: "返回顶部",

        // 是否在 markdown 中的外部链接旁显示外部链接图标
        externalLinkIcon: false,
    },

    // vite 配置
    vite: {
        // 开发服务器选项
        server: {
            // 指定开发服务器端口
            port: 10001,
        },

        // 使用到的插件
        plugins: [
            // 搜索插件
            pagefindPlugin({
                // 中文搜索优化
                customSearchQuery: chineseSearchOptimize,
                btnPlaceholder: "搜索",
                placeholder: "搜索文档",
                emptyText: "空空如也",
                heading: "共: {{searchResult}} 条结果",
                toSelect: "选择",
                toNavigate: "切换",
                toClose: "关闭",
                searchBy: "搜索提供者",
            }),
        ],
    },
});
