/**
 * 侧边栏菜单项的配置
 */
export default {
    // hi-ui
    "/hi-ui/": [
        {
            text: "hi-ui",
            collapsed: false,
            items: [
                { text: "效果演示", link: "/hi-ui/demo" },
                { text: "介绍", link: "/hi-ui/intro" },
                { text: "安装配置", link: "/hi-ui/install" },
                { text: "样式指南", link: "/hi-ui/styles" },
                { text: "工具函数", link: "/hi-ui/functions" },
                { text: "注意事项", link: "/hi-ui/note" },
                { text: "加群交流反馈", link: "/hi-ui/group" },
            ],
        },
        {
            text: "组件",
            collapsed: false,
            items: [
                {
                    text: "基础组件",
                    collapsed: false,
                    items: [
                        { text: "Icon 图标", link: "/hi-ui/components/icon" },
                        { text: "Button 按钮", link: "/hi-ui/components/button" },
                        { text: "Image 图片", link: "/hi-ui/components/image" },
                        { text: "Badge 徽标数", link: "/hi-ui/components/badge" },
                        { text: "Tag 标签", link: "/hi-ui/components/tag" },
                        { text: "Loading 加载", link: "/hi-ui/components/loading" },
                        { text: "LoadMore 加载更多", link: "/hi-ui/components/loadmore" },
                    ],
                },
                {
                    text: "反馈组件",
                    collapsed: false,
                    items: [
                        { text: "NoticeBar 滚动通知", link: "/hi-ui/components/notice-bar" },
                        { text: "Popup 弹出层", link: "/hi-ui/components/popup" },
                    ],
                },
                {
                    text: "布局组件",
                    collapsed: false,
                    items: [
                        { text: "Swiper 轮播", link: "/hi-ui/components/swiper" },
                        { text: "SwiperIndicator 轮播指示器", link: "/hi-ui/components/swiper-indicator" },
                        { text: "MovableButton 可拖动按钮", link: "/hi-ui/components/movable-button" },
                        { text: "Overlay 遮罩层", link: "/hi-ui/components/overlay" },
                        { text: "List 列表", link: "/hi-ui/components/list" },
                        { text: "Cell 单元格", link: "/hi-ui/components/cell" },
                        { text: "Grid 宫格", link: "/hi-ui/components/grid" },
                        { text: "Gap 间隔", link: "/hi-ui/components/gap" },
                    ],
                },
                {
                    text: "表单组件",
                    collapsed: false,
                    items: [
                        { text: "Checkbox 复选框", link: "/hi-ui/components/checkbox" },
                        { text: "CheckboxGroup 复选框组", link: "/hi-ui/components/checkbox-group" },
                        { text: "Radio 单选框", link: "/hi-ui/components/radio" },
                        { text: "RadioGroup 单选框组", link: "/hi-ui/components/radio-group" },
                        { text: "NumberStep 步进器", link: "/hi-ui/components/number-step" },
                        { text: "RegionPicker 省市区选择器", link: "/hi-ui/components/region-picker" },
                    ],
                },
                {
                    text: "导航组件",
                    collapsed: false,
                    items: [
                        { text: "StatusBar 状态栏", link: "/hi-ui/components/status-bar" },
                        { text: "NavigationBar 导航栏", link: "/hi-ui/components/navigation-bar" },
                        { text: "Empty 内容为空", link: "/hi-ui/components/empty" },
                        { text: "Tabs 选项卡", link: "/hi-ui/components/tabs" },
                    ],
                },
                {
                    text: "展示组件",
                    collapsed: false,
                    items: [
                        { text: "Price 价格", link: "/hi-ui/components/price" },
                        { text: "FilterTabs 过滤器选项卡", link: "/hi-ui/components/filter-tabs" },
                        { text: "Countdown 倒计时", link: "/hi-ui/components/countdown" },
                        { text: "AvatarGroup 头像组", link: "/hi-ui/components/avatar-group" },
                    ],
                },
            ],
        },
        {
            text: "工具函数",
            collapsed: false,
            items: [
                { text: "Common 通用", link: "/hi-ui/functions/common" },
                { text: "Array 数组", link: "/hi-ui/functions/array" },
                { text: "Date 日期", link: "/hi-ui/functions/date" },
                { text: "Function 函数", link: "/hi-ui/functions/function" },
                { text: "Image 图片", link: "/hi-ui/functions/image" },
                { text: "Number 数字", link: "/hi-ui/functions/number" },
                { text: "Object 对象", link: "/hi-ui/functions/object" },
                { text: "RichText 富文本", link: "/hi-ui/functions/richtext" },
                { text: "String 字符串", link: "/hi-ui/functions/string" },
                { text: "URL 路径", link: "/hi-ui/functions/url" },
                { text: "Validate 验证", link: "/hi-ui/functions/validate" },
                { text: "Uniapp 便捷化封装", link: "/hi-ui/functions/uniapp" },
            ],
        },
    ],

    // hi-ui pro
    "/hi-ui-pro/": [],

    // hi-http
    "/hi-http/": [],

    // hi-router
    "/hi-router/": [],
};
