import{Q as n}from"./chunks/Qrcodes.6IZJIQ7m.js";import{p as i,o as p,c as e,a3 as l,G as t}from"./chunks/framework.Cc18ViN9.js";const k=JSON.parse('{"title":"hi-uniapp-starter 项目基座","titleTemplate":"hi-uniapp-starter","description":"","frontmatter":{"titleTemplate":"hi-uniapp-starter","layout":"doc","aside":"left"},"headers":[],"relativePath":"hi-uniapp-starter/index.md","filePath":"hi-uniapp-starter/index.md","lastUpdated":null}'),h={name:"hi-uniapp-starter/index.md"},g=Object.assign(h,{setup(o){const a=i([{text:"1️⃣群",src:"/hi-uniapp-starter/qrcode/qq-group-01.png"}]);return(c,s)=>(p(),e("div",{"data-pagefind-body":!0},[s[0]||(s[0]=l(`<h1 id="hi-uniapp-starter-项目基座" tabindex="-1">hi-uniapp-starter 项目基座 <a class="header-anchor" href="#hi-uniapp-starter-项目基座" aria-label="Permalink to &quot;hi-uniapp-starter 项目基座&quot;">​</a></h1><div class="info custom-block"><p class="custom-block-title">简介</p><p>一个 <code>vue3 + pinia + hi-ui</code> 的基础项目底座。</p></div><h1 id="开始" tabindex="-1">开始 <a class="header-anchor" href="#开始" aria-label="Permalink to &quot;开始&quot;">​</a></h1><p>前往 <code>uni-app</code> 插件市场下载 <code>hi-uniapp-starter</code> 项目，<a href="https://ext.dcloud.net.cn/plugin?name=hi-uniapp-starter" target="_blank" rel="noreferrer">插件地址</a>。</p><div class="tip custom-block"><p class="custom-block-title">提示</p><p>由于 <code>uni-app</code> 对模板项目需要审核，所以一般插件市场上的版本不是最新的，可以前往仓库下载最新版本 <a href="https://gitee.com/chenshuang-jinli/hi-uniapp-starter" target="_blank" rel="noreferrer">仓库地址</a>。</p></div><h1 id="安装依赖" tabindex="-1">安装依赖 <a class="header-anchor" href="#安装依赖" aria-label="Permalink to &quot;安装依赖&quot;">​</a></h1><div class="language-bash vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">bash</span><pre class="shiki shiki-themes github-light github-dark vp-code" tabindex="0"><code><span class="line"><span style="--shiki-light:#6F42C1;--shiki-dark:#B392F0;">npm</span><span style="--shiki-light:#032F62;--shiki-dark:#9ECBFF;"> install</span></span></code></pre></div><h1 id="配置" tabindex="-1">配置 <a class="header-anchor" href="#配置" aria-label="Permalink to &quot;配置&quot;">​</a></h1><h2 id="配置-main-js" tabindex="-1">配置 <code>main.js</code> <a class="header-anchor" href="#配置-main-js" aria-label="Permalink to &quot;配置 \`main.js\`&quot;">​</a></h2><p>一般在 <code>main.js</code> 中只需配置一下扩展图标。</p><div class="language-js vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">js</span><pre class="shiki shiki-themes github-light github-dark vp-code" tabindex="0"><code><span class="line"><span style="--shiki-light:#6A737D;--shiki-dark:#6A737D;">// ...</span></span>
<span class="line"></span>
<span class="line"><span style="--shiki-light:#6A737D;--shiki-dark:#6A737D;">// 配置 hi-ui 扩展图标</span></span>
<span class="line"><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">uni.$hi.config.icon.prefix </span><span style="--shiki-light:#D73A49;--shiki-dark:#F97583;">=</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;"> [</span><span style="--shiki-light:#032F62;--shiki-dark:#9ECBFF;">&quot;app-iconfont&quot;</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">];</span></span>
<span class="line"></span>
<span class="line"><span style="--shiki-light:#6A737D;--shiki-dark:#6A737D;">// ...</span></span></code></pre></div><h2 id="扩展图标文件" tabindex="-1">扩展图标文件 <a class="header-anchor" href="#扩展图标文件" aria-label="Permalink to &quot;扩展图标文件&quot;">​</a></h2><p>扩展图标 <code>css</code> 文件放置在了 <code>@/styles/iconfont.css</code>，需将此文件替换为您当前项目用到的图标文件。</p><h2 id="项目配置文件" tabindex="-1">项目配置文件 <a class="header-anchor" href="#项目配置文件" aria-label="Permalink to &quot;项目配置文件&quot;">​</a></h2><p>修改 <code>@/settings/index.js</code> 中配置的项目默认配置。</p><div class="tip custom-block github-alert"><p class="custom-block-title">提示</p><p>此项目并没有使用 <code>.env.development</code> 和 <code>.env.production</code> 文件，只是个人习惯而已。</p></div><h2 id="配置-pages-json" tabindex="-1">配置 <code>pages.json</code> <a class="header-anchor" href="#配置-pages-json" aria-label="Permalink to &quot;配置 \`pages.json\`&quot;">​</a></h2><p>在 <code>pages.json</code> 中除了常规的配置，还支持以下页面 <code>meta</code> 配置项：</p><table tabindex="0"><thead><tr><th style="text-align:left;">名称</th><th style="text-align:left;">说明</th><th style="text-align:left;">类型</th><th style="text-align:left;">版本</th></tr></thead><tbody><tr><td style="text-align:left;"><code>needLogin</code></td><td style="text-align:left;">当前页面是否需要登录才能访问</td><td style="text-align:left;"><code>Boolean</code></td><td style="text-align:left;">-</td></tr><tr><td style="text-align:left;"><code>isLogin</code></td><td style="text-align:left;">是否是登录页（项目中处理跳转到登录页的逻辑时，如果没有特殊配置，会默认跳转到此字段为 <code>true</code> 的第一个页面路径）</td><td style="text-align:left;"><code>Boolean</code></td><td style="text-align:left;">-</td></tr><tr><td style="text-align:left;">isHome</td><td style="text-align:left;">是否是首页（项目中处理跳转到首页的逻辑时，如果没有特殊配置，会默认跳转到此字段为 <code>true</code> 的第一个页面路径）</td><td style="text-align:left;"><code>Boolean</code></td><td style="text-align:left;">-</td></tr></tbody></table><h2 id="配置开发服务器" tabindex="-1">配置开发服务器 <a class="header-anchor" href="#配置开发服务器" aria-label="Permalink to &quot;配置开发服务器&quot;">​</a></h2><p>如果您的项目是 <code>H5</code> 类型，建议在 <code>manifest.json</code> 中配置开发服务器。</p><div class="language-json vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang">json</span><pre class="shiki shiki-themes github-light github-dark vp-code" tabindex="0"><code><span class="line"><span style="--shiki-light:#032F62;--shiki-dark:#9ECBFF;">&quot;devServer&quot;</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">: {</span></span>
<span class="line"><span style="--shiki-light:#6A737D;--shiki-dark:#6A737D;">    // 指定服务器应该监听哪个 IP 地址</span></span>
<span class="line"><span style="--shiki-light:#005CC5;--shiki-dark:#79B8FF;">    &quot;host&quot;</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">: </span><span style="--shiki-light:#032F62;--shiki-dark:#9ECBFF;">&quot;localhost&quot;</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">,</span></span>
<span class="line"></span>
<span class="line"><span style="--shiki-light:#6A737D;--shiki-dark:#6A737D;">    // 指定开发服务器端口</span></span>
<span class="line"><span style="--shiki-light:#005CC5;--shiki-dark:#79B8FF;">    &quot;port&quot;</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">: </span><span style="--shiki-light:#005CC5;--shiki-dark:#79B8FF;">9000</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">,</span></span>
<span class="line"></span>
<span class="line"><span style="--shiki-light:#6A737D;--shiki-dark:#6A737D;">    // disableHostCheck</span></span>
<span class="line"><span style="--shiki-light:#005CC5;--shiki-dark:#79B8FF;">    &quot;https&quot;</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">: </span><span style="--shiki-light:#005CC5;--shiki-dark:#79B8FF;">false</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">,</span></span>
<span class="line"></span>
<span class="line"><span style="--shiki-light:#6A737D;--shiki-dark:#6A737D;">    // 禁用 Host 检查</span></span>
<span class="line"><span style="--shiki-light:#005CC5;--shiki-dark:#79B8FF;">    &quot;disableHostCheck&quot;</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">: </span><span style="--shiki-light:#005CC5;--shiki-dark:#79B8FF;">true</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">,</span></span>
<span class="line"></span>
<span class="line"><span style="--shiki-light:#6A737D;--shiki-dark:#6A737D;">    // 为开发服务器配置自定义代理规则</span></span>
<span class="line"><span style="--shiki-light:#005CC5;--shiki-dark:#79B8FF;">    &quot;proxy&quot;</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">: {</span></span>
<span class="line"><span style="--shiki-light:#6A737D;--shiki-dark:#6A737D;">        // 匹配规则</span></span>
<span class="line"><span style="--shiki-light:#005CC5;--shiki-dark:#79B8FF;">        &quot;/api&quot;</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">: {</span></span>
<span class="line"><span style="--shiki-light:#6A737D;--shiki-dark:#6A737D;">            // 代理到的实际地址</span></span>
<span class="line"><span style="--shiki-light:#005CC5;--shiki-dark:#79B8FF;">            &quot;target&quot;</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">: </span><span style="--shiki-light:#032F62;--shiki-dark:#9ECBFF;">&quot;https://apifoxmock.com/m1/4872189-4528102-default&quot;</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">,</span></span>
<span class="line"></span>
<span class="line"><span style="--shiki-light:#6A737D;--shiki-dark:#6A737D;">            // changeOrigin是一个可选的配置项。当它被设置为true时，请求头中的origin属性会被改变为目标服务器的地址，这样可以避免跨域问题</span></span>
<span class="line"><span style="--shiki-light:#005CC5;--shiki-dark:#79B8FF;">            &quot;changeOrigin&quot;</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">: </span><span style="--shiki-light:#005CC5;--shiki-dark:#79B8FF;">true</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">,</span></span>
<span class="line"></span>
<span class="line"><span style="--shiki-light:#6A737D;--shiki-dark:#6A737D;">            // 重写路径中的字符串</span></span>
<span class="line"><span style="--shiki-light:#005CC5;--shiki-dark:#79B8FF;">            &quot;rewrite&quot;</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">: {</span></span>
<span class="line"><span style="--shiki-light:#005CC5;--shiki-dark:#79B8FF;">                &quot;/api&quot;</span><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">: </span><span style="--shiki-light:#032F62;--shiki-dark:#9ECBFF;">&quot;/api&quot;</span></span>
<span class="line"><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">            }</span></span>
<span class="line"><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">        }</span></span>
<span class="line"><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">    }</span></span>
<span class="line"><span style="--shiki-light:#24292E;--shiki-dark:#E1E4E8;">}</span></span></code></pre></div><p>至此，此项目就配置完成了。</p><h2 id="目录结构" tabindex="-1">目录结构 <a class="header-anchor" href="#目录结构" aria-label="Permalink to &quot;目录结构&quot;">​</a></h2><div class="language- vp-adaptive-theme"><button title="Copy Code" class="copy"></button><span class="lang"></span><pre class="shiki shiki-themes github-light github-dark vp-code" tabindex="0"><code><span class="line"><span>| --- components                 组件目录</span></span>
<span class="line"><span>|     |__ a-component            一个组件模板</span></span>
<span class="line"><span>|     |__ page-root              页面根组件，所有页面的第一层组件</span></span>
<span class="line"><span>| --- constants                  常量目录</span></span>
<span class="line"><span>|     |__ demo.js                常量 \`DEMO\`</span></span>
<span class="line"><span>|     |__ index.js               常量索引</span></span>
<span class="line"><span>| --- functions                  工具函数目录</span></span>
<span class="line"><span>|     |__ demo.js                工具函数 \`DEMO\`</span></span>
<span class="line"><span>|     |__ index.js               工具函数索引</span></span>
<span class="line"><span>| --- mock                       模拟数据目录</span></span>
<span class="line"><span>|     |__ demo.js                模拟数据 \`DEMO\`</span></span>
<span class="line"><span>|     |__ index.js               模拟数据索引</span></span>
<span class="line"><span>| --- node_modules               \`node_modules\` 目录</span></span>
<span class="line"><span>| --- packages                   分包目录</span></span>
<span class="line"><span>|     |__ order                  订单模块包，用于订单模块，一个示例模块</span></span>
<span class="line"><span>|         |__ pages              订单模块页面目录</span></span>
<span class="line"><span>| --- pages                      主包页面目录</span></span>
<span class="line"><span>|     |__ auth                   鉴权相关页面，例如：登录、注册、认证等页面</span></span>
<span class="line"><span>|         |__ login.vue          登录页面</span></span>
<span class="line"><span>|     |__ home                   主页相关页面</span></span>
<span class="line"><span>|         |__ index.vue          主页</span></span>
<span class="line"><span>|     |__ mine                   我的相关页面</span></span>
<span class="line"><span>|         |__ index.vue          我的</span></span>
<span class="line"><span>|     |__ start                  启动相关页面，放置一些启动相关的页面，例如：启动页、欢迎页、场景分发等页面</span></span>
<span class="line"><span>|         |__ index.vue          启动页</span></span>
<span class="line"><span>|     |__ webview                \`webview\` 相关页面，放置一些 \`webview\` 页面</span></span>
<span class="line"><span>|         |__ index.vue          公共 \`webview\` 页面</span></span>
<span class="line"><span>|     |__ a-page.vue             一个页面模板</span></span>
<span class="line"><span>| --- router                     路由管理目录</span></span>
<span class="line"><span>|     |__ config.js              \`hi-router\` 路由配置文件</span></span>
<span class="line"><span>|     |__ index.js               路由相关工具函数</span></span>
<span class="line"><span>| --- services                   接口管理目录</span></span>
<span class="line"><span>|     |__ common.js              公共接口函数</span></span>
<span class="line"><span>|     |__ index.js               接口函数索引</span></span>
<span class="line"><span>|     |__ instances.js           \`hi-http\` 请求示例文件</span></span>
<span class="line"><span>|     |__ upload.js              上传接口函数</span></span>
<span class="line"><span>|     |__ user.js                用户相关接口</span></span>
<span class="line"><span>| --- settings                   项目配置管理目录</span></span>
<span class="line"><span>|     |__ index.js               项目配置文件</span></span>
<span class="line"><span>| --- static                     静态资源目录</span></span>
<span class="line"><span>|     |__ images                 图片资源目录</span></span>
<span class="line"><span>|         |__ bgis               背景图资源目录</span></span>
<span class="line"><span>|         |__ icons              图片图标资源目录</span></span>
<span class="line"><span>|         |__ temps              临时图片资源目录</span></span>
<span class="line"><span>|     |__ tabBar                 \`tabBar\` 相关资源目录</span></span>
<span class="line"><span>|         |__ icons              \`tabBar\` 图标资源目录</span></span>
<span class="line"><span>|     |__ favicon.ico            项目网站图标文件</span></span>
<span class="line"><span>|     |__ logo.jpg               项目 \`logo\` 图片</span></span>
<span class="line"><span>| --- stores                     状态数据管理目录</span></span>
<span class="line"><span>|     |__ demo.js                状态数据 \`DEMO\`</span></span>
<span class="line"><span>|     |__ index.js               状态数据索引</span></span>
<span class="line"><span>|     |__ persist-common-config  \`pinia\` 持久化插件 \`&quot;pinia-plugin-persistedstate&quot;\` 公共配置</span></span>
<span class="line"><span>|     |__ platform.js            平台相关状态数据</span></span>
<span class="line"><span>|     |__ user.js                用户相关状态数据</span></span>
<span class="line"><span>| --- styles                     样式管理目录</span></span>
<span class="line"><span>|     |__ assist.scss            辅助样式类</span></span>
<span class="line"><span>|     |__ iconfont.css           项目 \`iconfont\` 字体图标文件</span></span>
<span class="line"><span>|     |__ index.scss             样式索引</span></span>
<span class="line"><span>|     |__ variables.scss         项目样式变量</span></span>
<span class="line"><span>| --- uni_modules                \`uni_modules\` 目录</span></span>
<span class="line"><span>| --- unpackage                  编译目录</span></span>
<span class="line"><span>| --- .gitignore                 \`.gitignore\` 文件</span></span>
<span class="line"><span>| --- App.vue                    应用配置，用来配置 \`App\` 全局样式以及监听应用生命周期</span></span>
<span class="line"><span>| --- changelog.md               更新日志</span></span>
<span class="line"><span>| --- index.html                 \`index.html\` 模板</span></span>
<span class="line"><span>| --- package.json               \`package.json\` 文件</span></span>
<span class="line"><span>| --- pages.json                 配置页面路由、导航条、选项卡等页面类信息</span></span>
<span class="line"><span>| --- prettier.config.js         \`prettier\` 格式化配置</span></span>
<span class="line"><span>| --- readme.md                  项目介绍</span></span>
<span class="line"><span>| --- uni.promisify.adaptor.js   创建项目时自带的 \`Promise\` 拦截器配置文件</span></span>
<span class="line"><span>| --- uni.scss                   内置的常用样式变量</span></span></code></pre></div><h2 id="加群交流反馈" tabindex="-1">加群交流反馈 <a class="header-anchor" href="#加群交流反馈" aria-label="Permalink to &quot;加群交流反馈&quot;">​</a></h2><h3 id="qq群" tabindex="-1">QQ群 <a class="header-anchor" href="#qq群" aria-label="Permalink to &quot;QQ群&quot;">​</a></h3><br>`,28)),t(n,{list:a.value},null,8,["list"])]))}});export{k as __pageData,g as default};
