/**
 * 导航菜单项的配置
 */
export default [
    // 主页
    { text: "主页", link: "/" },

    // Hi 系列
    {
        text: "Hi",
        items: [
            { text: "hi-ui", link: "/hi-ui/demo" },
            { text: "hi-ui pro", link: "/hi-ui-pro/index" },
            { text: "hi-http", link: "/hi-http/index" },
            { text: "hi-router", link: "/hi-router/index" },
        ],
    },
];
